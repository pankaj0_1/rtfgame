﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;

namespace RTF_Utilities
{
    public class SentEmail
    {
        public static string fromemail = "local@nearbyexperiences.com";

        public static string fromemailtest1 = "jone.boaz@gmail.com";
        public static string fromemailtest = "v2rsolutionteam@gmail.com";

        public static string emailSentUat(string message, string subject, string from_email, string to_email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from_email);
                mail.To.Add(to_email);
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                SmtpServer.UseDefaultCredentials = true;
                SmtpServer.Port = 587;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "v2rsolutionteam@gmail.com";
                NetworkCred.Password = "v2rsolution@723re#";
                SmtpServer.Credentials = NetworkCred;
                SmtpServer.EnableSsl = true;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;

                SmtpServer.Send(mail);
                return "1";
            }
            catch (Exception ex)
            {
                //LogUtil lg = new LogUtil();
                LogUtil.MakeErrorLog(string.Format("{0} emailSentUat ", ""), ex);
                //ErrorLogs._InsertLogs("emailSentUat", from_email, to_email, 0, ex.Message.ToString());
                return "0";
            }
        }


        public static async void SendEmail_awsSmtp(string body, string subject, string fromemail, string toemail)
        {
            try
            {
                fromemail = "sports.cards.app@gmail.com";
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(fromemail);
                mail.To.Add(toemail);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                SmtpServer.UseDefaultCredentials = true;
                SmtpServer.Port = 587;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "sports.cards.app@gmail.com";
                NetworkCred.Password = "u2CaK4KjfBd92q5U";
                SmtpServer.Credentials = NetworkCred;
                SmtpServer.EnableSsl = true;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;

                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                //LogUtil lg = new LogUtil();
                LogUtil.MakeErrorLog(string.Format("{0} emailSentUat ", ""), ex);
            }
        }

        public  static string SendEmail_awsVerSmptp(string body, string subject, string fromemail, string toemail)
        {
            string a = "";
            try
            {
                fromemail = "sports.cards.app@gmail.com";
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(fromemail);
                mail.To.Add(toemail);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                SmtpServer.UseDefaultCredentials = true;
                SmtpServer.Port = 587;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "sports.cards.app@gmail.com";
                NetworkCred.Password = "u2CaK4KjfBd92q5U";
                SmtpServer.Credentials = NetworkCred;
                SmtpServer.EnableSsl = true;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.Send(mail);
                a = "1";
            }
            catch (Exception ex)
            {
                a = "0";
                //LogUtil lg = new LogUtil();
                LogUtil.MakeErrorLog(string.Format("{0} emailSentUat ", ""), ex);
            }
            return a;
        }


        public static async void SendEmail_aws(string body, string subject, string fromemail, string toemail)
        {
            try
            {
                fromemail = "helpdesk@sports.cards";
                String FROMNAME = "Sports.Cards";
                String SMTP_USERNAME = "AKIA447F3TA34TLAVBWP";
                String SMTP_PASSWORD = "4mcWfjEWmN7IXyiTCPaHf+bMvnfXvV9aiKRTtCt4";
                String HOST = "email-smtp.ap-south-1.amazonaws.com";
                int PORT = 587;

                MailMessage message = new MailMessage();
                message.IsBodyHtml = true;
                message.From = new MailAddress(fromemail, FROMNAME);
                message.To.Add(new MailAddress(toemail));
                message.Subject = subject;
                message.Body = body;
                using (var client = new System.Net.Mail.SmtpClient(HOST, PORT))
                {
                    client.Credentials = new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                    client.EnableSsl = true;
                    client.Send(message);
                }

            }
            catch (Exception ex)
            {
                LogUtil.MakeErrorLog(string.Format("{0} emailSentUat ", ""), ex);
            }
        }

        public static string SendEmail_awsVer(string body, string subject, string fromemail, string toemail)
        {
            string a = "";
            try
            {
                fromemail = "helpdesk@sports.cards";
                String FROMNAME = "Sports.Cards";
                String SMTP_USERNAME = "AKIA447F3TA34TLAVBWP";
                String SMTP_PASSWORD = "4mcWfjEWmN7IXyiTCPaHf+bMvnfXvV9aiKRTtCt4";
                String HOST = "email-smtp.ap-south-1.amazonaws.com";
                int PORT = 587;

                MailMessage message = new MailMessage();
                message.IsBodyHtml = true;
                message.From = new MailAddress(fromemail, FROMNAME);
                message.To.Add(new MailAddress(toemail));
                message.Subject = subject;
                message.Body = body;
                using (var client = new System.Net.Mail.SmtpClient(HOST, PORT))
                {
                    client.Credentials = new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                    client.EnableSsl = true;
                    client.Send(message);
                }
                a = "1";

            }
            catch (Exception ex)
            {
                a = "0";
                LogUtil.MakeErrorLog(string.Format("{0} emailSentUat ", ""), ex);
            }
            return a;
        }



        // public static async Task<string> SendEmail_awsVer(string body, string subject, string fromemail, string toemail)
        public static async void SendEmail_awsOld(string body, string subject, string fromemail, string toemail)
        {
            using (var client = new AmazonSimpleEmailServiceClient("AKIA447F3TA34TLAVBWP", "4mcWfjEWmN7IXyiTCPaHf+bMvnfXvV9aiKRTtC", RegionEndpoint.AFSouth1))
            {
                var emailRequest = new SendEmailRequest()
                {
                    Source = "no-reply@cricket24.bet",
                    Destination = new Destination(),
                    Message = new Message()
                };

                emailRequest.Destination.ToAddresses.Add(toemail);
                emailRequest.Message.Subject = new Content(subject);
                emailRequest.Message.Body = new Body(new Content(body));
                var response = await client.SendEmailAsync(emailRequest);
            }
        }
    }
}
