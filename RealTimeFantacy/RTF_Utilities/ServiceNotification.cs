﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace RTF_Utilities
{
    public class ServiceNotification
    {

        public static string Push_notification(string title, string text, string image, string sentto, string[] deviceids)
        {
            string res = "";
            try
            {
                //need to code for select ids from db by sent to

                string[] id = { "fZe8ZDdZ1v4:APA91bGK0iS_XZmwLPBT4p8z679McwHHOEGG_WZ5ye8Zu-tY9Z0RQSx8OafSQe2_pKPkd-unsulY0mwjSGrlpozQ-5qtrJ-tm0ilBrZB-S_IQphqFY9ib9fdvkPp_vW9BPLEJgBmsJ40",
                "fL0UTiV3A6w:APA91bHMVz0lclemVQ4D54PNkdfHxJkXfX1a7abnzyta8LzO9JHLFnryTUWVzNXgkgCTAyuYD8qALZyybqYMCi7qUbdSUpmzLpoh1NoFN-nhhBScZ2DMIGsfmgOXjrYqgk9S--S1CyrQ",
                "eML5VZywzAo:APA91bELIoSqKWhcp2FfysHcQXOWy2Wr1AReHuia3MFGLpvQ8ADRRxdbLJU65Za9HZz64D0-UV7scjtx3DbHxWZ1qmQivu5O3-h9UP3uEj08M5Jdv1zAL9PJ250deEPg1PksQrWNyQb8",
                "dxDiCQqrOwA:APA91bE5fnZ7GmNfvR0dOJRb5x3lUoKfc75Z3rF2s4quTtFQSTBErff8yx-8LDTkEDoG2gzKCBLMVqqttJCflqPxCBnObZUPfR3MrXCu1S5t-p6vKXfUZ3t2ZIEbNv19UK9EpTNjmMtf",
                "cj_wRDbg2n8:APA91bFpCuD5r9-11gdiM035eHGqhq3bBi8nQOnPxcsypfyt1N7a0hE7VZ5z75B7m01Bo3LtbMrd_77TjUxlHKxyr53p7Nygx7DaQ8RoLOmbj9bVoRnsdKTBI0xsKHOV4y5QkpI8G5jM" };

                //if (sentto == "All")
                //{
                FirebaseNotificationModel M = new FirebaseNotificationModel();
                M.registration_ids = deviceids;
                M.data = new NotificationModel();
                M.data.title = title;
                M.data.body = text;
                M.data.image = image;
                M.data.click_action = "https://dev.sports.cards";
                // M.data.notification_flag = notificationflag;
                // M.data.stock_symbol = symbol;
                res = Send(M);
                //}
                //else if (sentto == "reg-users")
                //{
                //    FirebaseNotificationModelUser M = new FirebaseNotificationModelUser();
                //    M.to = "/topics/" + "NiftyTrader-RegisteredUsers";
                //    M.data = new NotificationModel();
                //    M.data.title = title;
                //    M.data.text = text;
                //    M.data.image = image;
                //    M.data.notification_flag = notificationflag;
                //    M.data.stock_symbol = symbol;
                //    res = SendUser(M);
                //}

            }
            catch (Exception ex)
            {
                res = "false";
                //return new { result = 0, status = str } as dynamic;
            }
            return res;
        }

        public static string Send1(FirebaseNotificationModel firebaseModel)
        {
            string respoonse = "";
            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", "AAAAh4LC7VI:APA91bE-iG9Xf1PSZfnB3rjmRMwXlVa7t47MGeBK8_6zjTa6IPFjer8YrxvObjf5ahyxkzgrpvNGUeRksQr8Lm7QcqHVzkYIu8PZla9-4QoWFD8CmDWHUn8xQTtiJXyk78P5zA4w12R7"));
            //Sender Id - From firebase project setting  
            tRequest.Headers.Add(string.Format("Sender: id={0}", "582014397778"));
            tRequest.ContentType = "application/json";
            //var jsonBody = JsonConvert.SerializeObject(firebaseModel);
            string postbody = JsonConvert.SerializeObject(firebaseModel).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                //result.Response = sResponseFromServer;
                            }
                    }
                }
            }
            return respoonse;
        }
        public static string Send(FirebaseNotificationModel firebaseModel)
        {
            string respoonse = "";
            HttpRequestMessage httpRequest = null;
            HttpClient httpClient = null;
            var applicationID = "AAAAh4LC7VI:APA91bE-iG9Xf1PSZfnB3rjmRMwXlVa7t47MGeBK8_6zjTa6IPFjer8YrxvObjf5ahyxkzgrpvNGUeRksQr8Lm7QcqHVzkYIu8PZla9-4QoWFD8CmDWHUn8xQTtiJXyk78P5zA4w12R7";
            var authorizationKey = string.Format("key={0}", applicationID);
            var jsonBody = JsonConvert.SerializeObject(firebaseModel);
            try
            {
                httpRequest = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send");
                httpRequest.Headers.TryAddWithoutValidation("Authorization", authorizationKey);
                httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
                httpClient = new HttpClient();
                var respuesta = httpClient.SendAsync(httpRequest).Result;
                if (respuesta.StatusCode == HttpStatusCode.Accepted || respuesta.StatusCode == HttpStatusCode.OK || respuesta.StatusCode == HttpStatusCode.Created)
                {
                    respoonse = "true";
                }
                else
                {
                    respoonse = "false";
                }
            }
            catch
            { respoonse = "false"; throw; }
            finally
            {
                httpRequest.Dispose();
                httpClient.Dispose();
            }
            return respoonse;
        }

        //public static string SendUser(FirebaseNotificationModelUser firebaseModel)
        //{
        //    string respoonse = "";
        //    HttpRequestMessage httpRequest = null;
        //    HttpClient httpClient = null;
        //    var applicationID = "1:582014397778:web:6348054eab5aab76fade8b";
        //    var authorizationKey = string.Format("key={0}", applicationID);
        //    var jsonBody = JsonConvert.SerializeObject(firebaseModel);
        //    try
        //    {
        //        httpRequest = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send");
        //        httpRequest.Headers.TryAddWithoutValidation("Authorization", authorizationKey);
        //        httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
        //        httpClient = new HttpClient();
        //        var respuesta = httpClient.SendAsync(httpRequest).Result;
        //        if (respuesta.StatusCode == HttpStatusCode.Accepted || respuesta.StatusCode == HttpStatusCode.OK || respuesta.StatusCode == HttpStatusCode.Created)
        //        {
        //            respoonse = "true";
        //        }
        //        else
        //        {
        //            respoonse = "false";
        //            // throw new Exception("Ocurrio un error al obtener la respuesta del servidor: " + respuesta.StatusCode);
        //        }
        //    }
        //    catch
        //    { respoonse = "false"; throw; }
        //    finally
        //    {
        //        httpRequest.Dispose();
        //        httpClient.Dispose();
        //    }
        //    return respoonse;
        //}
        public class FirebaseNotificationModel
        {
            [JsonProperty(PropertyName = "registration_ids")]
            public string[] registration_ids { get; set; }

            [JsonProperty(PropertyName = "notification")]
            public NotificationModel data { get; set; }
        }

        public class FirebaseNotificationModelUser
        {
            [JsonProperty(PropertyName = "to")]
            public string to { get; set; }

            [JsonProperty(PropertyName = "data")]
            public NotificationModel data { get; set; }
        }

        public class NotificationModel
        {
            public string title { get; set; }
            public string body { get; set; }
            public string image { get; set; }
            public string click_action { get; set; }
            //public string notification_flag { get; set; }
            //public string stock_symbol { get; set; }
        }
    }
}
