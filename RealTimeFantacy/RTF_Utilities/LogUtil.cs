﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RTF_Utilities
{
  
    public class LogUtil
    {
        public static string LogFolder = "C:\\Cricket24.BetLog\\AllLogs\\";
        public static string LogPath;
        string PageName { get; set; }

        public LogUtil()
        {

        }
        public static void CreateLog(string log)
        {
            if (!string.IsNullOrEmpty(LogFolder))
            {
                if (!Directory.Exists(LogFolder))
                {
                    Directory.CreateDirectory(LogFolder);
                }
                LogPath = Path.Combine(LogFolder, string.Format("Log_{0}.txt", System.DateTime.Now.ToString("dd_MMM_yyyy")));
            }

            try
            {
                if (!File.Exists(LogPath))
                {
                    using (StreamWriter file = new StreamWriter(LogPath))
                    {
                        file.WriteLine(log);
                    }
                }
                else
                {
                    using (StreamWriter w = File.AppendText(LogPath))
                    {
                        w.WriteLine(log);
                        w.Flush();
                        w.Close();
                    }
                }
            }
            catch
            {
            }
        }

        public static void MakeStartLog(string PageName)
        {
            CreateLog(string.Format("{0}: Request Start:{1}", PageName, System.DateTime.Now));
        }
        public static void MakeEndLog(string PageName)
        {
            CreateLog(string.Format("{0}: Request End:{1}", PageName, System.DateTime.Now));
        }
        public static void MakeErrorLog(string PageName, Exception Ex)
        {
            CreateLog(string.Format("ERROR IN {0} at {1} /n  {2}", PageName, System.DateTime.Now, string.Format("Error: {0},  Details: {1}", Ex.Message, Ex.StackTrace)));
        }
        public static void MakeCustomLog(string PageName, String Message)
        {
            CreateLog(string.Format("{0} at {1} ---->  {2}", PageName, System.DateTime.Now, Message));
        }
        public static void MakeErrorLogforIf(string PageName, string msg)
        {
            CreateLog(string.Format("ERROR IN {0} at {1}", PageName, System.DateTime.Now, string.Format("Error: {0}", msg)));
        }
    }
}
