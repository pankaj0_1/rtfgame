﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RTF_BaseObject;
using RTF_BaseObject.RTF;
using RTF_Mgr_Interface.Admin;
using RTF_Mgr_Interface.Api;
using RTF_Utilities;
using RTF_Utilities.Enum;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace RTF.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[EnableCors("AllowOrigin")]
    public class RTFGetController : ControllerBase
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;
        IApiMgr _IApiMgr;

        public RTFGetController(IAdminMgr IAdminMgr, IApiMgr IApiMgr, IConfiguration iConfig, IHttpContextAccessor httpContextAccessor)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
            _IApiMgr = IApiMgr;
        }

        [HttpGet]
        public dynamic getConfiguration()
        {
            ResultObject<Configuration> res = new ResultObject<Configuration>();
            try
            {
                res = _IApiMgr.getConfiguration("config", "");
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            withdrawal_limit = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Withdraw Limit").Select(x => x.config_value).FirstOrDefault()),
                            register_cash = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Register coin").Select(x => x.config_value).FirstOrDefault()),
                            //daily_login = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Daily Login").Select(x => x.config_value).FirstOrDefault()),
                            //seven_day_login = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Seven Day Login").Select(x => x.config_value).FirstOrDefault()),
                            //one_month_login = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "One Month Login").Select(x => x.config_value).FirstOrDefault()),
                            referral_code = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Refferal Code").Select(x => x.config_value).FirstOrDefault()),
                            //quiz = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Quiz").Select(x => x.config_value).FirstOrDefault()),
                            //daily_user_poll = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Daily User Poll").Select(x => x.config_value).FirstOrDefault()),
                            //question_limit = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Question Limit").Select(x => x.config_value).FirstOrDefault()),
                            //extra_referral_coin = Convert.ToInt32(res.ResultData.lstconfig.Where(x => x.config_name == "Extra Referral Coin").Select(x => x.config_value).FirstOrDefault())
                        }
                        //result = 1,
                        //message = "Success",
                        //data = res.ResultData.lstconfig.ToList()
                        //data = new
                        //{
                        //    country_code = res.ResultData.lstcountry.ToList(),
                        //    config_list = res.ResultData.lstconfig.ToList(),
                        //},

                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getCountry()
        {
            ResultObject<Configuration> res = new ResultObject<Configuration>();
            try
            {
                res = _IApiMgr.getConfiguration("config", "");
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.lstcountry.ToList()
                        //data = new
                        //{
                        //    country_code = res.ResultData.lstcountry.ToList(),
                        //    config_list = res.ResultData.lstconfig.ToList(),
                        //},

                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getFAQ()
        {
            try
            {
                ResultObject<List<FaqData>> res = _IAdminMgr.viewfaq("viewFaqApi", "", ""); ;
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            x.faq_id,
                            x.faq_question,
                            x.faq_answer
                        })
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        //[HttpGet]
        //public dynamic getMatchesData()
        //{
        //    ResultObject<List<MatchModel>> res = new ResultObject<List<MatchModel>>();
        //    try
        //    {
        //        res = _IApiMgr.GethomeMatches("gethome_matches", "", "");
        //        if (res.Result == ResultType.Success)
        //        {

        //            return new
        //            {
        //                result = 1,
        //                message = "Success",
        //                data = new
        //                {
        //                    //live = res.ResultData.Where(r => r.match_status == 1 && r.match_date.Value.Date == DateTime.Now.Date).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
        //                    //upcomming = res.ResultData.Where(r => r.match_status != 1 && r.match_date.Value.Date >= DateTime.Now.Date).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),

        //                    live = res.ResultData.Where(r => r.match_status == 1 && r.match_date.Value.Date == DateTime.Now.Date).OrderBy(r => r.match_date).ThenBy(r => r.match_time)
        //                     .Select(x => new
        //                     {
        //                         x.match_id,
        //                         x.home_team_id,
        //                         x.away_team_id,
        //                         x.match_date,
        //                         x.match_time,
        //                         x.venue,
        //                         x.hometeamname,
        //                         x.awayteamname,
        //                         x.MatchNumber,
        //                         x.HomeTeam_odds,
        //                         x.AwayTeam_odds,
        //                         x.batting,
        //                         x.home_team_runs,
        //                         x.home_team_overs,
        //                         x.home_team_wickets,
        //                         x.away_team_runs,
        //                         x.away_team_overs,
        //                         x.away_team_wickets,
        //                         x.winning_team,
        //                         x.superover_batting,
        //                         x.super_home_team_runs,
        //                         x.super_home_team_overs,
        //                         x.super_home_team_wickets,
        //                         x.super_away_team_runs,
        //                         x.super_away_team_overs,
        //                         x.super_away_team_wickets

        //                     }).ToList(),
        //                    upcomming = res.ResultData.Where(r => r.match_status != 1 && r.match_date.Value.Date >= DateTime.Now.Date).OrderBy(r => r.match_date).ThenBy(r => r.match_time)
        //                    .Select(x => new
        //                    {
        //                        x.match_id,
        //                        x.home_team_id,
        //                        x.away_team_id,
        //                        x.match_date,
        //                        x.match_time,
        //                        x.venue,
        //                        x.hometeamname,
        //                        x.awayteamname,
        //                        x.MatchNumber,
        //                        x.HomeTeam_odds,
        //                        x.AwayTeam_odds,
        //                        x.batting,
        //                        x.home_team_runs,
        //                        x.home_team_overs,
        //                        x.home_team_wickets,
        //                        x.away_team_runs,
        //                        x.away_team_overs,
        //                        x.away_team_wickets,
        //                        x.winning_team,
        //                        x.superover_batting,
        //                        x.super_home_team_runs,
        //                        x.super_home_team_overs,
        //                        x.super_home_team_wickets,
        //                        x.super_away_team_runs,
        //                        x.super_away_team_overs,
        //                        x.super_away_team_wickets
        //                    }).ToList(),

        //                }
        //            } as dynamic;
        //        }
        //        else if (res.Result == ResultType.Info)
        //        {
        //            return new
        //            {
        //                result = 0,
        //                message = "Invalid Data",
        //                data = ""
        //            } as dynamic;
        //        }
        //        else
        //        {
        //            return new
        //            {
        //                result = 0,
        //                message = "Some Error occured",
        //                data = ""
        //            } as dynamic;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new
        //        {
        //            result = 0,
        //            message = "Some Error occured",
        //            data = ""
        //        } as dynamic;
        //    }
        //}

        [HttpGet]
        public dynamic getOddsData(int match_id)
        {
            ResultObject<List<Tbl_Odds_History>> res = new ResultObject<List<Tbl_Odds_History>>();
            try
            {
                res = _IApiMgr.GetMatch_odds("getoddsbymatchid", match_id.ToString());
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getMatcheWinningData(int match_id)
        {
            ResultObject<dynamic> res = new ResultObject<dynamic>();
            try
            {
                res = _IApiMgr.matchDynamic("getMatchesWinningData", match_id);
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        //[HttpGet]
        //public dynamic getOddsScoresData(string match_key)
        //{
        //    ResultObject<List<Tbl_Odds_History>> res = new ResultObject<List<Tbl_Odds_History>>();
        //    try
        //    {
        //        res = _IApiMgr.GetMatch_odds("getoddsbymatchkey", match_key.ToString());
        //        if (res.Result == ResultType.Success)
        //        {

        //            ResultObject<List<MatchModel>> resMatch = new ResultObject<List<MatchModel>>();
        //            resMatch = _IApiMgr.GethomeMatches("gethome_matchesById", res.ResultData.Select(x => x.match_id).FirstOrDefault().ToString(), "");
        //            return new
        //            {
        //                result = 1,
        //                message = "Success",
        //                //data = res.ResultData
        //                data = new
        //                {
        //                    odds = res.ResultData,
        //                    score = resMatch.ResultData.Select(x=>new { 
        //                    x.hometeamname,
        //                    x.awayteamname,
        //                    x.home_team_runs,
        //                    x.home_team_wickets,
        //                    x.home_team_overs,
        //                    x.away_team_runs,
        //                    x.away_team_wickets,
        //                    x.away_team_overs,
        //                    }).FirstOrDefault(),
        //                    marketStatus = resMatch.ResultData.Select(x=>x.marketStatus).FirstOrDefault()
        //                }
        //            } as dynamic;
        //        }
        //        else if (res.Result == ResultType.Info)
        //        {
        //            return new
        //            {
        //                result = 0,
        //                message = "Invalid Data",
        //                data = ""
        //            } as dynamic;
        //        }
        //        else
        //        {
        //            return new
        //            {
        //                result = 0,
        //                message = "Some Error occured",
        //                data = ""
        //            } as dynamic;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new
        //        {
        //            result = 0,
        //            message = "Some Error occured",
        //            data = ""
        //        } as dynamic;
        //    }
        //}
    }
}