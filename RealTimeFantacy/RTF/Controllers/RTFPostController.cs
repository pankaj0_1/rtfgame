﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using RTF_BaseObject;
using RTF_BaseObject.RTF;
using RTF_Mgr_Interface.Admin;
using RTF_Mgr_Interface.Api;
using RTF_Utilities;
using RTF_Utilities.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static RTF_BaseObject.PostModel.PostDataModel;
using RTF_BaseObject.RTF_API_Model;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;

namespace RTF.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[EnableCors("AllowOrigin")]
    public class RTFPostController : ControllerBase
    {
        public static IWebHostEnvironment _environment;
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;
        IApiMgr _IApiMgr;


        public RTFPostController(IAdminMgr IAdminMgr, IApiMgr IApiMgr, IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IWebHostEnvironment environment)
        {
            _environment = environment;
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
            _IApiMgr = IApiMgr;
        }

        public class FIleUploadAPI
        {
            public IFormFile files
            {
                get;
                set;
            }
        }

        #region userapi
        [HttpPost]
        /// public dynamic signIn(EncryptPostData encryptdata)
        public dynamic signIn(signin data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                // string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //signin data = JsonConvert.DeserializeObject<signin>(decryptdata);
                res = _IApiMgr.signin("signin", data);
                if (res.Result == ResultType.Success)
                {
                    //var tokenStr = GenerateJSONWebToken(res.ResultData); 
                    string otpmessage = res.ResultData.otp + " is the OTP to login into your Sports.Cards account.";
                    SendSms.snsSned(data.country_code + res.ResultData.mobile, otpmessage);
                    return new
                    {
                        result = 1,
                        message = "Success"
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Mobile number does not exist",
                        //data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        [HttpPost]
        //public dynamic otpVerification(EncryptPostData encryptdata)
        public dynamic otpVerification(signin data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                //string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //signin data = JsonConvert.DeserializeObject<signin>(decryptdata);
                res = _IApiMgr.signin("otpverify", data);
                if (res.Result == ResultType.Success)
                {
                    ResultObject<dynamic> resWallet = new ResultObject<dynamic>();
                    resWallet = _IApiMgr.Get_user_wallet_balance("userwalletbal", res.ResultData.user_id);
                    var tokenStr = GenerateJSONWebToken(res.ResultData);
                    //if ((res.ResultData.email != "" && res.ResultData.email != null) && (res.ResultData.email_is_verified == 0 || res.ResultData.email_is_verified == null))
                    //{
                    //    verifyEmailsent(Convert.ToString(res.ResultData.user_id), (res.ResultData.first_name + " " + res.ResultData.last_name), res.ResultData.email);
                    //}
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            token = tokenStr,
                            res.ResultData.country_code_id,
                            res.ResultData.mobile,
                            res.ResultData.first_name,
                            res.ResultData.last_name,
                            res.ResultData.email,
                            res.ResultData.profile_picture,
                            res.ResultData.email_is_verified,
                            res.ResultData.status,
                            res.ResultData.last_login,
                            res.ResultData.created_at,
                            res.ResultData.updated_at,
                            walletBalance = Convert.ToDecimal(resWallet.ResultData.balance),
                            creditedCash = res.ResultData.creditedCoin,
                            res.ResultData.creditType,
                            res.ResultData.referral_code,
                            res.ResultData.addedBalance,
                            res.ResultData.winningBalance,
                            res.ResultData.bonusBalance,
                            res.ResultData.user_id
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Incorrect verification code.",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [HttpPost]
        //public dynamic signUp(EncryptPostData encryptdata)
        public dynamic signUp(Usersdetails data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                //string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Usersdetails data = JsonConvert.DeserializeObject<Usersdetails>(decryptdata);
                if(data.login_flag ==0)
                {
                    res = _IApiMgr.signup("signup", data);
                    if (res.Result == ResultType.Success)
                    {
                        if (res.ResultData.country_code == "Mobile number already exists")
                        {
                            return new
                            {
                                result = 0,
                                message = "Mobile number is already exists",
                            } as dynamic;
                        }
                        else if (res.ResultData.country_code == "Email already exists")
                        {
                            return new
                            {
                                result = 0,
                                message = "Email Id is already exists",
                            } as dynamic;
                        }
                        else
                        {
                            string otpmessage = res.ResultData.otp + " is the OTP to login into your Sports.Cards account.";
                            if (res.ResultData.email_is_verified != 1 && res.ResultData.email != null && res.ResultData.email_link == null)
                            {
                                verifyEmailsent(Convert.ToString(res.ResultData.user_id), (res.ResultData.first_name + " " + res.ResultData.last_name), res.ResultData.email);
                            }
                            SendSms.snsSned(res.ResultData.country_code + res.ResultData.mobile, otpmessage);

                            return new
                            {
                                result = 1,
                                message = "Success",
                            } as dynamic;
                        }
                    }
                    else if (res.Result == ResultType.Info)
                    {
                        return new
                        {
                            result = 0,
                            message = "Mobile number is already exist",
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = "Failure"
                        } as dynamic;
                    }
                }
                else
                {
                    res = _IApiMgr.signup("signupsocial", data);
                    ResultObject<dynamic> resWallet = new ResultObject<dynamic>();
                    resWallet = _IApiMgr.Get_user_wallet_balance("userwalletbal", res.ResultData.user_id);
                    var tokenStr = GenerateJSONWebToken(res.ResultData);
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            token = tokenStr,
                            res.ResultData.country_code_id,
                            res.ResultData.mobile,
                            res.ResultData.first_name,
                            res.ResultData.last_name,
                            res.ResultData.email,
                            res.ResultData.profile_picture,
                            res.ResultData.email_is_verified,
                            res.ResultData.status,
                            res.ResultData.last_login,
                            res.ResultData.created_at,
                            res.ResultData.updated_at,
                            walletBalance = Convert.ToDecimal(resWallet.ResultData.balance),
                            creditedCash = res.ResultData.creditedCoin,
                            res.ResultData.creditType,
                            res.ResultData.referral_code,
                            res.ResultData.addedBalance,
                            res.ResultData.winningBalance,
                            res.ResultData.bonusBalance,
                            res.ResultData.user_id
                        }
                    } as dynamic;
                }
              
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure"
                } as dynamic;
            }
        }

        [HttpPost]
        //public dynamic referralVerification(EncryptPostData encryptdata)
        public dynamic referralVerification(Usersdetails data)
        {
            ResultObject<string> res = new ResultObject<string>();

            //string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
            //Usersdetails data = JsonConvert.DeserializeObject<Usersdetails>(decryptdata);

            res = _IApiMgr.updateemaillink("referralVerify", "", "", data.referral_code);
            if (res.Result == ResultType.Success)
            {
                return new
                {
                    result = 1,
                    message = "Referral Code is Match",
                } as dynamic;
            }
            else
            {
                return new
                {
                    result = 0,
                    message = "Referral Code is Not Match",
                } as dynamic;
            }

        }


        [Authorize]
        [HttpPost]
        public dynamic forceLogin()
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                Usersdetails data = new Usersdetails();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.signup("forcelogin", data);
                if (res.Result == ResultType.Success)
                {
                    if (res.ResultData.last_login != null)
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                last_login = res.ResultData.last_login.Value.ToString("dd-MM-yyyy")
                            }
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                last_login = res.ResultData.last_login
                            }
                        } as dynamic;
                    }

                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        public dynamic dailyReward()
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                Usersdetails data = new Usersdetails();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.signup("dailyReward", data);
                if (res.Result == ResultType.Success)
                {
                    ResultObject<dynamic> resWallet = new ResultObject<dynamic>();
                    resWallet = _IApiMgr.Get_user_wallet_balance("userwalletbal", res.ResultData.user_id);

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            //last_login = res.ResultData.last_login.Value.ToString("dd-MM-yyyy"),
                            last_login = res.ResultData.last_login,
                            res.ResultData.creditedCoin,
                            walletBalance = Convert.ToDecimal(resWallet.ResultData.balance)
                        }
                    } as dynamic;

                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "User Already got Reward",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic myProfile()
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                Usersdetails data = new Usersdetails();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.signup("myProfile", data);
                if (res.Result == ResultType.Success)
                {
                    ResultObject<dynamic> resWallet = new ResultObject<dynamic>();
                    resWallet = _IApiMgr.Get_user_wallet_balance("userwalletbal", res.ResultData.user_id);
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            res.ResultData.country_code_id,
                            res.ResultData.country_code,
                            res.ResultData.mobile,
                            res.ResultData.first_name,
                            res.ResultData.last_name,
                            res.ResultData.email,
                            res.ResultData.profile_picture,
                            res.ResultData.email_is_verified,
                            res.ResultData.status,
                            res.ResultData.last_login,
                            res.ResultData.created_at,
                            res.ResultData.updated_at,
                            walletBalance = Convert.ToDecimal(resWallet.ResultData.balance),
                            res.ResultData.addedBalance,
                            res.ResultData.winningBalance,
                            res.ResultData.bonusBalance
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        //public dynamic editProfile(EncryptPostData encryptdata)
        public dynamic editProfile(Usersdetails data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                //string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Usersdetails data = JsonConvert.DeserializeObject<Usersdetails>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.signup("editProfile", data);
                if (res.Result == ResultType.Success)
                {
                    //if (res.ResultData.email != data.email)
                    //if (res.ResultData.email != null && res.ResultData.email != "")
                    if (res.ResultData.email_is_verified != 1 && res.ResultData.email != null)
                    //if ((res.ResultData.email != "" && res.ResultData.email != null) && (res.ResultData.email_is_verified == 0 || res.ResultData.email_is_verified == null))
                    {
                        verifyEmailsent(Convert.ToString(res.ResultData.user_id), (res.ResultData.first_name + " " + res.ResultData.last_name), res.ResultData.email);
                    }

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            res.ResultData.country_code_id,
                            res.ResultData.mobile,
                            res.ResultData.first_name,
                            res.ResultData.last_name,
                            res.ResultData.email,
                            res.ResultData.profile_picture,
                            res.ResultData.email_is_verified,
                            res.ResultData.status,
                            res.ResultData.last_login,
                            res.ResultData.created_at,
                            res.ResultData.updated_at
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        #endregion

        [Authorize]
        [HttpPost]
        public dynamic getUserWalletBalance()
        {
            ResultObject<dynamic> res = new ResultObject<dynamic>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                res = _IApiMgr.Get_user_wallet_balance("userwalletbal", long.Parse(userid));
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.balance,
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        //public dynamic addBet(Tbl_Bet_Details data)
        public dynamic addBet(EncryptPostData encryptdata)
        {
            ResultObject<ViewBetDtails> res = new ResultObject<ViewBetDtails>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Tbl_Bet_Details data = JsonConvert.DeserializeObject<Tbl_Bet_Details>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.addBet("addBet", data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic viewMyBet()
        {
            ResultObject<List<ViewBetDtails>> res = new ResultObject<List<ViewBetDtails>>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                res = _IApiMgr.viewBet("viewBet", userid.ToString());
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            live = res.ResultData.Where(x => x.won_status == 0).OrderByDescending(r => r.created_at).ToList(),
                            settled = res.ResultData.Where(x => x.won_status != 0).OrderByDescending(r => r.created_at).ToList(),
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        //[Authorize]
        [HttpPost]
        public dynamic getHomeMatches()
        {
            ResultObject<List<GetMatchModel>> res = new ResultObject<List<GetMatchModel>>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }
                //userid = "2";
                if (userid == null)
                {
                    res = _IApiMgr.GethomeMatches("gethome_matches", "", "");
                }
                else
                {
                    res = _IApiMgr.GethomeMatches("gethome_matches_userId", "", userid);
                }

                if (res.Result == ResultType.Success)
                {
                    List<GetMatchModel> matchLst = new List<GetMatchModel>();
                    try
                    {
                        matchLst.AddRange(res.ResultData.Where(r => r.is_mymatch == true && r.match_status == 1).OrderBy(r => r.match_date_time).ThenBy(r => r.match_time).ToList());
                        matchLst.AddRange(res.ResultData.Where(r => r.is_mymatch == true && r.match_status == 0).OrderBy(r => r.match_date_time).ThenBy(r => r.match_time).ToList());
                        matchLst.AddRange(res.ResultData.Where(r => r.is_mymatch == true && r.match_status == 2).OrderByDescending(r => r.match_date_time).ThenBy(r => r.match_time).ToList());
                    }
                    catch (Exception ex)
                    {
                        matchLst.AddRange(res.ResultData.Where(r => r.is_mymatch == true).OrderByDescending(r => r.match_date).OrderByDescending(r => r.match_status).ThenBy(r => r.match_time).ToList().Take(5));
                    }


                    return new
                    {
                        result = 1,
                        message = "Success",
                        //data = new
                        //{
                        //    live = res.ResultData.Where(r => r.match_status == 1 && r.match_date.Value.Date == DateTime.Now.Date && r.is_mymatch == false).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                        //    upcomming = res.ResultData.Where(r => r.match_status != 1 && r.match_date.Value.Date >= DateTime.Now.Date && r.is_mymatch == false).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                        //    mymatches = res.ResultData.Where(r => r.match_status != 1 && r.is_mymatch == true).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList()
                        //}
                        data = new
                        {
                            live = res.ResultData.Where(r => r.match_status == 1 && r.match_date.Value.Date == DateTime.Now.Date && r.is_mymatch == false).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                            upcomming = res.ResultData.Where(r => r.match_status == 0 && r.is_mymatch == false && Convert.ToDateTime(r.match_date_time) >= DateTime.Now).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                            //mymatches = res.ResultData.Where(r => r.is_mymatch == true).OrderByDescending(r => r.match_date).OrderByDescending(r => r.match_status).ThenBy(r => r.match_time).ToList().Take(5)
                            mymatches = matchLst.ToList().Take(5)
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic getHomeMyMatches(matchPost data)
        {
            ResultObject<List<GetMatchModel>> res = new ResultObject<List<GetMatchModel>>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res.Result = ResultType.Info;
                }
                else
                {
                    res = _IApiMgr.GethomeMatches("gethome_my_matches_userId", "", userid);
                }

                if (res.Result == ResultType.Success)
                {

                    if (data.match_status == 1)
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = res.ResultData.Where(r => r.match_status == 1 && r.match_date.Value.Date == DateTime.Now.Date && r.is_mymatch == true).OrderByDescending(r => r.match_date_time).ThenBy(r => r.match_time).ToList()
                        } as dynamic;
                    }
                    else if (data.match_status == 0)
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = res.ResultData.Where(r => r.match_status == 0 && r.is_mymatch == true && Convert.ToDateTime(r.match_date_time) >= DateTime.Now).OrderByDescending(r => r.match_date_time).ThenBy(r => r.match_time).ToList(),
                        } as dynamic;
                    }
                    else if (data.match_status == 2)
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = res.ResultData.Where(r => r.match_status == 2 && r.is_mymatch == true).OrderByDescending(r => r.match_date).ThenBy(r => r.match_date_time).ToList()
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = "Invalid match status!",
                            data = ""
                        } as dynamic;
                    }
                    //else
                    //{
                    //    return new
                    //    {
                    //        result = 1,
                    //        message = "Success",
                    //        data = new
                    //        {
                    //            live = res.ResultData.Where(r => r.match_status == 1 && r.match_date.Value.Date == DateTime.Now.Date && r.is_mymatch == true).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                    //            upcomming = res.ResultData.Where(r => r.match_status != 1 && r.match_date.Value.Date >= DateTime.Now.Date && r.is_mymatch == true).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                    //            completed = res.ResultData.Where(r => r.match_status == 2 && r.is_mymatch == true).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList()
                    //        }
                    //    } as dynamic;
                    //}

                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        //[Authorize]
        [HttpPost]
        public dynamic getMatchOdds(EncryptPostData encryptdata)
        //public dynamic getMatchOdds(Match_para Match_para)
        {
            ResultObject<List<Tbl_Odds_History>> res = new ResultObject<List<Tbl_Odds_History>>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                res = _IApiMgr.GetMatch_odds("getoddsbymatchid", Match_para.match_id.ToString());
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }



        [Authorize]
        [HttpPost]
        public dynamic walletTopUp(EncryptPostData encryptdata)
        //public dynamic walletTopUp(WalletTopUp data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                WalletTopUp data = JsonConvert.DeserializeObject<WalletTopUp>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.walletTopUp("WalletTopUp", data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Your wallet balance has been credited successfully.",
                        transaction_id = res.ResultData
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic walletTransactionHistory(EncryptPostData encryptdata)
        //public dynamic walletTransactionHistory(WalletTopUp data)
        {
            ResultObject<Wallet_Transaction> res = new ResultObject<Wallet_Transaction>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                WalletTopUp data = JsonConvert.DeserializeObject<WalletTopUp>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.walletTransactionHistory("walletTransactionHistory", data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            totalAvailableCoins = res.ResultData.AvailableCoins,
                            totalBonus = res.ResultData.Bonus,
                            totalEarning = res.ResultData.Earning,
                            totalBetPlaced = res.ResultData.BetsPlaced,
                            BonusDaily = res.ResultData.lstwallet.Where(r => r.typename == "Daily login").ToList(),
                            weeklylogin = res.ResultData.weeklylogins.ToList(),
                            referralEarn = res.ResultData.lstwallet.Where(r => r.typename == "Refer friends").ToList(),
                            quizEarn = res.ResultData.lstwallet.Where(r => r.typename == "Quiz").ToList(),
                            Earninglist = res.ResultData.lstwallet.OrderByDescending(r => r.created_at).Where(r => r.typename == "Earnings" || r.typename == "Bet void").ToList(),
                            BetPlaced = res.ResultData.lstwallet.Where(r => r.typename == "Bet Placed").ToList(),

                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "No Data Found",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [HttpPost]
        // public dynamic getMatchDetails(EncryptPostData encryptdata)
        public dynamic getMatchDetails(Match_para Match_para)
        {
            ResultObject<List<GetMatchModel>> res = new ResultObject<List<GetMatchModel>>();
            try
            {
                // string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                //var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                //IList<Claim> claim = idnetity.Claims.ToList();
                //string userid = null;
                //if (claim.Count > 0)
                //{
                //    userid = claim[0].Value;
                //}

                //if (userid == null)
                //{
                //    res = _IApiMgr.GethomeMatches("gethome_matchesById", Match_para.match_id.ToString(), "");
                //}
                //else
                //{
                //    res = _IApiMgr.GethomeMatches("gethome_matchesByUserId", Match_para.match_id.ToString(), userid);
                //}

                res = _IApiMgr.GethomeMatches("gethome_matchesById", Match_para.match_id.ToString(), "");
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.FirstOrDefault()
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [HttpPost]
        // public dynamic getMatchContest(EncryptPostData encryptdata)
        public dynamic getMatchContest(Match_para Match_para)
        {
            ResultObject<contestModelallData> res = new ResultObject<contestModelallData>();
            try
            {
                // string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res = _IApiMgr.GetMatchbyContest("gethome_matche_contests", Match_para.match_id.ToString(), "");
                }
                else
                {
                    res = _IApiMgr.GetMatchbyContest("gethome_matche_contests_byUser", Match_para.match_id.ToString(), userid);
                }

                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            contestList = res.ResultData.contestList.ToList(),
                            contestCount = res.ResultData.contestCount,
                            packCount = res.ResultData.packCount
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic getMyMatchContest(Match_para Match_para)
        {
            ResultObject<contestModelallData> res = new ResultObject<contestModelallData>();
            try
            {
                // string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res.Result = ResultType.Info;
                }
                else
                {
                    res = _IApiMgr.GetMatchbyContest("get_match_contests_byuser", Match_para.match_id.ToString(), userid);
                }


                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            contestList = res.ResultData.contestList.ToList(),
                            contestCount = res.ResultData.contestCount,
                            packCount = res.ResultData.packCount
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic getMyPacks(Match_para Match_para)
        {
            ResultObject<List<GetMyPackModel>> res = new ResultObject<List<GetMyPackModel>>();
            try
            {
                // string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res.Result = ResultType.Info;
                }
                else
                {
                    res = _IApiMgr.getMyPacksByUser("get_all_my_pack_by_userid", Match_para.match_id.ToString(), userid);
                }


                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.ToList()
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [HttpPost]
        public dynamic getPacksStaticData(Match_para Match_para)
        {
            ResultObject<List<GetPackDataDetails>> res = new ResultObject<List<GetPackDataDetails>>();
            try
            {
                // string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                //var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                //IList<Claim> claim = idnetity.Claims.ToList();
                //string userid = null;
                //if (claim.Count > 0)
                //{
                //    userid = claim[0].Value;
                //}

                //if (userid == null)
                //{
                //    res.Result = ResultType.Info;
                //}
                //else
                //{
                //    res = _IApiMgr.getPacksStaticData("get_packs_static_data_by_match",  Match_para.match_id.ToString(),"");
                //}

                res = _IApiMgr.getPacksStaticData("get_packs_static_data_by_match", Match_para.match_id.ToString(), "");


                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.FirstOrDefault()
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        public async Task<string> uploadProfile([FromForm] FIleUploadAPI objfile)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            Usersdetails data = new Usersdetails();
            if (objfile.files.Length > 0)
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                try
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\uploads\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\uploads\\");
                    }
                    using (FileStream filestream = System.IO.File.Create(_environment.WebRootPath + "\\uploads\\" + userid + "_" + objfile.files.FileName))
                    {
                        objfile.files.CopyTo(filestream);
                        filestream.Flush();
                        data.profile_picture = "uploads/" + userid + "_" + objfile.files.FileName;
                        res = _IApiMgr.signup("editProfile", data);
                        return data.profile_picture;
                    }
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "Unsuccessful";
            }

        }


        #region Email Verified

        [HttpPost]
        public dynamic emailVerify(emailVerifyPost data)
        //public dynamic emailVerify(EncryptPostData encryptdata)
        {
            string uid = "error";
            string result = "";
            try
            {
                //JObject decryptdata = JObject.Parse(AESEncryption.DecryptStringAES(encryptdata.hdndata));
                //string accessKey = Convert.ToString(decryptdata.SelectToken("accessKey"));

                string accessKey = Convert.ToString(data.accessKey);
                string encryp = accessKey;
                accessKey = accessKey.Replace('_', '+');
                accessKey = accessKey.Replace('~', '/');
                uid = Encryption.DecryptString(accessKey, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                if (genDate > DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updatePassword("emailVerify", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "Email Verfied Successfully";
                        result = "1";
                    }
                    else if (res.ResultData == "2")
                    {
                        uid = "Email link expired";
                        result = "0";
                    }
                    else
                    {
                        uid = "Email link expired";
                        result = "0";
                    }
                }
                else
                {
                    uid = "Email link expired";
                    result = "1";
                }
                return new
                {
                    result = result,
                    message = uid
                } as dynamic;

            }
            catch (Exception ex)
            {
                result = "0";
                uid = "Email link expired";
                return new
                {
                    result = result,
                    message = uid
                } as dynamic;
            }
        }

        public void verifyEmailsent(string userid, string name, string email)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {

                //  string idData = res.ResultData.ToString() + "/" + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                string idData = userid + "/" + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                string encryplink = Encryption.EncryptString(idData, Encryption.passkey);
                encryplink = encryplink.Replace('+', '_');
                encryplink = encryplink.Replace('/', '~');
                //string url = "https://sports.cards/email-verification?accessKey=" + encryplink;
                // string url = "http://13.126.154.20/email-verification?accessKey=" + encryplink;
                string originurl = _iConfig.GetConnectionString("FrontOrigin");
                string url = originurl + "email-verification?accessKey=" + encryplink;
                string body = "Hello " + name + " <br/><br/>" +
               "You are receiving this email because your email id is register for your account. <br/>" +
              "Please click on the below link to verify your email <br/><br/>" +
                url + " <br/><br/>" +
                "Thank you  <br/>" +
                "Team Sports.Cards  <br/><br/>" +
               "Copyright © 2020 Sports.Cards, All rights reserved.";

                string subject = "Email Verification";
                Task.Factory.StartNew(() => SentEmail.SendEmail_aws(body, subject, SentEmail.fromemailtest, email));

                //res = _IApiMgr.updateemaillink("updateemaillink", email, userid, url);
                res = _IApiMgr.updateemaillink("updateemaillink", email, userid, encryplink);

            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
                LogUtil.MakeErrorLog(string.Format("{0} resetpassword ", ""), ex);
            }

        }

        [HttpPost]
        //public dynamic referralEmail(signin data)
        public dynamic referralEmail(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                signin data = JsonConvert.DeserializeObject<signin>(decryptdata);

                ResultObject<string> res = new ResultObject<string>();
                res = _IApiMgr.updateemaillink("checkEmail", "", data.email, "");
                if (res.Result == ResultType.Success)
                {
                    string body = "Hello " + data.email + "  <br/>  <br/>" +
                       "Come join me in the fantasy world of our favourite game - CRICKET, where you can bet & vote for you   <br/>" +
                       "favourite team, play online quizzes, and earn lots of coins. Let’s multiply the fun by playing together!  <br/> <br/>" +
                         data.url + " <br/> <br/>" +
                       "Thank you  <br/>" +
                       "Team Sports.Cards.   <br/> <br/>" +
                       "Copyright © 2020 Sports.Cards., All rights reserved.";

                    string subject = "Sports Cards Referral";
                    SentEmail.SendEmail_aws(body, subject, SentEmail.fromemailtest, data.email);
                    return new
                    {
                        result = 1,
                        message = "Success",
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }

            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                } as dynamic;
            }
        }

        #endregion

        [Authorize]
        [HttpPost]
        public dynamic getUserQuiz()
        {
            ResultObject<List<Tbl_Quiz>> res = new ResultObject<List<Tbl_Quiz>>();
            try
            {
                List<Tbl_Quiz> data = new List<Tbl_Quiz>();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                res = _IApiMgr.getQuiz("getUserQuiz", userid.ToString(), data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.Select(x => new
                        {
                            x.question_id,
                            x.question,
                            x.option1,
                            x.option2,
                            x.option3,
                            x.option4
                        }
                        )

                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "User Already Played",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic submitQuiz(EncryptPostData encryptdata)
        //public dynamic submitQuiz(Tbl_Quiz dataEnc)
        {
            ResultObject<List<Tbl_Quiz>> res = new ResultObject<List<Tbl_Quiz>>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Tbl_Quiz dataEnc = JsonConvert.DeserializeObject<Tbl_Quiz>(decryptdata);

                List<Tbl_Quiz> data = JsonConvert.DeserializeObject<List<Tbl_Quiz>>(dataEnc.quiz_data);
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                res = _IApiMgr.getQuiz("submitQuiz", userid.ToString(), data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.Select(x => new
                        {
                            x.question_id,
                            correctAnswer = x.answer,
                            creditedCoin = Convert.ToInt32(x.option1)
                        }
                        ).ToList()

                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        private string GenerateJSONWebToken(Usersdetails userdata)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_iConfig["Jwt:Key"]));
            var credintials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Sub, userdata.user_id.ToString()),
                //new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Email, userdata.email),
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(
                issuer: _iConfig["Jwt:Issuer"],
                audience: _iConfig["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddDays(30),
                signingCredentials: credintials
                );
            var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodetoken;
        }

        [Authorize]
        [HttpPost]
        public dynamic getUserWallet()
        {
            ResultObject<UserWalletApi> resUserWallet = new ResultObject<UserWalletApi>();
            try
            {

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                resUserWallet = _IApiMgr.getUserWallet("userWallet", userid);
                if (resUserWallet.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = resUserWallet.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic getWalletTransaction()
        {
            ResultObject<List<WalletTransactiontApi>> resTrx = new ResultObject<List<WalletTransactiontApi>>();
            try
            {
                WalletTopUp data = new WalletTopUp();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                //userid = "34";
                resTrx = _IApiMgr.getTransaction("viewTransaction", userid.ToString(), data);
                //resTrx = _IApiMgr.getTransaction("viewTransaction", "3", data);
                if (resTrx.Result == ResultType.Success)
                {
                    var datedist = resTrx.ResultData.Select(x => x.created_at.Date.ToString("yyyy-MM-dd")).Distinct();

                    List<WalletTransactiontApi> lstsymboldata = new List<WalletTransactiontApi>();

                    Dictionary<string, List<WalletTransactiontApi>> SetPiese = new Dictionary<string, List<WalletTransactiontApi>>();

                    foreach (var item in datedist)
                    {
                        lstsymboldata = resTrx.ResultData.Where(r => r.created_at.Date.ToString("yyyy-MM-dd") == item).ToList();
                        SetPiese.Add(item.ToString(), lstsymboldata);
                    }

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = SetPiese
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic submitPack(postPackData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;

                if (data.list.Count() <= 11)
                {
                    string checkcredit = "";
                    decimal creditsck = data.list.Sum(x => x.credit);
                    if (creditsck <= 100)
                    {
                        for (int i = 0; i < data.list.Count(); i++)
                        {
                            if ((data.list[i].credit > 25))
                            {
                                checkcredit = "25";
                                break;
                            }
                        }

                        if (checkcredit == "25")
                        {
                            return new
                            {
                                result = 0,
                                message = "Invalid Data",
                                data = ""
                            } as dynamic;
                        }
                        else
                        {
                            res = _IApiMgr.submitPack("addPack", userid.ToString(), data);
                            if (res.Result == ResultType.Success)
                            {
                                return new
                                {
                                    result = 1,
                                    message = "Success",
                                    data = res.ResultData
                                } as dynamic;
                            }
                            else
                            {
                                return new
                                {
                                    result = 0,
                                    message = "Insufficient Fund",
                                    data = ""
                                } as dynamic;
                            }
                        }
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = "Invalid Data",
                            data = ""
                        } as dynamic;
                    }
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }

        }


        [Authorize]
        [HttpPost]
        //public dynamic addCash(EncryptPostData encryptdata)
        public dynamic addCash(WalletTopUp data)
        {
            ResultObject<Tbl_CashfreeUser> res = new ResultObject<Tbl_CashfreeUser>();
            try
            {
                //string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //WalletTopUp data = JsonConvert.DeserializeObject<WalletTopUp>(decryptdata);


                //var orderDecryp1 = AESEncryption.EncryptStringToBytes("5");
                //string b64ciphertext1 = Convert.ToBase64String(orderDecryp1);
                //var a = AESEncryption.DecryptStringAES(b64ciphertext1);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.addCashFree("addCashApi", "", data);
                if (res.Result == ResultType.Success)
                {
                    var appId = _iConfig.GetConnectionString("appid");
                    var secretKey = _iConfig.GetConnectionString("secretkey");


                    var UrlSet = _iConfig.GetConnectionString("UrlSet");
                    string returnUrl = "";
                    string notifyUrl = "";
                    if (UrlSet == "0")
                    {
                        returnUrl = _iConfig.GetConnectionString("returnUrl");
                        notifyUrl = _iConfig.GetConnectionString("notifyUrl");

                    }
                    else if (UrlSet == "1")
                    {
                        returnUrl = _iConfig.GetConnectionString("returnUrlStag");
                        notifyUrl = _iConfig.GetConnectionString("notifyUrlStag");
                    }
                    else if (UrlSet == "2")
                    {
                        returnUrl = _iConfig.GetConnectionString("returnUrlPrd");
                        notifyUrl = _iConfig.GetConnectionString("notifyUrlPrd");
                    }


                    var orderDecryp = AESEncryption.EncryptStringToBytes(res.ResultData.cashfree_order_id.ToString());
                    string b64ciphertext = Convert.ToBase64String(orderDecryp);
                    notifyUrl = notifyUrl + b64ciphertext;
                    returnUrl = notifyUrl;

                    var createOrder = _iConfig.GetConnectionString("createOrder");

                    JObject orderData = new JObject();

                    using (var client = new WebClient())
                    {
                        var values = new NameValueCollection();
                        values["appId"] = appId;
                        values["secretKey"] = secretKey;
                        values["returnUrl"] = returnUrl;
                        values["notifyUrl"] = notifyUrl;
                        values["orderId"] = res.ResultData.cashfree_order_id;
                        values["orderAmount"] = res.ResultData.cashfree_amount.ToString();
                        values["orderCurrency"] = "INR";
                        values["orderNote"] = "";
                        values["customerEmail"] = res.ResultData.email;
                        values["customerName"] = res.ResultData.first_name;
                        values["customerPhone"] = res.ResultData.mobile;
                        var response = client.UploadValues(createOrder, values);
                        var responseString = Encoding.Default.GetString(response);
                        orderData = JObject.Parse(responseString);
                    }

                    if ((Convert.ToString(orderData.SelectToken("status"))) == "OK")
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = Convert.ToString(orderData.SelectToken("paymentLink"))
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = Convert.ToString(orderData.SelectToken("reason"))
                        } as dynamic;
                    }
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        public dynamic saveTransaction(orderCreate data1)
        {
            ResultObject<string> res = new ResultObject<string>();
            WalletTopUp data = new WalletTopUp();

            var idnetity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = idnetity.Claims.ToList();
            var userid = claim[0].Value;
            data.user_id = long.Parse(userid);
            try
            {
                var appId = _iConfig.GetConnectionString("appid");
                var secretKey = _iConfig.GetConnectionString("secretkey");
                var orderStatusApi = _iConfig.GetConnectionString("orderStatus");

                JObject orderData = new JObject();
                using (var client = new WebClient())
                {
                    var values = new NameValueCollection();
                    values["appId"] = appId;
                    values["secretKey"] = secretKey;
                    values["orderId"] = data1.orderId.ToString();
                    var response = client.UploadValues(orderStatusApi, values);
                    var responseString = Encoding.Default.GetString(response);
                    orderData = JObject.Parse(responseString);
                }
                data.orderId = data1.orderId.ToString();
                data.amount = Convert.ToDecimal(orderData.SelectToken("orderAmount"));
                data.txStatus = Convert.ToString(orderData.SelectToken("txStatus"));
                data.referenceId = Convert.ToString(orderData.SelectToken("referenceId"));
                data.paymentDetails = Convert.ToString(orderData.SelectToken("paymentDetails"));
                if ((Convert.ToString(orderData.SelectToken("txStatus"))) == "SUCCESS")
                {
                    res = _IApiMgr.walletTopUp("addCashVerify", data);
                    if (res.Result == ResultType.Success)
                    {
                        return new
                        {
                            result = 1,
                            message = "SUCCESS",
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = "You Top-Up Already Added.",
                        } as dynamic;
                    }
                }
                else if ((Convert.ToString(orderData.SelectToken("txStatus"))) == "FAILED")
                {
                    res = _IApiMgr.walletTopUp("addCashVerify", data);
                    if (res.Result == ResultType.Success)
                    {
                        return new
                        {
                            result = 0,
                            message = "FAILED",
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = "You Top-Up Already Added.",
                        } as dynamic;
                    }
                }
                else if ((Convert.ToString(orderData.SelectToken("txStatus"))) == "PENDING")
                {
                    res = _IApiMgr.walletTopUp("addCashVerify", data);
                    if (res.Result == ResultType.Success)
                    {
                        return new
                        {
                            result = 0,
                            message = "PENDING",
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = "You Top-Up Already Added.",
                        } as dynamic;
                    }
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "FAILED",
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "FAILED",
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic packContestMapping(List<packContestMapData> data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;

                res = _IApiMgr.packContestMapping("packContestMapping", userid.ToString(), data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Insufficient Fund",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }

        }


        [HttpPost]
        // public dynamic getMatchContestDetails(EncryptPostData encryptdata)
        public dynamic getContestDetails(Match_para Match_para)
        {
            ResultObject<contestDeailsandLeaderboard> res = new ResultObject<contestDeailsandLeaderboard>();
            try
            {
                // string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                //if (userid == null)
                //{
                //    res.Result = ResultType.Info;
                //    //res = _IApiMgr.GetMatchbyContest("gethome_matche_contests", Match_para.match_id.ToString(), "");
                //}
                //else
                //{
                res = _IApiMgr.contesDetails("get_ContesList", Match_para.contest_id.ToString(), "");
                //}

                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            contestDetails = res.ResultData.getContestModel,
                            leaderboardData = res.ResultData.leaderboardModels.ToList()
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [HttpPost]
        // public dynamic getPackDetails(EncryptPostData encryptdata)
        public dynamic getPackDetails(Match_para Match_para)
        {
            ResultObject<List<GetMyPackDetails>> res = new ResultObject<List<GetMyPackDetails>>();
            try
            {
                // string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res.Result = ResultType.Info;
                }
                else
                {
                    res = _IApiMgr.getPackDetails("get_PackDetails", Match_para.pack_id.ToString(), userid);
                }

                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        //data = res.ResultData
                        data = new
                        {
                            total_potential_payout = res.ResultData.Select(x => x.total_potential_payout).FirstOrDefault(),
                            final_potential_payout = res.ResultData.Select(x => x.final_potential_payout).FirstOrDefault(),
                            selection_total = res.ResultData.Count(),
                            totalCredits = res.ResultData.Sum(x => x.credits),
                            pack_name = res.ResultData.Select(x => x.pack_name).FirstOrDefault(),
                            list = res.ResultData.Select(x => new
                            {
                                x.selection_name,
                                x.multipliers,
                                x.credits,
                                x.market_name,
                                x.won_status
                            })

                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        public dynamic showWinner()
        {
            ResultObject<GetWinnerModel> res = new ResultObject<GetWinnerModel>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res.Result = ResultType.Info;
                }
                else
                {
                    res = _IApiMgr.showWinner("showWinner", userid, "", "");
                }

                if (res.Result == ResultType.Success)
                {
                    List<GetWinnerModelNew> winners = new List<GetWinnerModelNew>();
                    foreach (GetWinnerModel1 _Contestdata in res.ResultData.GetWinnerModel1)
                    {
                        GetWinnerModelNew getWinner = new GetWinnerModelNew();
                        getWinner.WinnerModel = new List<GetWinnerModel2>();
                        getWinner.contestData = _Contestdata;
                        getWinner.WinnerModel.AddRange(res.ResultData.GetWinnerModel2.Where(x => x.match_contest_id == _Contestdata.match_contest_id).OrderBy(x => x.rank).ToList().Take(5));
                        winners.Add(getWinner);
                    }
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = winners.ToList()
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic showWinnerbyContest(Match_para _Para)
        {
            ResultObject<GetWinnerModel> res = new ResultObject<GetWinnerModel>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }
                //userid = "35";
                if (userid == null)
                {
                    res.Result = ResultType.Info;
                }
                else
                {
                    res = _IApiMgr.showWinner("showWinnerbyContest", userid, "", _Para.contest_id.ToString());
                }

                if (res.Result == ResultType.Success)
                {
                    List<GetWinnerModelNew> winners = new List<GetWinnerModelNew>();
                    foreach (GetWinnerModel1 _Contestdata in res.ResultData.GetWinnerModel1)
                    {
                        GetWinnerModelNew getWinner = new GetWinnerModelNew();
                        getWinner.WinnerModel = new List<GetWinnerModel2>();
                        getWinner.contestData = _Contestdata;
                        getWinner.WinnerModel.AddRange(res.ResultData.GetWinnerModel2.Where(x => x.user_id == Convert.ToInt64(userid)).OrderBy(x => x.rank).ToList());
                        getWinner.WinnerModel.Where(x => x.user_id == Convert.ToInt64(userid)).Select(c => { c.my_record = 1; return c; }).ToList();
                        getWinner.WinnerModel.AddRange(res.ResultData.GetWinnerModel2.Where(x => x.user_id != Convert.ToInt64(userid)).OrderBy(x => x.rank).ToList());
                        winners.Add(getWinner);
                    }


                    // _mappedResultData.Where(x => x.market_id == market_id).Select(c => { c.isWon = data.isWon; return c; }).ToList();
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = winners.FirstOrDefault(),
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        public dynamic sendVerifyMail()
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                Usersdetails data = new Usersdetails();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                //data.user_id = 49;
                res = _IApiMgr.signup("myProfile", data);
                if (res.Result == ResultType.Success)
                {
                    if (res.ResultData.email_is_verified != 1 && res.ResultData.email != null)
                    {

                        ResultObject<string> res1 = new ResultObject<string>();
                        string idData = data.user_id + "/" + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                        string encryplink = Encryption.EncryptString(idData, Encryption.passkey);
                        encryplink = encryplink.Replace('+', '_');
                        encryplink = encryplink.Replace('/', '~');
                        string originurl = _iConfig.GetConnectionString("FrontOrigin");
                        string url = originurl + "email-verification?accessKey=" + encryplink;
                        string body = "Hello " + res.ResultData.first_name + " <br/><br/>" +
                       "You are receiving this email because your email id is register for your account. <br/>" +
                      "Please click on the below link to verify your email <br/><br/>" +
                        url + " <br/><br/>" +
                        "Thank you  <br/>" +
                        "Team Sports.Cards  <br/><br/>" +
                       "Copyright © 2020 Sports.Cards, All rights reserved.";

                        string subject = "Email Verification";
                        string verify = SentEmail.SendEmail_awsVer(body, subject, SentEmail.fromemailtest, res.ResultData.email);
                        //Task<string> verify = SentEmail.SendEmail_awsVer(body, subject, SentEmail.fromemailtest, res.ResultData.email);

                        if (verify.ToString() == "1")
                        {
                            res1 = _IApiMgr.updateemaillink("updateemaillink", res.ResultData.email, data.user_id.ToString(), encryplink);
                            return new
                            {
                                result = 1,
                                message = "Mail sent successfully",
                            } as dynamic;
                        }
                        else
                        {
                            return new
                            {
                                result = 0,
                                message = "Mail not sent successfully",
                            } as dynamic;
                        }
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = "This user already verified",
                        } as dynamic;
                    }

                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        //public dynamic walletWithdraw(EncryptPostData encryptdata)
        public dynamic walletWithdraw(Tbl_Withdrawal_Request data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                //string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Tbl_Withdrawal_Request data = JsonConvert.DeserializeObject<Tbl_Withdrawal_Request>(decryptdata);
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                ResultObject<Usersdetails> resUser = new ResultObject<Usersdetails>();
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res.Result = ResultType.Info;
                }
                else
                {

                    Usersdetails dataUSer = new Usersdetails();
                    dataUSer.user_id = long.Parse(userid);
                    resUser = _IApiMgr.signup("myProfile", dataUSer);

                    data.user_id = long.Parse(userid);
                    res = _IApiMgr.walletWithdraw("addWalletTransaction", data);
                }

                if (res.Result == ResultType.Success)
                {
                    var resData = res.ResultData.Split(',');
                    string otpmessage = "Your OTP to redeem is " + resData[0] + ".Please do not share this one time password with anyone.";
                    SendSms.snsSned(resUser.ResultData.country_code + resUser.ResultData.mobile, otpmessage);
                    return new
                    {
                        result = 1,
                        message = "Success",
                        withdrawal_id = long.Parse(resData[1])
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Insufficient Winning Balance",
                    } as dynamic;
                }

            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        //public dynamic walletWithdraw(EncryptPostData encryptdata)
        public dynamic verifyWithdrawRequest(Tbl_Withdrawal_Request data)
        {
            ResultObject<Tbl_Withdrawal_Request> tblWithdraw = new ResultObject<Tbl_Withdrawal_Request>();
            try
            {
                //string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                //Tbl_Withdrawal_Request data = JsonConvert.DeserializeObject<Tbl_Withdrawal_Request>(decryptdata);
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                    data.user_id = long.Parse(userid);
                }

                if (userid == null)
                {
                    tblWithdraw.Result = ResultType.Info;
                }
                else
                {
                    //tblWithdraw = _IApiMgr.getWithdrawal_Request("withDrawOtpverify", userid, data.otp.ToString());
                    tblWithdraw = _IApiMgr.getWithdrawal_Request("withDrawOtpverify", data);
                }
                if (tblWithdraw.Result == ResultType.Success)
                {
                    if (tblWithdraw.ResultData.batchFormat == "Failure")
                    {
                        return new
                        {
                            result = 0,
                            message = "Insufficient Winning Balance",
                        } as dynamic;
                    }
                    else
                    {
                        if (tblWithdraw.ResultData.otp_is_verified != 1)
                        {
                            ResultObject<Tbl_Access_Token> tokenData = new ResultObject<Tbl_Access_Token>();
                            tokenData = _IApiMgr.getAccesTokenData("getAccesstoken", "");
                            string latestToken = tokencheck(tokenData.ResultData);
                            if (latestToken == "error")
                            {
                                return new
                                {
                                    result = 0,
                                    message = "Problem in Payment System, Try again letter",
                                } as dynamic;
                            }
                            else
                            {
                                batch batchjson = new batch();
                                Tbl_Withdrawal_RequestJson jsonformat = new Tbl_Withdrawal_RequestJson();
                                jsonformat.batch = new List<batch>();
                                int r = (new Random()).Next(100, 1000);
                                string randomNumer = r.ToString("000");

                                string batchTran_Id = tblWithdraw.ResultData.name.Trim().Replace(" ", "-");

                                jsonformat.batchTransferId = batchTran_Id + "-" + tblWithdraw.ResultData.withdrawal_id + "-" + randomNumer;
                                jsonformat.batchFormat = "BANK_ACCOUNT";

                                batchjson.amount = tblWithdraw.ResultData.amount;
                                batchjson.transferId = tblWithdraw.ResultData.withdrawal_id + randomNumer;
                                batchjson.remarks = "Transfer with Id " + batchjson.transferId;
                                batchjson.name = tblWithdraw.ResultData.name;
                                batchjson.email = tblWithdraw.ResultData.email;
                                batchjson.phone = tblWithdraw.ResultData.phone;
                                batchjson.bankAccount = tblWithdraw.ResultData.bank_account;
                                batchjson.ifsc = tblWithdraw.ResultData.ifsc;

                                jsonformat.batch.Add(batchjson);

                                string requestTransferJson = JsonConvert.SerializeObject(jsonformat);
                                string requestTransferApi = requestBatchTransferApi(requestTransferJson, latestToken);
                                var requestTransferResponseJson = JObject.Parse(requestTransferApi);
                                ResultObject<string> resTok = new ResultObject<string>();
                                if (Convert.ToString(requestTransferResponseJson.SelectToken("status")) == "SUCCESS")
                                {
                                    resTok = _IApiMgr.updateToken("updateRequBatchSuccess", jsonformat.batchTransferId, "USP_Wallet_Payout", batchjson.transferId, tblWithdraw.ResultData.withdrawal_id.ToString(), batchjson.remarks);
                                    if (resTok.Result == ResultType.Success)
                                    {
                                        return new
                                        {
                                            result = 1,
                                            message = "Thank you for submiting your withdrawal request",
                                        } as dynamic;
                                    }
                                    else
                                    {
                                        return new
                                        {
                                            result = 0,
                                            message = "Failure",
                                        } as dynamic;
                                    }

                                }
                                else
                                {
                                    resTok = _IApiMgr.updateToken("updateRequBatchSuccessFailure", jsonformat.batchTransferId, "USP_Wallet_Payout", batchjson.transferId, tblWithdraw.ResultData.withdrawal_id.ToString(), batchjson.remarks);
                                    return new
                                    {
                                        result = 0,
                                        message = "Problem in Payment System, Try again letter",
                                    } as dynamic;
                                }
                            }
                        }
                        else
                        {
                            return new
                            {
                                result = 0,
                                message = "OTP Already verified of Withdraw Request.",
                            } as dynamic;
                        }

                    }
                }
                else if (tblWithdraw.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Incorrect verification code.",
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                } as dynamic;
            }
        }

        public string tokencheck(Tbl_Access_Token tokenData)
        {
            string token = "";
            string response = "";
            string AuthTokenApi = _iConfig.GetConnectionString("verifyToken");
            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            using (var client = new HttpClient(handler))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + tokenData.payout_token);
                var data = new Dictionary<string, string> { };
                var content = new FormUrlEncodedContent(data);
                var responseTask = client.PostAsync(AuthTokenApi, content);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    response = readTask.Result.ToString();
                }
                client.Dispose();
            }
            handler.Dispose();
            var tokenJson = JObject.Parse(response);
            if (Convert.ToString(tokenJson.SelectToken("status")) == "ERROR")
            {
                HttpClientHandler handlerTok = new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                };
                string authorizeToken = _iConfig.GetConnectionString("authorizeToken");
                using (var client = new HttpClient(handlerTok))
                {
                    var data = new Dictionary<string, string> { };
                    var content = new FormUrlEncodedContent(data);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("X-Client-Id", tokenData.payout_client_id);
                    client.DefaultRequestHeaders.Add("X-Client-Secret", tokenData.payout_client_secret);
                    var responseTask = client.PostAsync(authorizeToken, content);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        response = readTask.Result.ToString();
                    }
                    client.Dispose();
                }
                handlerTok.Dispose();
                var tokenDataJson = JObject.Parse(response);
                if (Convert.ToString(tokenDataJson.SelectToken("status")) == "SUCCESS")
                {
                    ResultObject<string> resTok = new ResultObject<string>();
                    token = Convert.ToString(tokenDataJson.SelectToken("data").SelectToken("token"));
                    resTok = _IApiMgr.updateToken("updatepayout_token", token, "USP_Wallet_Payout", "", "", "");
                    if (resTok.Result != ResultType.Success)
                    {
                        token = "error";
                    }
                }
                else
                {
                    token = "error";
                }
            }
            else if (Convert.ToString(tokenJson.SelectToken("status")) == "SUCCESS")
            {
                token = tokenData.payout_token;
            }
            else
            {
                token = "error";
            }

            return token;
        }


        public string requestBatchTransferApi(string json, string token)
        {
            string response = "";
            string requestBatchTransfer = _iConfig.GetConnectionString("requestBatchTransfer");

            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            using (var client = new HttpClient(handler))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                var model1 = new StringContent(json, Encoding.UTF8, "application/json");
                //var x = "{\"data\":{\"Dest\":\"<<REDACTED>>\"}}";

                //var data = new Dictionary<string, string> { json};
                //var content = new FormUrlEncodedContent(data);
                var responseTask = client.PostAsync(requestBatchTransfer, model1);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    response = readTask.Result.ToString();
                }
                client.Dispose();
            }
            handler.Dispose();

            return response;
        }

        [Authorize]
        [HttpPost]
        public dynamic mapdevicetoken(NotificationUserMapping data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res.Result = ResultType.Info;
                }
                else
                {
                    data.user_id = Convert.ToInt64(userid);
                    res = _IApiMgr.mapdevicetoken("mapdevicetoken", data);
                }
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = res.ResultData
                    } as dynamic;
                }

            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }

        }

    }
}