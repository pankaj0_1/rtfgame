﻿function CSLoaderFadeIn() {
    $('.theme-loader').fadeIn('slow', function () {
    });
}

function CSLoaderFadeOut() {
    $('.theme-loader').fadeOut('slow', function () {
    });
}


function CSShowModel(ModelID) {
    $("#" + ModelID).modal('show');
}

function CSHideModel(ModelID) {
    $("#" + ModelID).modal('hide');
}

function CSAlertMsg(type, msg) {
    Snackbar.show({
        text: msg,
        pos: 'top-center'
    });
}

function CSReloadFunc() {
    location.reload();
}


function CSBindDataTableRemove(tableId) {
    $('#' + tableId).DataTable().destroy();
}

function CSBindingMultiSelectComp1(id, val) {
    $("#" + id).selectpicker({
        noneSelectedText: 'Select',
        noneResultsText: 'No Data matched {0}',
    });
    $("#" + id).selectpicker('val', val);
}

function CSWindowReload() {
    window.location.reload();
}

window.setTitle = (title) => {
    document.title = title;
}

function BindActivitydtexcel(tableId, customBind, DeforderColNo, sorting) {
    $.fn.dataTable.ext.errMode = 'none';

    var table = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [{
                extend: 'csv',
                className: 'btn',
                exportOptions: {
                    columns: [2, 3, 4, 5, 6, 7]
                }
            },
            {
                extend: 'excel',
                className: 'btn',
                exportOptions: {
                    columns: [2, 3, 4, 5, 6, 7]
                }
            }]
        },
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            null,
            null,
            null,// <-- disable sorting for column 3
            null,
            null,
            null,
            null
        ],
        "order": [DeforderColNo, "asc"],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
    });
}

function CSBindDataTableCityWithCheck(tableId, customBind, DeforderColNo, sorting) {

    $.fn.dataTable.ext.errMode = 'none';
    c2 = $('#' + tableId).DataTable({
        columnDefs: [{
            "searchable": false,
            "orderable": false,
            targets: 0,
        }],
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            null,
            null,
            null
        ],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "order": [DeforderColNo, "asc"],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ enteries.",
        },
    });

    c2.on('order.dt search.dt', function () {
        c2.column(1, { search: 'applied', order: 'applied', bSortable: false }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    multiCheck(c2);
}

function getcheckdtvalAll(tableId, val) {
    if (val == false) {
        $('input[type="checkbox"]', '#' + tableId).prop('checked', true);
    }
    else {
        $('input[type="checkbox"]', '#' + tableId).prop('checked', false);
    }
    $(this).toggleClass('allChecked');

}

function changespantext(id, value) {
    $('#' + id).html(value);
}

function getcheckUncheckAll() {
    $("#checkall").prop("checked", false);
}


function cleardatatabel(tableId) {
    $('#' + tableId).DataTable().clear().draw()
}

function faqsort() {
    $("#sortable").sortable({
        update: function (event, ui) {
            var itemIDs = "";
            $("#sortable").find(".tasksingleinline").each(function () {
                var itemid = $(this).attr("data-taskid");
                itemIDs = itemIDs + itemid + ",";
            });
            $.ajax({
                type: 'POST',
                datatype: "json",
                contentType: "application/json",
                url: 'api/NearbyPost/updateFaqSort?itemIDs=' + itemIDs,
                success: function (data) {
                    location.reload();
                }
            })
        }
    })

}


function CSBindDataTableRemoveFaq(tableId) {
    $('#' + tableId).DataTable().destroy();
}

function BindViewMatchdtexcel(tableId, DeforderColNo) {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        //dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [
                {
                    extend: 'csv',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                },
                {
                    extend: 'print',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                }
            ]
        },

        "order": [7, "asc"],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],

        //'columnDefs': [{
        //    'targets': "_all",
        //    'orderable': false
        //}],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
        "lengthMenu": [20, 50,100],
        "pageLength": 20,
        drawCallback: function () {
            $('.t-dot').tooltip({ template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>' })
            $('.dataTables_wrapper table').removeClass('table-striped');
        }
    });
}


function BindViewoddtexcel(tableId, DeforderColNo) {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#' + tableId).DataTable({
        // dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',

        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
        "lengthMenu": [5, 10, 20, 50],
        "pageLength": 5,
    });
}


function BindViewUserdtexcel(tableId, DeforderColNo) {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        //dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [
                {
                    extend: 'csv',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2,4]
                    }
                },
                {
                    extend: 'print',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 4]
                    }
                }
            ]
        },
        "aoColumns": [
            null,
            null,
            null,
            { "bSortable": false },
            null,
            null
        ],
        //"order": [DeforderColNo, "desc"],
        'order': [],
        'columnDefs': [{ orderable: false, targets: [0] }],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
        "lengthMenu": [20, 50,100],
        "pageLength": 20,
        drawCallback: function () {
            $('.t-dot').tooltip({ template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>' })
            $('.dataTables_wrapper table').removeClass('table-striped');
        }
    });
}


function BindViewTrxdtexcel(tableId, DeforderColNo) {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        //dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [
                {
                    extend: 'csv',
                    className: 'btn',
                    charset: 'UTF-8',
                    bom: true,
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    }
                },
                {
                    extend: 'print',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    }
                }
            ]
        },

        //"order": [DeforderColNo, "desc"],
        "order": [],
        "columnDefs": [{ orderable: false, targets: [0] }],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
        "lengthMenu": [5, 10, 20, 50],
        "pageLength": 5,
        //drawCallback: function () {
        //    $('.t-dot').tooltip({ template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>' })
        //    $('.dataTables_wrapper table').removeClass('table-striped');
        //}
    });
}

function BindViewBetdtexcel(tableId, DeforderColNo) {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        //dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [
                {
                    extend: 'csv',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4,5,6,7,8,9]
                    }
                },
                {
                    extend: 'print',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                }
            ]
        },

        "order": [DeforderColNo, "asc"],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
        "lengthMenu": [10, 20, 50],
        "pageLength": 10,
        drawCallback: function () {
            $('.t-dot').tooltip({ template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>' })
            $('.dataTables_wrapper table').removeClass('table-striped');
        }
    });
}

function BindViewwtihdrawdtexcel(tableId, DeforderColNo) {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        //dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [
                {
                    extend: 'csv',
                    className: 'btn',
                    charset: 'UTF-8',
                    bom: true,
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                },
                {
                    extend: 'print',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                }
            ]
        },



        //"order": [0, "desc"],
        'order': [],
        'columnDefs': [{ orderable: false, targets: [0] }],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        

        "aoColumns": [
            { "bSortable": false },
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
        "lengthMenu": [20, 50,100],
        "pageLength": 20,
        //drawCallback: function () {
        //    $('.t-dot').tooltip({ template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>' })
        //    $('.dataTables_wrapper table').removeClass('table-striped');
        //}
    });
}
   

function spinOdd() {
    $("#oddsHome").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "20",
        decimals: 2,
        step: 0.01
    });
    $("#oddsAway").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "20",
        decimals: 2,
        step: 0.01
    });
}


function spin() {
    $("#runsHome").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "infnite",
    });
    $("#oversHome").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "20",
        decimals: 1,
        step: 0.1
    });
    $("#wicketHome").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "10",
    });


    $("#runsHome2").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "infnite",
    });
    $("#oversHome2").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "20",
        decimals: 1,
        step: 0.1
    });
    $("#wicketHome2").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "10",
    });
}

function spinValue(spindid) {
    var desc = document.getElementById(spindid).value
    if (desc == "") {
        return "0";
    }
    return desc
}

function destroyspin(id) {
    $('#' + id).trigger('touchspin.destroy');
}

function spin1(id) {
    if (id == "runsHome" || id == "runsHome2") {
        $('#' + id).TouchSpin({
            buttondown_class: "btn btn-classic btn-danger",
            buttonup_class: "btn btn-classic btn-success",
            max: "infnite",
        });
    }
    else if (id == "oversHome" || id == "oversHom2") {
        $('#' + id).TouchSpin({
            buttondown_class: "btn btn-classic btn-danger",
            buttonup_class: "btn btn-classic btn-success",
            max: "20",
            decimals: 1,
            step: 0.1
        });
    }
    else if (id == "wicketHome" || id == "wicketHome2") {
        $('#' + id).TouchSpin({
            buttondown_class: "btn btn-classic btn-danger",
            buttonup_class: "btn btn-classic btn-success",
            max: "10",
        });
    }

}

function spinSuperover() {
    $("#superrunsHome").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "infnite",
    });
    $("#superoversHome").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "20",
        decimals: 1,
        step: 0.1
    });
    $("#superwicketHome").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "10",
    });


    $("#superrunsHome2").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "infnite",
    });
    $("#superoversHome2").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "20",
        decimals: 1,
        step: 0.1
    });
    $("#superwicketHome2").TouchSpin({
        buttondown_class: "btn btn-classic btn-danger",
        buttonup_class: "btn btn-classic btn-success",
        max: "10",
    });
}

function datetimepast() {
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if (month < 10)
        month = '0' + month.toString();
    if (day < 10)
        day = '0' + day.toString();

    var minDate = year + '-' + month + '-' + day;

    $('#matchdate').attr('min', minDate);
}



function CSBindingMultiSelect(id) {
    $("#" + id).selectpicker({
        showTick: true,
        iconBase: 'fa',
        tickIcon: 'fa-check',
        noneSelectedText: '---Select User---',
        noneResultsText: 'No Data matched of {0}',
        size: '8'
    });
}


function CSBindingMultiSelecttext(id,text) {
    $("#" + id).selectpicker({
        showTick: true,
        iconBase: 'fa',
        tickIcon: 'fa-check',
        noneSelectedText: text,
        noneResultsText: 'No Data matched of {0}',
        size: '8'
    });
}


function ddl(id) {
    var selectedValues = $("#" + id).val();
    return selectedValues.join(',');
}


function multiselecthide(id) {
    document.getElementById("attid").style.display = 'none';
}

function multiselectshow(id) {
    document.getElementById("attid").style.display = 'inline';
}

function setddl(id, val) {
    $("#" + id).selectpicker('val', val);
}

function CSBindDataTableFaq(tableId, customBind, DeforderColNo, sorting) {

    $.fn.dataTable.ext.errMode = 'none';
    c2 = $('#' + tableId).DataTable({
        columnDefs: [{
            "searchable": false,
            "orderable": false,
            targets: 0,
        }],
        "aoColumns": [
            { "bSortable": false },
          null,
            null,
            null,
            null
        ],
        "order": [DeforderColNo, "asc"],
        //'order': [],
        //'columnDefs': [{ orderable: false, targets: [0] }],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        //"order": [DeforderColNo, "asc"],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ enteries.",
        },
    });

    c2.on('order.dt search.dt', function () {
        c2.column(0, { search: 'applied', order: 'applied', bSortable: false }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    //multiCheck(c2);
}



function CSBindDataTableQA(tableId, customBind, DeforderColNo, sorting) {
    $.fn.dataTable.ext.errMode = 'none';
    c2 = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [
                {
                    extend: 'csv',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2]
                    }
                },
                {
                    extend: 'print',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2]
                    }
                }
            ]
        },


        columnDefs: [{
            "searchable": false,
            "orderable": false,
            targets: 0,
        }],
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            null
        ],
        "order": [DeforderColNo, "asc"],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ .",
        },
        "stripeClasses": [],
        "lengthMenu": [20, 50, 100],
        "pageLength": 20,
    });

    c2.on('order.dt search.dt', function () {
        c2.column(0, { search: 'applied', order: 'applied', bSortable: false }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    //multiCheck(c2);
}



function CSBindDataTableContest(tableId, customBind, DeforderColNo, sorting) {
    $.fn.dataTable.ext.errMode = 'none';
    c2 = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [
                {
                    extend: 'csv',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2,3,4,5,6,7]
                    }
                },
                {
                    extend: 'print',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7]
                    }
                }
            ]
        },


        columnDefs: [{
            "searchable": false,
            "orderable": false,
            targets: 0,
        }],
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            null
        ],
        "order": [DeforderColNo, "asc"],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ .",
        },
        "stripeClasses": [],
        "lengthMenu": [20, 50, 100],
        "pageLength": 20,
    });
}


function CSBindTermsCkeditor() {
    CKEDITOR.config.removeButtons =
        'Save,NewPage,Preview,Print,PasteFromWord,PasteText,Paste,Copy,Cut,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Templates,Checkbox,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles';
    CKEDITOR.replace('shortdescription');
    CKEDITOR.replace('fulldescription');
    CKEDITOR.add
}

function getEditor(id) {
    var desc = CKEDITOR.instances[id].getData();
    return desc
}

function CSBindCmsCkeditor() {
    if (CKEDITOR.instances['contentval']) CKEDITOR.instances['contentval'].destroy();
    CKEDITOR.config.removeButtons =
        'Save,NewPage,Preview,Print,PasteFromWord,PasteText,Paste,Copy,Cut,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Templates,Checkbox,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles';
    CKEDITOR.replace('contentval');
    CKEDITOR.add
}



function CSBindFaqCkeditor1() {
    $(window).on('load', function () {
        $('#ckeditor-textarea').ckeditor();
    });
    CKEDITOR.config.removeButtons =
        'Save,NewPage,Preview,Print,PasteFromWord,PasteText,Paste,Copy,Cut,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Templates,Checkbox,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles';
    CKEDITOR.replace('contentval');
    CKEDITOR.add
}


function CSBindFaqCkeditor() {
    if (CKEDITOR.instances['contentval']) CKEDITOR.instances['contentval'].destroy();
    CKEDITOR.config.removeButtons =
        'Save,NewPage,Preview,Print,PasteFromWord,PasteText,Paste,Copy,Cut,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Templates,Checkbox,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles';
    CKEDITOR.replace('contentval');
    CKEDITOR.add
}

function isNumberKey() {
    $("#digit").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errormsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
}

function isDecimalKey() {
    $(document).on('keydown', 'input[pattern]', function (e) {
        var input = $(this);
        var oldVal = input.val();
        var regex = new RegExp(input.attr('pattern'), 'g');

        setTimeout(function () {
            var newVal = input.val();
            if (!regex.test(newVal)) {
                input.val(oldVal);
            }
        }, 0);
    });
}


function checkSelected(selectid,ids) {
    $("#" + selectid).selectpicker('val', ids);
}

function BindViewPackdtexcel(tableId, DeforderColNo) {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [
                {
                    extend: 'csv',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    }
                },
                {
                    extend: 'print',
                    className: 'btn',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    }
                }
            ]
        },

        "order": [DeforderColNo, "asc"],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            //"sInfo": "Showing page PAGE of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  MENU ",
        },
        "stripeClasses": [],
        "lengthMenu": [10, 20, 50],
        "pageLength": 10,
    });
}

function CSDynamicBindDDlWinner() {
    $(".iswonclass").on('click', function () {
        var aid = this.getAttribute("myattr");
        $(".tempselectionddl_" + aid).prop("disabled", false);
        $(".tempselectionddl_" + aid).selectpicker('refresh');
       // $(".tempselectionddl_" + aid).find('option:eq(0)').prop('selected', true);
       // $(".tempselectionddl_" + aid).prop("disabled", false);

        //$(".isvoidclass input:checkbox").attr("checked", false);
        $(this).attr("checked", true);
    });

    $(".isvoidclass").on('click', function () {
        this.getAttribute("myattr");
        var aid = this.getAttribute("myattr");

        $(".tempselectionddl_" + aid).selectpicker('deselectAll');
        $(".tempselectionddl_" + aid).prop("disabled", true);
        $(".tempselectionddl_"+ aid).selectpicker('refresh');

        //$(".tempselectionddl_" + aid).find('option:eq(0)').prop('selected', true);
        //$(".tempselectionddl_" + aid).prop("disabled", true);
    });
}


function CSBindingMultiSelectWinner(id) {
    $("#" + id).selectpicker({
        showTick: true,
        iconBase: 'fa',
        tickIcon: 'fa-check',
        noneSelectedText: '---Choose Selection---',
        noneResultsText: 'No Data matched of {0}',
        size: '8'
    });
    // $("#" + id).selectpicker('val', [1]); 
}



function ddlMarket(id) {
    var selectedValues = $("#" + id).val();
    return selectedValues.join(',,');
}

function bindnotificationDT() {
    table = $('#NotificationTable').DataTable();
}