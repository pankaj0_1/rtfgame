﻿using RTF_BaseObject;
using RTF_BaseObject.RTF;
using RTF_Mgr_Interface.Admin;
using RTF_Utilities;
using RTF_Utilities.Enum;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RTF_BaseObject.RTF_API_Model;

namespace RTF.Services
{
    public class RTFAdmin
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;

        public RTFAdmin(IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IAdminMgr IAdminMgr)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
        }

        public ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data)
        {
            ResultObject<Tbl_Admin_User> res = new ResultObject<Tbl_Admin_User>();
            try
            {
                res = _IAdminMgr.adminLoginAuth(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "adminLoginAuth", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<Tbl_Admin_User> checkLogin()
        {
            ResultObject<Tbl_Admin_User> loginChk = new ResultObject<Tbl_Admin_User>();
            try
            {
                loginChk.ResultData = new Tbl_Admin_User();
                loginChk.ResultData = _httpContextAccessor.HttpContext.Session.GetObject<Tbl_Admin_User>("AuthLoginData");
                loginChk.Result = ResultType.Success;
            }
            catch (Exception ex)
            {
                _httpContextAccessor.HttpContext.Session.Clear();
            }
            if (loginChk.ResultData == null)
            {
                loginChk.Result = ResultType.Error;
            }
            return loginChk;
        }

        public void logOut()
        {
            _httpContextAccessor.HttpContext.Session.Clear();
        }

        public ResultObject<string> addMatch(string RequestType, MatchData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.addMatch(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "addMatch", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Tournaments>> viewTournament(string RequestType, string id)
        {
            ResultObject<List<Tbl_Tournaments>> res = new ResultObject<List<Tbl_Tournaments>>();
            try
            {
                res = _IAdminMgr.viewTournament(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewTournament", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Teams>> viewTeam(string RequestType, string id)
        {
            ResultObject<List<Tbl_Teams>> res = new ResultObject<List<Tbl_Teams>>();
            try
            {
                res = _IAdminMgr.viewTeam(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewTeam", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<MatchData>> viewMatch(string RequestType, string id)
        {
            ResultObject<List<MatchData>> res = new ResultObject<List<MatchData>>();
            try
            {
                res = _IAdminMgr.viewMatch(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewTeam", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<updateScore> viewScore(string RequestType, string data)
        {
            ResultObject<updateScore> res = new ResultObject<updateScore>();
            try
            {
                res = _IAdminMgr.viewScore(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewScore", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updaeScore(string RequestType, updateScore data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updaeScore(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "updaeScore", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updateProfile(RequestType, param1, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "updateProfile", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Users>> viewuser(string RequestType, string id)
        {
            ResultObject<List<Tbl_Users>> res = new ResultObject<List<Tbl_Users>>();
            try
            {
                res = _IAdminMgr.viewuser(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewuser", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Wallet_Transaction_Admin>> viewTransaction(string RequestType, string id)
        {
            ResultObject<List<Tbl_Wallet_Transaction_Admin>> res = new ResultObject<List<Tbl_Wallet_Transaction_Admin>>();
            try
            {
                res = _IAdminMgr.viewTransaction(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewTransaction", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> walletTopUp(string RequestType, WalletTopUpAdd data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.walletTopUp(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "walletTopUp", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updatePassword(string RequestType, string param1, string param2, string param3)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updatePassword(RequestType, param1, param2, param3);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "updatePassword", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> resetpassword(string RequestType, string email)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updatePassword(RequestType, email, "", "");
                if (res.Result == ResultType.Success)
                {
                    //email = "mohammedsajid.v2r@gmail.com";
                    string idData = res.ResultData.ToString() + "/" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string encrypPass = Encryption.EncryptString(idData, Encryption.passkey);
                    encrypPass = encrypPass.Replace('+', '_');
                    encrypPass = encrypPass.Replace('/', '~');
                    string url = "";
                    try
                    {
                        url = _httpContextAccessor.HttpContext.Request.Scheme + "://" + _httpContextAccessor.HttpContext.Request.Host.Value;
                    }
                    catch (Exception ex)
                    {
                        LogUtil.MakeErrorLog(string.Format("{0} resetpassword0 ", ""), ex);
                        url = "http://15.207.57.251:3000/";
                    }

                    string body = "Hello admin!  \n \n" +
                      "You are receiving this email because we recevied a password reset request for your account.\n \n" +
                      "You are just one step away from reset your password. \n" +
                       url + "/resetpassword/" + encrypPass + "\n\n" +
                       "Thank you \n" +
                       "Team Cricket24.Bet. \n" +
                       url + " \n \n" +
                      "Copyright © 2020 Cricket24.Bet., All rights reserved.";

                    string subject = "Reset Password Request";



                    SentEmail.SendEmail_aws(body, subject, SentEmail.fromemailtest, email);
                    //if (check == "1")
                    //{
                    //LogUtil.MakeCustomLog(string.Format("{0} resetpassword ", ""), "6");
                    res.ResultMessage = "You will receive a password reset link at your e-mail address. Please click on that link to reset your password";
                    //}
                    //else
                    //{
                    //    LogUtil.MakeCustomLog(string.Format("{0} resetpassword ", ""), "7");
                    //    res.Result = ResultType.Info;
                    //    res.ResultMessage = "Mail Service not working.";
                    //}
                }
                else
                {
                    res.Result = ResultType.Info;
                    res.ResultMessage = "User with this e-mail address is not registered with Cricket24.Bet.";
                }
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
                LogUtil.MakeErrorLog(string.Format("{0} resetpassword ", ""), ex);
            }
            return res;
        }

        public string ForgotPass(string userID)
        {
            string uid = "error";
            try
            {
                string encryp = userID;
                userID = userID.Replace('_', '+');
                userID = userID.Replace('~', '/');
                uid = Encryption.DecryptString(userID, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                genDate = genDate.AddMinutes(60);
                if (genDate >= DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updatePassword("resetpasswordlinkcheck", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "success";
                    }
                    else
                    {
                        uid = "error";
                    }
                }
                else
                {
                    uid = "error";
                }
            }
            catch (Exception ex)
            {
                uid = "error";
            }
            return uid;
        }

        public string emailVerify(string userID)
        {
            string uid = "error";
            try
            {
                string encryp = userID;
                userID = userID.Replace('_', '+');
                userID = userID.Replace('~', '/');
                uid = Encryption.DecryptString(userID, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                genDate = genDate.AddMinutes(60);
                if (genDate >= DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updatePassword("emailVerify", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "success";
                    }
                    else if (res.ResultData == "2")
                    {
                        uid = "This Email Id Already Active";
                    }
                    else
                    {
                        uid = "error";
                    }
                }
                else
                {
                    uid = "error";
                }
            }
            catch (Exception ex)
            {
                uid = "error";
            }
            return uid;
        }

        public ResultObject<string> updateresetpass(string reqtype, string param1, string param2, string param3)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                string encryp = param3;
                param3 = param3.Replace('_', '+');
                param3 = param3.Replace('~', '/');
                string uid = Encryption.DecryptString(param3, Encryption.passkey);
                string[] idData = uid.Split("/");
                param3 = idData[0];
                res = _IAdminMgr.updatePassword(reqtype, param1, encryp, param3);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<ViewBetDtailsAdmin>> viewBet(string RequestType, string id)
        {
            ResultObject<List<ViewBetDtailsAdmin>> res = new ResultObject<List<ViewBetDtailsAdmin>>();
            try
            {
                res = _IAdminMgr.viewBet(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewBet", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Withdrawal_RequestAdmin>> viewWithdrawal(string RequestType, string id)
        {
            ResultObject<List<Tbl_Withdrawal_RequestAdmin>> res = new ResultObject<List<Tbl_Withdrawal_RequestAdmin>>();
            try
            {
                res = _IAdminMgr.viewWithdrawal(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewWithdrawal", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> updateWirhdrawal(string RequestType, Tbl_Withdrawal_RequestAdmin data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updateWirhdrawal(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "updateWirhdrawal", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<reconcile_amountAdmin> reconcile_amount(string RequestType, string id, string param)
        {
            ResultObject<reconcile_amountAdmin> res = new ResultObject<reconcile_amountAdmin>();
            try
            {
                res = _IAdminMgr.reconcile_amount(RequestType, id, param);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewScore", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<MatchData>> viewMatch(string RequestType, string id, string param, string data, string to)
        {
            ResultObject<List<MatchData>> res = new ResultObject<List<MatchData>>();
            try
            {
                res = _IAdminMgr.viewMatch(RequestType, id, param, data, to);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("RTFAdmin", "viewTeam", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        #region faq
        public ResultObject<List<FaqData>> viewfaq(string req, string param)
        {
            ResultObject<List<FaqData>> res = new ResultObject<List<FaqData>>();
            try
            {
                res = _IAdminMgr.viewfaq(req, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editfaq(string reqtype, string param, FaqData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editfaq(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region cmsMaster
        public ResultObject<List<CmsPageData>> cmsMaster()
        {
            ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
            try
            {
                res = _IAdminMgr.cmsMaster("viewcms", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<CmsPageData>> cmsMasterreq(string reqtype, string param)
        {
            ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
            try
            {
                res = _IAdminMgr.cmsMaster(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> editCmsMaster(string reqtype, string param, CmsPageData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editCmsMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region cmsMaster

        public ResultObject<List<Tbl_QuizAdmin>> quizMaster(string reqtype, string param)
        {
            ResultObject<List<Tbl_QuizAdmin>> res = new ResultObject<List<Tbl_QuizAdmin>>();
            try
            {
                res = _IAdminMgr.quizMaster(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> editQuizMaster(string reqtype, string param, Tbl_QuizAdmin data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editQuizMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region contestMaster

        public ResultObject<List<Tbl_ContestAdmin>> contestData(string reqtype, string param)
        {
            ResultObject<List<Tbl_ContestAdmin>> res = new ResultObject<List<Tbl_ContestAdmin>>();
            try
            {
                res = _IAdminMgr.contestData(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> insertContest(string reqtype, string param, Tbl_ContestAdmin data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.insertContest(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        public ResultObject<string> inserMatchtContest(string reqtype, string param, List<Tbl_Match_ContestAdmin> data)
        {
            ResultObject<string> res = new ResultObject<string>();
            ResultObject<StaticLeaderboardModelData> resPlan = new ResultObject<StaticLeaderboardModelData>();
            try
            {
                resPlan = _IAdminMgr.inserMatchtContest(reqtype, param, data);
                res.ResultData = resPlan.ResultData.resultD;
                if (resPlan.ResultData.resultD == "Success")
                {
                    foreach (GetContestModel contestModel in resPlan.ResultData.contestModels.ToList())
                    {
                        List<StaticLeaderboardModel> MapleaderboardModels = new List<StaticLeaderboardModel>();
                        foreach (StaticLeaderboardModel SData in resPlan.ResultData.leaderboardModels.Where(x=>x.contest_ids.Split(",").Contains(contestModel.contest_id.ToString())).ToList())
                        {
                            StaticLeaderboardModel staticLeaderboard = new StaticLeaderboardModel();
                            staticLeaderboard.Ranks_From = SData.Ranks_From;
                            staticLeaderboard.Ranks_To = SData.Ranks_To;
                            staticLeaderboard.Prize_Money_per_person = (SData.per_of_the_pool * contestModel.max_prize) / 100;
                            staticLeaderboard.per_of_the_pool = SData.per_of_the_pool;
                            staticLeaderboard.no_of_ppl_n_rank = SData.no_of_ppl_n_rank;
                            staticLeaderboard.contest_id = contestModel.contest_id;
                            staticLeaderboard.match_contest_id = contestModel.match_contest_id;
                            MapleaderboardModels.Add(staticLeaderboard);
                        }
                        ResultObject<string> res_ = new ResultObject<string>();
                        res_ = _IAdminMgr.CreateLeaderBoardByMatchContest("createLeaderBoardByMatchContest", MapleaderboardModels);
                    }
                }
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Match_ContestAdmin>> MatchtContestData(string reqtype, string param)
        {
            ResultObject<List<Tbl_Match_ContestAdmin>> res = new ResultObject<List<Tbl_Match_ContestAdmin>>();
            try
            {
                res = _IAdminMgr.MatchtContestData(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<PackAdmin>> viewPack(string reqtype, string param)
        {
            ResultObject<List<PackAdmin>> res = new ResultObject<List<PackAdmin>>();
            try
            {
                res = _IAdminMgr.viewPack(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion


        public ResultObject<List<GetPackDataDetailsAdmin>> getPacksStaticData(string RequestType, string matchid)
        {
            ResultObject<List<GetPackDataDetailsAdmin>> res = new ResultObject<List<GetPackDataDetailsAdmin>>();
            try
            {
                res = _IAdminMgr.getPacksStaticData(RequestType, matchid, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        public ResultObject<string> insetMarkeWin(string RequestType, List<MappedResultData> data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.insetMarkeWin(RequestType, data);
                if (res.ResultData.ToLower() == "success")
                {
                    ResultObject<GetMatchResultsStaticDetails> resultObject = new ResultObject<GetMatchResultsStaticDetails>();
                    resultObject = _IAdminMgr.matchResultStaticDetails("GetMatchResultsStaticDetails", data.FirstOrDefault().match_id.ToString(), "");

                    List<staticPackOrderDetailsData> finalSelectedPackorders = new List<staticPackOrderDetailsData>();


                    //for select selection which is mark as won by admin
                    foreach (staticMarketWinnersData winnersData in resultObject.ResultData.staticMarketWinners.Where(x => x.is_won == 1).ToList())
                    {
                        finalSelectedPackorders.AddRange(resultObject.ResultData.staticPackOrdersDetails.Where(x => x.selection_id == winnersData.selection_id).ToList());
                    }

                    //for sum of final_potential_payout by match_contest_ids
                    List<long> match_contest_ids = resultObject.ResultData.staticPackOrders.Select(x => x.match_contest_id).Distinct().ToList();
                    foreach (long mContestId in match_contest_ids.ToList())
                    {
                        List<staticPackOrdersData> finalselectedPacksData = new List<staticPackOrdersData>();
                        List<staticPackOrdersData> finalselectedPacksAndRankData = new List<staticPackOrdersData>();

                        foreach (staticPackOrdersData packData in resultObject.ResultData.staticPackOrders.Where(x => x.match_contest_id == mContestId).ToList())
                        {
                            if (finalSelectedPackorders.Where(x => x.pack_order_id == packData.pack_order_id && x.match_contest_id == mContestId).Sum(x => x.potential_payout) > 0)
                            {
                                staticPackOrdersData ordersData = new staticPackOrdersData();
                                ordersData.match_id = packData.match_id;
                                ordersData.user_id = packData.user_id;
                                ordersData.pack_order_id = packData.pack_order_id;
                                ordersData.match_contest_id = mContestId;
                                ordersData.final_potential_payout = finalSelectedPackorders.Where(x => x.pack_order_id == packData.pack_order_id).Sum(x => x.potential_payout);
                                finalselectedPacksData.Add(ordersData);
                            }
                        }
                        long ranks = 1;
                        //for mark top ranks of packs
                        foreach (staticPackOrdersData rankObj in finalselectedPacksData.OrderByDescending(x => x.final_potential_payout).ToList())
                        {
                            rankObj.rank = ranks;
                            rankObj.won_amount = resultObject.ResultData.leaderboardNmappedContestDatas.Where(x => x.match_contest_id == mContestId && (ranks >= x.Ranks_From && ranks <= x.Ranks_To)).Select(x => x.Prize_Money_per_person).FirstOrDefault();
                            // rankObj.won_amount = resultObject.ResultData.leaderboardNmappedContestDatas.Where(x => x.match_contest_id == mContestId).Where(x=>x.Ranks_From >= ranks).Where(x => x.Ranks_To <=ranks).Select(x => x.Prize_Money_per_person).FirstOrDefault();
                            finalselectedPacksAndRankData.Add(rankObj);
                            ranks++;
                        }

                        ResultObject<string> result = new ResultObject<string>();
                        result = _IAdminMgr.updateRank("updateRank", finalselectedPacksAndRankData);
                        //send object for update final_potential_payout and rank in Tbl_Pack_Orders table 
                        //finalselectedPacksAndRankData
                    }


                }
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<MappedResultData>> viewMarketWinner(string RequestType, string matchid)
        {
            ResultObject<List<MappedResultData>> res = new ResultObject<List<MappedResultData>>();
            try
            {
                res = _IAdminMgr.viewMarketWinner(RequestType, matchid, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> insertAndSendNotification(submitNotificationModal data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                ResultObject<List<NotificationUserMapping>> notifyResult = new ResultObject<List<NotificationUserMapping>>();
                notifyResult.ResultData = new List<NotificationUserMapping>();
                if (data.target_audience == "all")
                {
                    notifyResult = _IAdminMgr.getuserdevicetoken("getallusertoken");
                }
                else if (data.target_audience == "paid_contest_players")
                {
                    notifyResult = _IAdminMgr.getuserdevicetoken("get_paid_contest_players");
                }
                else if (data.target_audience == "unregistered_players")
                {
                    notifyResult = _IAdminMgr.getuserdevicetoken("get_unregistered_players");
                }
                else if (data.target_audience == "free_contest_players")
                {
                    notifyResult = _IAdminMgr.getuserdevicetoken("get_free_contest_players");
                }

                string responseFireBase = ServiceNotification.Push_notification(data.title, data.message, "", "", notifyResult.ResultData.Select(x => x.device_token).ToArray());
                if (responseFireBase == "true")
                {
                    data.notification_success = true;
                    //insert data into db;
                }
                res = _IAdminMgr.InsertNotification("AddNotification", data);
                res.Result = ResultType.Success;


            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<submitNotificationModal>> getNotificationLog()
        {
            ResultObject<List<submitNotificationModal>> res = new ResultObject<List<submitNotificationModal>>();
            try
            {
                res = _IAdminMgr.getNotificationLog("viewNotificationLogs", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

    }
}
