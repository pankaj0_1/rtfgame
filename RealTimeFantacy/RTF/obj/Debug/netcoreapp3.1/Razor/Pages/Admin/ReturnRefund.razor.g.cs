#pragma checksum "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "477e06c540ec3b045ae97652ac3bc1535a43eb68"
// <auto-generated/>
#pragma warning disable 1591
namespace RTF.Pages.Admin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject.PostModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject.RTF;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Pages.Component;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/refundpolicy")]
    public partial class ReturnRefund : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "container term-form");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "col-lg-12 col-12 p-0");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "statbox widget box box-shadow");
            __builder.AddMarkupContent(6, "<div class=\"widget-header\"><div class=\"row\"><div class=\"col-xl-12 col-md-12 col-sm-12 col-12\"><h4>Return & Refund Policy</h4></div></div></div>\r\n            ");
            __builder.OpenElement(7, "div");
            __builder.AddAttribute(8, "class", "widget-content widget-content-area");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(9);
            __builder.AddAttribute(10, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 16 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                  model

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(11, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 16 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                         OnConfirm

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(12, "OnInvalidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 16 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                                                      HandleInvalidSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(13, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(14);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(15, "\r\n                    ");
                __builder2.OpenElement(16, "div");
                __builder2.AddAttribute(17, "class", "modal-body");
                __builder2.OpenElement(18, "div");
                __builder2.AddAttribute(19, "class", "row");
                __builder2.OpenElement(20, "div");
                __builder2.AddAttribute(21, "class", "form-group mb-4 col-lg-4");
                __builder2.AddMarkupContent(22, "<label for=\"cname\">Title<span class=\"astick\">*</span></label>\r\n                                ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(23);
                __builder2.AddAttribute(24, "disabled", true);
                __builder2.AddAttribute(25, "type", "text");
                __builder2.AddAttribute(26, "class", "form-control");
                __builder2.AddAttribute(27, "id", "sname");
                __builder2.AddAttribute(28, "placeholder", "Title");
                __builder2.AddAttribute(29, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 22 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                                  model.page_title

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(30, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.page_title = __value, model.page_title))));
                __builder2.AddAttribute(31, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => model.page_title));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(32, "\r\n                                ");
                __Blazor.RTF.Pages.Admin.ReturnRefund.TypeInference.CreateValidationMessage_0(__builder2, 33, 34, 
#nullable restore
#line 23 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                          () => model.page_title

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(35, "\r\n                            ");
                __builder2.OpenElement(36, "div");
                __builder2.AddAttribute(37, "class", "form-group mb-4 col-lg-4");
                __builder2.AddMarkupContent(38, "<label for=\"cname\">Meta Title<span class=\"astick\">*</span></label>\r\n                                ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(39);
                __builder2.AddAttribute(40, "maxlength", "1000");
                __builder2.AddAttribute(41, "type", "text");
                __builder2.AddAttribute(42, "class", "form-control");
                __builder2.AddAttribute(43, "id", "sname");
                __builder2.AddAttribute(44, "placeholder", "Meta Title");
                __builder2.AddAttribute(45, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 27 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                                          model.page_metatitle

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(46, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.page_metatitle = __value, model.page_metatitle))));
                __builder2.AddAttribute(47, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => model.page_metatitle));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(48, "\r\n                                ");
                __Blazor.RTF.Pages.Admin.ReturnRefund.TypeInference.CreateValidationMessage_1(__builder2, 49, 50, 
#nullable restore
#line 28 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                          () => model.page_metatitle

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(51, "\r\n                            ");
                __builder2.OpenElement(52, "div");
                __builder2.AddAttribute(53, "class", "form-group mb-4 col-lg-4");
                __builder2.AddMarkupContent(54, "<label for=\"cname\">Meta Keywords</label>\r\n                                ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(55);
                __builder2.AddAttribute(56, "maxlength", "1000");
                __builder2.AddAttribute(57, "type", "text");
                __builder2.AddAttribute(58, "class", "form-control");
                __builder2.AddAttribute(59, "id", "sname");
                __builder2.AddAttribute(60, "placeholder", "Meta Keywords");
                __builder2.AddAttribute(61, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 32 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                                          model.page_metakeywords

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(62, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.page_metakeywords = __value, model.page_metakeywords))));
                __builder2.AddAttribute(63, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => model.page_metakeywords));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(64, "\r\n                                ");
                __Blazor.RTF.Pages.Admin.ReturnRefund.TypeInference.CreateValidationMessage_2(__builder2, 65, 66, 
#nullable restore
#line 33 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                          () => model.page_metakeywords

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(67, "\r\n                            ");
                __builder2.OpenElement(68, "div");
                __builder2.AddAttribute(69, "class", "form-group mb-4 col-lg-12");
                __builder2.AddMarkupContent(70, "<label for=\"cname\">Meta Description</label>\r\n                                ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputTextArea>(71);
                __builder2.AddAttribute(72, "maxlength", "1000");
                __builder2.AddAttribute(73, "rows", "6");
                __builder2.AddAttribute(74, "cols", "50");
                __builder2.AddAttribute(75, "type", "text");
                __builder2.AddAttribute(76, "class", "form-control");
                __builder2.AddAttribute(77, "id", "sname");
                __builder2.AddAttribute(78, "placeholder", "Meta Description");
                __builder2.AddAttribute(79, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 37 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                                                                 model.page_metadescription

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(80, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.page_metadescription = __value, model.page_metadescription))));
                __builder2.AddAttribute(81, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => model.page_metadescription));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(82, "\r\n                                ");
                __Blazor.RTF.Pages.Admin.ReturnRefund.TypeInference.CreateValidationMessage_3(__builder2, 83, 84, 
#nullable restore
#line 38 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                          () => model.page_metadescription

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(85, "\r\n                            ");
                __builder2.OpenElement(86, "div");
                __builder2.AddAttribute(87, "class", "form-group mb-4 col-lg-12");
                __builder2.AddMarkupContent(88, "<label for=\"cname\">Content<span class=\"astick\">*</span></label>\r\n                                ");
                __builder2.OpenElement(89, "textarea");
                __builder2.AddAttribute(90, "maxlength", "1000");
                __builder2.AddAttribute(91, "name", "contentval");
                __builder2.AddAttribute(92, "type", "text");
                __builder2.AddAttribute(93, "class", "form-control");
                __builder2.AddAttribute(94, "placeholder", "Content");
                __builder2.AddAttribute(95, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 42 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
                                                                                     model.page_content

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(96, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => model.page_content = __value, model.page_content));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(97, "\r\n                    ");
                __builder2.AddMarkupContent(98, "<div class=\"modal-footer\"><button type=\"submit\" id=\"btnSubmit\" class=\"btn btn-primary\">Save</button></div>");
            }
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 55 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ReturnRefund.razor"
       


    RTFAdmin _RTFAdmin;
    ResultObject<List<CmsPageData>> resObject;
    CmsPageData model;

    protected override void OnInitialized()
    {
        _RTFAdmin = new RTFAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        model = new CmsPageData();
        resObject = _RTFAdmin.cmsMasterreq("viewcmsreq", "Return & Refund Policy");
        if (resObject.ResultData.Count > 0)
        {
            model = resObject.ResultData.FirstOrDefault();
        }
        else
        {
            model = new CmsPageData();
        }
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");

    }

    protected override void OnAfterRender(bool firstRender)
    {
        if (firstRender)
        {
            JSRuntime.InvokeVoidAsync("CSBindCmsCkeditor");
        }
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "Return & Refund Policy" });
    }

    protected void HandleInvalidSubmit()
    {
        JSRuntime.InvokeVoidAsync("CSAlertMsg", "Info", "Please Fill Mandatory Field");
    }

    async void OnConfirm()
    {
        await JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        string contant = await JSRuntime.InvokeAsync<string>("getEditor", "contentval");
        ResultObject<string> res1 = new ResultObject<string>();
        model.page_content = contant;
        if (model.page_content == null || model.page_content == "")
        {
            await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Info", "Please Fill Content Field");
        }
        else
        {
            res1 = _RTFAdmin.editCmsMaster("updatecms", "", model);
            if (res1.ResultMessage == "Success")
            {
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Return & Refund Policy updated successfully");
            }
            else if (res1.ResultMessage == "CMS Title Already Exists")
            {
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", res1.ResultMessage);
            }
            else
            {
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
            }
            resObject = _RTFAdmin.cmsMasterreq("viewcmsreq", "Return & Refund Policy");
            model = resObject.ResultData.FirstOrDefault();
        }
        StateHasChanged();
        await JSRuntime.InvokeVoidAsync("CSBindCmsCkeditor");
        await JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
namespace __Blazor.RTF.Pages.Admin.ReturnRefund
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateValidationMessage_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_3<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
