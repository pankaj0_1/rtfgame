#pragma checksum "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6aae463275fef55526bbfc3ddc3161dea25021f4"
// <auto-generated/>
#pragma warning disable 1591
namespace RTF.Pages.Admin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject.PostModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject.RTF;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Pages.Component;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/withdrawalxyz")]
    public partial class Withdrawal : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "layout-px-spacing match-view");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "row layout-top-spacing");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "col-xl-12 col-lg-12 col-sm-12  layout-spacing");
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "widget-content widget-content-area br-6");
            __builder.AddMarkupContent(8, "<div class=\"widget-header\"><div class=\"row\"><div class=\"col-xl-12 col-md-12 col-sm-12 col-12\"><h4>View User Withdrawal Request</h4></div></div></div>\r\n                ");
            __builder.OpenElement(9, "div");
            __builder.AddAttribute(10, "class", "table-responsive mb-4 mt-4");
            __builder.OpenElement(11, "table");
            __builder.AddAttribute(12, "id", "viewwithdt");
            __builder.AddAttribute(13, "class", "multi-table table table-striped table-bordered table-hover non-hover");
            __builder.AddAttribute(14, "style", "width:100%");
            __builder.AddMarkupContent(15, @"<thead><tr><th>Request Date</th>
                                <th>User Name & Mobile</th>
                                <th>Bank Name</th>
                                <th>Ifsc & Account Number</th>
                                <th>Upi Id</th>
                                <th>Amount</th>
                                <th>Branch Name & Address</th>
                                <th class=""text-right"">Action</th></tr></thead>
                        ");
            __builder.OpenElement(16, "tbody");
#nullable restore
#line 31 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                              
                                foreach (Tbl_Withdrawal_RequestAdmin list in resObject.ResultData)
                                {
                                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 34 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                     if (list.approve_status == 0)
                                    {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(17, "tr");
            __builder.AddAttribute(18, "style", "background-color:#ffccRTF !important");
            __builder.OpenElement(19, "td");
            __builder.AddContent(20, 
#nullable restore
#line 37 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.created_at.Value.ToString("dd-MM-yyyy hh:mm tt")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(21, "\r\n                                            ");
            __builder.OpenElement(22, "td");
            __builder.OpenElement(23, "span");
            __builder.AddContent(24, 
#nullable restore
#line 38 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.first_name

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(25, " ");
            __builder.AddContent(26, 
#nullable restore
#line 38 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                        list.last_name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(27, "<br>");
            __builder.OpenElement(28, "span");
            __builder.AddContent(29, "+");
            __builder.AddContent(30, 
#nullable restore
#line 38 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                                           list.country_code

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(31, "-");
            __builder.AddContent(32, 
#nullable restore
#line 38 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                                                              list.mobile

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(33, "\r\n                                            ");
            __builder.OpenElement(34, "td");
            __builder.AddContent(35, 
#nullable restore
#line 39 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.bank_name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(36, "\r\n                                            ");
            __builder.OpenElement(37, "td");
            __builder.AddContent(38, "IFSC: ");
            __builder.AddContent(39, 
#nullable restore
#line 40 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.ifsc_code

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(40, " <br> ");
            __builder.AddContent(41, 
#nullable restore
#line 40 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                              list.account_number

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(42, "\r\n                                            ");
            __builder.OpenElement(43, "td");
            __builder.AddContent(44, 
#nullable restore
#line 41 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.upi_id

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(45, "\r\n                                            ");
            __builder.OpenElement(46, "td");
            __builder.AddMarkupContent(47, "₹");
            __builder.AddContent(48, 
#nullable restore
#line 42 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                  list.amount

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(49, "\r\n                                            ");
            __builder.OpenElement(50, "td");
            __builder.AddContent(51, "Name: ");
            __builder.AddContent(52, 
#nullable restore
#line 43 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.bank_branch

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(53, " <br>Address: ");
            __builder.AddContent(54, 
#nullable restore
#line 43 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                        list.bank_address

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(55, "\r\n                                            ");
            __builder.OpenElement(56, "td");
            __builder.AddAttribute(57, "title", 
#nullable restore
#line 44 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                        list.remarks

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(58, "Rejected");
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 47 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                    }
                                    else if (list.approve_status == 1)
                                    {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(59, "tr");
            __builder.AddAttribute(60, "style", "background-color:#7CFC00 !important");
            __builder.OpenElement(61, "td");
            __builder.AddContent(62, 
#nullable restore
#line 51 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.created_at.Value.ToString("dd-MM-yyyy hh:mm tt")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(63, "\r\n                                            ");
            __builder.OpenElement(64, "td");
            __builder.OpenElement(65, "span");
            __builder.AddContent(66, 
#nullable restore
#line 52 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.first_name

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(67, " ");
            __builder.AddContent(68, 
#nullable restore
#line 52 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                        list.last_name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(69, " <br>");
            __builder.OpenElement(70, "span");
            __builder.AddContent(71, "+");
            __builder.AddContent(72, 
#nullable restore
#line 52 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                                            list.country_code

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(73, "-");
            __builder.AddContent(74, 
#nullable restore
#line 52 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                                                               list.mobile

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(75, "\r\n                                            ");
            __builder.OpenElement(76, "td");
            __builder.AddContent(77, 
#nullable restore
#line 53 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.bank_name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(78, "\r\n                                            ");
            __builder.OpenElement(79, "td");
            __builder.AddContent(80, "IFSC: ");
            __builder.AddContent(81, 
#nullable restore
#line 54 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.ifsc_code

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(82, " <br> ");
            __builder.AddContent(83, 
#nullable restore
#line 54 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                              list.account_number

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(84, "\r\n                                            ");
            __builder.OpenElement(85, "td");
            __builder.AddContent(86, 
#nullable restore
#line 55 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.upi_id

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(87, "\r\n                                            ");
            __builder.OpenElement(88, "td");
            __builder.AddMarkupContent(89, "₹");
            __builder.AddContent(90, 
#nullable restore
#line 56 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                  list.amount

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(91, "\r\n                                            ");
            __builder.OpenElement(92, "td");
            __builder.AddContent(93, "Name: ");
            __builder.AddContent(94, 
#nullable restore
#line 57 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.bank_branch

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(95, " <br>Address: ");
            __builder.AddContent(96, 
#nullable restore
#line 57 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                        list.bank_address

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(97, "\r\n                                            ");
            __builder.AddMarkupContent(98, "<td>Approved</td>");
            __builder.CloseElement();
#nullable restore
#line 60 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                    }
                                    else
                                    {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(99, "tr");
            __builder.OpenElement(100, "td");
            __builder.AddContent(101, 
#nullable restore
#line 64 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.created_at.Value.ToString("dd-MM-yyyy hh:mm tt")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(102, "\r\n                                            ");
            __builder.OpenElement(103, "td");
            __builder.OpenElement(104, "span");
            __builder.AddContent(105, 
#nullable restore
#line 65 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.first_name

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(106, " ");
            __builder.AddContent(107, 
#nullable restore
#line 65 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                        list.last_name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(108, " <br>");
            __builder.OpenElement(109, "span");
            __builder.AddContent(110, "+");
            __builder.AddContent(111, 
#nullable restore
#line 65 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                                            list.country_code

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(112, "-");
            __builder.AddContent(113, 
#nullable restore
#line 65 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                                                               list.mobile

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(114, "\r\n                                            ");
            __builder.OpenElement(115, "td");
            __builder.AddContent(116, 
#nullable restore
#line 66 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.bank_name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(117, "\r\n                                            ");
            __builder.OpenElement(118, "td");
            __builder.AddContent(119, "IFSC: ");
            __builder.AddContent(120, 
#nullable restore
#line 67 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.ifsc_code

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(121, " <br> ");
            __builder.AddContent(122, 
#nullable restore
#line 67 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                              list.account_number

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(123, "\r\n                                            ");
            __builder.OpenElement(124, "td");
            __builder.AddContent(125, 
#nullable restore
#line 68 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                 list.upi_id

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(126, "\r\n                                            ");
            __builder.OpenElement(127, "td");
            __builder.AddMarkupContent(128, "₹");
            __builder.AddContent(129, 
#nullable restore
#line 69 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                  list.amount

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(130, "\r\n                                            ");
            __builder.OpenElement(131, "td");
            __builder.AddContent(132, "Name: ");
            __builder.AddContent(133, 
#nullable restore
#line 70 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                       list.bank_branch

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(134, " <br>Address: ");
            __builder.AddContent(135, 
#nullable restore
#line 70 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                        list.bank_address

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(136, "\r\n                                            ");
            __builder.OpenElement(137, "td");
            __builder.OpenElement(138, "button");
            __builder.AddAttribute(139, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 72 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                    () => editstatus(list.withdrawal_id, 1)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(140, "class", "shadow-none badge outline-badge-primary");
            __builder.AddAttribute(141, "style", "color:#8dbf42 !important; border: 1px solid #8dbf42 !important");
            __builder.AddContent(142, "Approve");
            __builder.CloseElement();
            __builder.AddMarkupContent(143, "\r\n                                                ");
            __builder.OpenElement(144, "button");
            __builder.AddAttribute(145, "style", "margin-top: 2px;");
            __builder.AddAttribute(146, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 73 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                             () => Showwithdraw(list.withdrawal_id)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(147, "class", "badge outline-badge-danger shadow-none");
            __builder.AddContent(148, "Reject");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 76 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 76 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                     
                                }
                            

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 88 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
 if (showingDialog)
{

#line default
#line hidden
#nullable disable
            __builder.OpenComponent<RTF.Pages.Admin.WithdrawalPopUp>(149);
            __builder.AddAttribute(150, "OnCancel", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, 
#nullable restore
#line 90 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                               CancelConfigure

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(151, "OnConfirm", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, 
#nullable restore
#line 90 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                           ConfirmConfigure

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(152, "withdrawal", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<RTF_BaseObject.RTF.Tbl_Withdrawal_RequestAdmin>(
#nullable restore
#line 90 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
                                                                                         _withdrawData

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
#nullable restore
#line 91 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
#nullable restore
#line 93 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\Withdrawal.razor"
       
    [Parameter] public string param { get; set; }
    ResultObject<List<Tbl_Withdrawal_RequestAdmin>> resObject;
    Tbl_Withdrawal_RequestAdmin _withdrawData;
    RTFAdmin _RTFAdmin;
    bool showingDialog = false;

    protected override void OnInitialized()
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        _RTFAdmin = new RTFAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        resObject = _RTFAdmin.viewWithdrawal("viewWihdrawal", "");
        _withdrawData = new Tbl_Withdrawal_RequestAdmin();
    }

    protected override void OnAfterRender(bool firstRender)
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
        JSRuntime.InvokeVoidAsync("BindViewwtihdrawdtexcel", "viewwithdt", 0);
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "View Withdrawal Request" });

    }

    void Showwithdraw(long id)
    {
        _withdrawData = resObject.ResultData.Where(x => x.withdrawal_id == id).FirstOrDefault();
        showingDialog = true;
    }

    void CancelConfigure()
    {
        _withdrawData = new Tbl_Withdrawal_RequestAdmin();
        JSRuntime.InvokeVoidAsync("CSHideModel", "withdrawalpopup");
        showingDialog = false;
    }

    public void editstatus(long id, int status)
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        ResultObject<string> res1 = new ResultObject<string>();
        if (status == 1)
        {
            _withdrawData.approve_status = 1;
            _withdrawData.withdrawal_id = id;
        }
        res1 = _RTFAdmin.updateWirhdrawal("withdrawalupdate", _withdrawData);
        if (res1.ResultMessage == "Success")
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Approve updated successfully.");
            System.Threading.Thread.Sleep(2000);
            JSRuntime.InvokeVoidAsync("CSReloadFunc");
            //_withdrawData = new Tbl_Withdrawal_RequestAdmin();
            //resObject = _RTFAdmin.viewWithdrawal("viewWihdrawal", "");
            //JSRuntime.InvokeVoidAsync("CSBindDataTableRemove", "viewwithdt");
        }
        else
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
        }
        //StateHasChanged();
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");

    }

    void ConfirmConfigure()
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        ResultObject<string> res1 = new ResultObject<string>();
        if (_withdrawData.approve_status == 1)
        {
            _withdrawData.remarks = "";
        }
        res1 = _RTFAdmin.updateWirhdrawal("withdrawalupdate", _withdrawData);
        if (res1.ResultMessage == "Success")
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Reject updated successfully.");
            System.Threading.Thread.Sleep(2000);
            JSRuntime.InvokeVoidAsync("CSReloadFunc");
            JSRuntime.InvokeVoidAsync("CSBindDataTableRemove", "viewwithdt");
            _withdrawData = new Tbl_Withdrawal_RequestAdmin();
            resObject = _RTFAdmin.viewWithdrawal("viewWihdrawal", "");
        }
        else
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
        }
        JSRuntime.InvokeVoidAsync("CSHideModel", "withdrawalpopup");
        showingDialog = false;
        // JSRuntime.InvokeVoidAsync("CSReloadFunc");
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591
