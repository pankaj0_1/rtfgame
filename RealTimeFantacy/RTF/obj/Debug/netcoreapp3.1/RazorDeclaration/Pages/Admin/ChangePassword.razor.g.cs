// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace RTF.Pages.Admin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject.PostModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject.RTF;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Pages.Component;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/changepassword")]
    public partial class ChangePassword : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 48 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Admin\ChangePassword.razor"
       
    RTFAdmin _RTFAdmin;
    ChangePasswordData changep;

    protected override void OnInitialized()
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        _RTFAdmin = new RTFAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        changep = new ChangePasswordData();
    }


    protected override void OnAfterRender(bool firstRender)
    {
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "Change Password" });
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
    }

    void OnConfirm()
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        if (changep.oldpassword == changep.confirmpassword)
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Info", "New Password can't be same as current password");
        }
        else
        {

            ResultObject<string> res = new ResultObject<string>();
            string oldpass = changep.oldpassword;
            res = _RTFAdmin.updatePassword("passudpate", changep.confirmpassword, oldpass,"");
            if (res.ResultMessage == "Success")
            {
                JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Password  updated successfully.");

            }
            else if (res.ResultMessage == "The old password entered by you is incorrect")
            {
                JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", res.ResultMessage);
            }
            else
            {
                JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
            }
            changep = new ChangePasswordData();
            showComapreMsg = false;
        }
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
    }



    bool showComapreMsg = false;
    protected void HandleInvalidSubmit()
    {
        if (changep.newpassword != changep.confirmpassword)
        {
            showComapreMsg = true;
        }
        else
        {
            showComapreMsg = false;
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591
