// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace RTF.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject.PostModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_BaseObject.RTF;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\_Imports.razor"
using RTF.Pages.Component;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(LoginLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Login : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 61 "E:\RTF Repo\rtfgame\RealTimeFantacy\RTF\Pages\Login.razor"
       

    public string Username { get; set; }
    public string Password { get; set; }

   

    ResultObject<Tbl_Admin_User> resLog;
    Tbl_Admin_User _AdminUserData;
    RTFAdmin _LogIn;
    protected override void OnInitialized()
    {
        _LogIn = new RTFAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        _AdminUserData = new Tbl_Admin_User();
        //_AdminUserData.email = "v2rsolutionteam@gmail.com";
        //_AdminUserData.password = "123456";
    }

    protected override void OnAfterRender(bool firstRender)
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "Login" });
    }

    private async Task SignIn(EditContext editContext)
    {
        await JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        try
        {
            ResultObject<Tbl_Admin_User> resData = _LogIn.adminLoginAuth("loginCheck", _AdminUserData);
            if (resData.ResultMessage == "Success")
            {
                if (!session.Items.ContainsKey("AuthLoginData"))
                {
                    session.Items.Add("AuthLoginData", resData.ResultData);
                    await JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
                    await JSRuntime.InvokeAsync<string>(
                "clientJsMethods.RedirectTo", "/dashboard");
                }
            }
            else
            {
                await JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
                await JSRuntime.InvokeAsync<string>("CSAlertMsg", "info", "Invalid email id or password");
            }
        }
        catch (Exception)
        {
            await JSRuntime.InvokeAsync<string>("CSReloadFunc");
        }
    }


    protected void HandleInvalidSubmit()
    {
    }

    

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationmanager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private SessionState session { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591
