﻿using RTF_BaseObject;
using RTF_BaseObject.RTF;
using RTF_BaseObject.RTF_API_Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_Mgr_Interface.Admin
{
    public interface IAdminMgr
    {

        ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data);

        ResultObject<string> addMatch(string RequestType, MatchData data);

        ResultObject<List<Tbl_Tournaments>> viewTournament(string RequestType, string id);
        ResultObject<List<Tbl_Teams>> viewTeam(string RequestType, string id);
        ResultObject<List<MatchData>> viewMatch(string RequestType, string id);
        ResultObject<updateScore> viewScore(string RequestType, string id);
        ResultObject<string> updaeScore(string RequestType, updateScore data);
        ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data);

        ResultObject<List<Tbl_Users>> viewuser(string RequestType, string id);
        ResultObject<List<Tbl_Wallet_Transaction_Admin>> viewTransaction(string RequestType, string id);

        ResultObject<string> walletTopUp(string RequestType, WalletTopUpAdd data);
        ResultObject<string> updatePassword(string RequestType, string param1, string param2,string param3);

        ResultObject<List<ViewBetDtailsAdmin>> viewBet(string RequestType, string Id);

        ResultObject<List<Tbl_Withdrawal_RequestAdmin>> viewWithdrawal(string RequestType, string Id);

        ResultObject<string> updateWirhdrawal(string RequestType, Tbl_Withdrawal_RequestAdmin data);

        ResultObject<reconcile_amountAdmin> reconcile_amount(string RequestType, string id, string param);

        ResultObject<List<MatchData>> viewMatch(string RequestType, string id,string param,string data, string to);

        ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2);
        ResultObject<string> editfaq(string RequestType, string param, FaqData data);

        ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2);
        ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data);

        ResultObject<List<Tbl_QuizAdmin>> quizMaster(string RequestType, string param1, string param2);
        ResultObject<string> editQuizMaster(string RequestType, string param, Tbl_QuizAdmin data);

        ResultObject<List<Tbl_ContestAdmin>> contestData(string RequestType, string param1, string param2);
        ResultObject<string> insertContest(string RequestType, string param, Tbl_ContestAdmin data);

        ResultObject<StaticLeaderboardModelData> inserMatchtContest(string RequestType, string param, List<Tbl_Match_ContestAdmin> data);
        ResultObject<string> CreateLeaderBoardByMatchContest(string RequestType, List<StaticLeaderboardModel> data);

        ResultObject<List<Tbl_Match_ContestAdmin>> MatchtContestData(string RequestType, string param1, string param2);

        ResultObject<List<PackAdmin>> viewPack(string RequestType, string param1, string param2);

        ResultObject<List<GetPackDataDetailsAdmin>> getPacksStaticData(string RequestType, string match_Id, string user_id);
        ResultObject<string> insetMarkeWin(string RequestType, List<MappedResultData> data);
        ResultObject<List<MappedResultData>> viewMarketWinner(string RequestType, string match_Id, string user_id);

        ResultObject<GetMatchResultsStaticDetails> matchResultStaticDetails(string RequestType, string match_Id, string user_id);

        ResultObject<string> updateRank(string RequestType, List<staticPackOrdersData> finalselectedPacksAndRankData);
        ResultObject<List<NotificationUserMapping>> getuserdevicetoken(string RequestType);
        ResultObject<List<submitNotificationModal>> getNotificationLog(string RequestType, string param);

        ResultObject<string> InsertNotification(string RequestType, submitNotificationModal data);


    }
}
