﻿using AutoMapper;
using RTF_BaseObject;
using RTF_BaseObject.RTF;
using RTF_BaseObject.PostModel;
using RTF_DA_Interface.Api;
using RTF_Mgr_Interface.Api;
using System;
using System.Collections.Generic;
using System.Text;
using static RTF_BaseObject.PostModel.PostDataModel;
using RTF_BaseObject.RTF_API_Model;

namespace RTF_Mgr.Api
{
    public class ApiMgr : IApiMgr
    {
        IApiDa _IApiDa;
        IMapper _mapper;

        public ApiMgr(IMapper IMapper, IApiDa IApiDa)
        {
            _IApiDa = IApiDa;
            _mapper = IMapper;
        }
        public ResultObject<string> forgotpass(string RequestType, signin data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IApiDa.forgotpass(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Usersdetails> signin(string RequestType, signin data)
        {
            ResultObject<Usersdetails> resultObject = new ResultObject<Usersdetails>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Usersdetails>>(_IApiDa.signin(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Usersdetails> signup(string RequestType, Usersdetails data)
        {
            ResultObject<Usersdetails> resultObject = new ResultObject<Usersdetails>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Usersdetails>>(_IApiDa.signup(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Configuration> getConfiguration(string RequestType, string param)
        {
            ResultObject<Configuration> resultObject = new ResultObject<Configuration>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Configuration>>(_IApiDa.getConfiguration(RequestType, param));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<dynamic> Get_user_wallet_balance(string RequestType, Int64 Id)
        {
            ResultObject<dynamic> resultObject = new ResultObject<dynamic>();
            try
            {
                resultObject = _mapper.Map<ResultObject<dynamic>>(_IApiDa.Get_user_wallet_balance(RequestType, Id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<ViewBetDtails> addBet(string RequestType, Tbl_Bet_Details data)
        {
            ResultObject<ViewBetDtails> resultObject = new ResultObject<ViewBetDtails>();
            try
            {
                resultObject = _mapper.Map<ResultObject<ViewBetDtails>>(_IApiDa.addBet(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<ViewBetDtails>> viewBet(string RequestType, string Id)
        {
            ResultObject<List<ViewBetDtails>> resultObject = new ResultObject<List<ViewBetDtails>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ViewBetDtails>>>(_IApiDa.viewBet(RequestType, Id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<GetMatchModel>> GethomeMatches(string RequestType, string Id, string param)
        {
            ResultObject<List<GetMatchModel>> resultObject = new ResultObject<List<GetMatchModel>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<GetMatchModel>>>(_IApiDa.GethomeMatches(RequestType, Id, param));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<contestModelallData> GetMatchbyContest(string RequestType, string Id, string param)
        {
            ResultObject<contestModelallData> resultObject = new ResultObject<contestModelallData>();
            try
            {
                resultObject = _mapper.Map<ResultObject<contestModelallData>>(_IApiDa.GetMatchbyContest(RequestType, Id, param));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
        
        public ResultObject<List<GetMyPackModel>> getMyPacksByUser(string RequestType, string match_id, string user_id)
        {
            ResultObject<List<GetMyPackModel>> resultObject = new ResultObject<List<GetMyPackModel>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<GetMyPackModel>>>(_IApiDa.getMyPacksByUser(RequestType, match_id, user_id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
        public ResultObject<List<GetPackDataDetails>> getPacksStaticData(string RequestType, string Id, string param)
        {
            ResultObject<List<GetPackDataDetails>> resultObject = new ResultObject<List<GetPackDataDetails>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<GetPackDataDetails>>>(_IApiDa.getPacksStaticData(RequestType, Id, param));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
        public ResultObject<List<Tbl_Odds_History>> GetMatch_odds(string RequestType, string Id)
        {
            ResultObject<List<Tbl_Odds_History>> resultObject = new ResultObject<List<Tbl_Odds_History>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Odds_History>>>(_IApiDa.GetMatch_odds(RequestType, Id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> walletWithdraw(string RequestType, Tbl_Withdrawal_Request data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IApiDa.walletWithdraw(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "walletWithdraw", "IApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> walletTopUp(string RequestType, WalletTopUp data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IApiDa.walletTopUp(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "walletWithdraw", "IApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Wallet_Transaction> walletTransactionHistory(string RequestType, WalletTopUp data)
        {
            ResultObject<Wallet_Transaction> resultObject = new ResultObject<Wallet_Transaction>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Wallet_Transaction>>(_IApiDa.walletTransactionHistory(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "walletTransactionHistory", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }


        public ResultObject<string> updateemaillink(string RequestType, string email, string userid, string url)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IApiDa.updateemaillink(RequestType, email, userid, url));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<dynamic> matchDynamic(string RequestType, long Id)
        {
            ResultObject<dynamic> resultObject = new ResultObject<dynamic>();
            try
            {
                resultObject = _mapper.Map<ResultObject<dynamic>>(_IApiDa.matchDynamic(RequestType, Id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "matchDynamic", "IApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Tbl_Quiz>> getQuiz(string RequestType, string Id, List<Tbl_Quiz> data)
        {
            ResultObject<List<Tbl_Quiz>> resultObject = new ResultObject<List<Tbl_Quiz>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Quiz>>>(_IApiDa.getQuiz(RequestType, Id, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getQuiz", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<UserWalletApi> getUserWallet(string RequestType, string Id)
        {
            ResultObject<UserWalletApi> resultObject = new ResultObject<UserWalletApi>();
            try
            {
                resultObject = _mapper.Map<ResultObject<UserWalletApi>>(_IApiDa.getUserWallet(RequestType, Id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getUserWallet", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<WalletTransactiontApi>> getTransaction(string RequestType, string Id, WalletTopUp data)
        {
            ResultObject<List<WalletTransactiontApi>> resultObject = new ResultObject<List<WalletTransactiontApi>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<WalletTransactiontApi>>>(_IApiDa.getTransaction(RequestType, Id, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getTransaction", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
        public ResultObject<string> submitPack(string RequestType, string Id, postPackData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IApiDa.submitPack(RequestType, Id, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
        public ResultObject<string> packContestMapping(string RequestType, string Id, List<packContestMapData> data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IApiDa.packContestMapping(RequestType, Id, data));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Tbl_CashfreeUser> addCashFree(string RequestType, string id, WalletTopUp data)
        {
            ResultObject<Tbl_CashfreeUser> resultObject = new ResultObject<Tbl_CashfreeUser>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Tbl_CashfreeUser>>(_IApiDa.addCashFree(RequestType, id, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "addCashFree", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<contestDeailsandLeaderboard> contesDetails(string RequestType, string id, string data)
        {
            ResultObject<contestDeailsandLeaderboard> resultObject = new ResultObject<contestDeailsandLeaderboard>();
            try
            {
                resultObject = _mapper.Map<ResultObject<contestDeailsandLeaderboard>>(_IApiDa.contesDetails(RequestType, id, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "addCashFree", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<GetMyPackDetails>> getPackDetails(string RequestType, string Id, string param)
        {
            ResultObject<List<GetMyPackDetails>> resultObject = new ResultObject<List<GetMyPackDetails>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<GetMyPackDetails>>>(_IApiDa.getPackDetails(RequestType, Id,param));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getPackDetails", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<GetWinnerModel> showWinner(string RequestType, string id, string match_id, string param)
        {
            ResultObject<GetWinnerModel> resultObject = new ResultObject<GetWinnerModel>();
            try
            {
                resultObject = _mapper.Map<ResultObject<GetWinnerModel>>(_IApiDa.showWinner(RequestType, id, match_id, param));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "addCashFree", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Tbl_Access_Token> getAccesTokenData(string RequestType, string param)
        {
            ResultObject<Tbl_Access_Token> resultObject = new ResultObject<Tbl_Access_Token>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Tbl_Access_Token>>(_IApiDa.getAccesTokenData(RequestType, param));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getAccesTokenData", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updateToken(string RequestType, string param, string sp, string param2, string id, string reamrk)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IApiDa.updateToken(RequestType, param, sp, param2,id,reamrk));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updateToken", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Tbl_Withdrawal_Request> getWithdrawal_Request(string RequestType, string id, string param)
        {
            ResultObject<Tbl_Withdrawal_Request> resultObject = new ResultObject<Tbl_Withdrawal_Request>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Tbl_Withdrawal_Request>>(_IApiDa.getWithdrawal_Request(RequestType, id, param));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getAccesTokenData", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> mapdevicetoken(string RequestType, NotificationUserMapping data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IApiDa.mapdevicetoken(RequestType, data));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getAccesTokenData", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Tbl_Withdrawal_Request> getWithdrawal_Request(string RequestType, Tbl_Withdrawal_Request data)
        {
            ResultObject<Tbl_Withdrawal_Request> resultObject = new ResultObject<Tbl_Withdrawal_Request>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Tbl_Withdrawal_Request>>(_IApiDa.getWithdrawal_Request(RequestType, data));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getAccesTokenData", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
    }
}
