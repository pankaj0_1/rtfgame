﻿using RTF_BaseObject;
using RTF_BaseObject.RTF;
using RTF_BaseObject.RTF_API_Model;
using System;
using System.Collections.Generic;
using System.Text;
using static RTF_BaseObject.PostModel.PostDataModel;

namespace RTF_DA_Interface.Api
{
    public interface IApiDa
    {
        public ResultObject<string> forgotpass(string RequestType, signin data);

        ResultObject<Usersdetails> signin(string RequestType, signin data);


        ResultObject<Usersdetails> signup(string RequestType, Usersdetails data);

        ResultObject<Configuration> getConfiguration(string RequestType, string param);

        ResultObject<dynamic> Get_user_wallet_balance(string RequestType, Int64 Id);

        ResultObject<ViewBetDtails> addBet(string RequestType, Tbl_Bet_Details data);
  

        ResultObject<List<ViewBetDtails>> viewBet(string RequestType, string Id);
        ResultObject<List<GetMatchModel>> GethomeMatches(string RequestType, string Id, string param);
        ResultObject<contestModelallData> GetMatchbyContest(string RequestType, string Id, string param);
        ResultObject<List<GetMyPackModel>> getMyPacksByUser(string RequestType, string match_id, string User_id);
        ResultObject<List<GetPackDataDetails>> getPacksStaticData(string RequestType, string match_id, string User_id);
        ResultObject<List<Tbl_Odds_History>> GetMatch_odds(string RequestType, string Id);

        ResultObject<string> walletWithdraw(string RequestType, Tbl_Withdrawal_Request data);

        ResultObject<string> walletTopUp(string RequestType, WalletTopUp data);

        ResultObject<Wallet_Transaction> walletTransactionHistory(string RequestType, WalletTopUp data);
        ResultObject<string> updateemaillink(string RequestType, string email, string userid, string url);

        ResultObject<dynamic> matchDynamic(string RequestType, Int64 Id);

        ResultObject<List<Tbl_Quiz>> getQuiz(string RequestType, string Id, List<Tbl_Quiz> data);
        ResultObject<UserWalletApi> getUserWallet(string RequestType, string Id);
        ResultObject<List<WalletTransactiontApi>> getTransaction(string RequestType, string Id, WalletTopUp data);

        ResultObject<string> submitPack(string RequestType, string Id, postPackData data);
        ResultObject<string> packContestMapping(string RequestType, string Id, List<packContestMapData> data);
        ResultObject<Tbl_CashfreeUser> addCashFree(string RequestType, string id, WalletTopUp data);

        ResultObject<contestDeailsandLeaderboard> contesDetails(string RequestType, string id, string data);
        ResultObject<List<GetMyPackDetails>> getPackDetails(string RequestType, string Id, string param);

        ResultObject<GetWinnerModel> showWinner(string RequestType, string id, string match_id, string param);
        ResultObject<Tbl_Access_Token> getAccesTokenData(string RequestType, string param);
        ResultObject<string> updateToken(string RequestType, string param, string sp, string param2, string id, string reamrk);

        ResultObject<Tbl_Withdrawal_Request> getWithdrawal_Request(string RequestType, string id, string param);

        ResultObject<Tbl_Withdrawal_Request> getWithdrawal_Request(string RequestType, Tbl_Withdrawal_Request data);
        ResultObject<string> mapdevicetoken(string RequestType, NotificationUserMapping data);
    }
}
