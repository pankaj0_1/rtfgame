﻿using RTF_BaseObject;
using RTF_BaseObject.RTF;
using RTF_BaseObject.PostModel;
using RTF_DA_Interface.Api;
using RTF_Utilities;
using RTF_Utilities.Enum;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using static RTF_BaseObject.PostModel.PostDataModel;
using RTF_BaseObject.RTF_API_Model;

namespace RTF_DA.Api
{
    public class ApiDA : BaseConnection, IApiDa
    {
        public ApiDA(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {

        }

        public ResultObject<string> forgotpass(string RequestType, signin data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Configuration> getConfiguration(string RequestType, string param)
        {
            ResultObject<Configuration> resQuote = new ResultObject<Configuration>();
            resQuote.ResultData = new Configuration();
            resQuote.ResultData.lstconfig = new List<Tbl_Configurations>();
            resQuote.ResultData.lstcountry = new List<Tbl_Country_code>();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param);
                using (var data = SqlMapper.QueryMultiple(con, "USP_Configuration", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resQuote.ResultData.lstconfig = data.Read<Tbl_Configurations>().ToList();
                    resQuote.ResultData.lstcountry = data.Read<Tbl_Country_code>().ToList();
                }

                if (resQuote.ResultData == null)
                {
                    resQuote.ResultData = new Configuration();
                    resQuote.ResultMessage = "Data Not Found.";
                    resQuote.Result = ResultType.Info;
                }
                else
                {
                    resQuote.ResultMessage = "Success";
                    resQuote.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                resQuote.ResultMessage = ex.Message.ToString();
                resQuote.Result = ResultType.Error;
            }
            return resQuote;
        }


        public ResultObject<Usersdetails> signin(string RequestType, signin data)
        {
            ResultObject<Usersdetails> resQuery = new ResultObject<Usersdetails>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                resQuery.ResultData = SqlMapper.Query<Usersdetails>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Usersdetails();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }


        public ResultObject<Usersdetails> signup(string RequestType, Usersdetails data)
        {
            ResultObject<Usersdetails> resQuery = new ResultObject<Usersdetails>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                if (RequestType == "myProfile")
                {
                    parameters.Add("@id", data.user_id);
                }
                else if (RequestType == "forcelogin" || RequestType == "dailyReward")
                {
                    parameters.Add("@id", data.user_id);
                }
                else
                {
                    parameters.Add("@XmlInput", XMLData);
                }

                resQuery.ResultData = SqlMapper.Query<Usersdetails>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Usersdetails();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<dynamic> Get_user_wallet_balance(string RequestType, Int64 Id)
        {
            ResultObject<dynamic> resQuery = new ResultObject<dynamic>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<Tbl_User_Wallet>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_User_Wallet();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<ViewBetDtails> addBet(string RequestType, Tbl_Bet_Details data)
        {
            ResultObject<ViewBetDtails> resQuery = new ResultObject<ViewBetDtails>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                resQuery.ResultData = SqlMapper.Query<ViewBetDtails>(con, "USP_Bet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new ViewBetDtails();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<ViewBetDtails>> viewBet(string RequestType, string Id)
        {
            ResultObject<List<ViewBetDtails>> resQuery = new ResultObject<List<ViewBetDtails>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<ViewBetDtails>(con, "USP_Bet", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<ViewBetDtails>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<GetMatchModel>> GethomeMatches(string RequestType, string Id, string param)
        {
            ResultObject<List<GetMatchModel>> resQuery = new ResultObject<List<GetMatchModel>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                parameters.Add("@param", param);
                resQuery.ResultData = SqlMapper.Query<GetMatchModel>(con, "USP_Match", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<GetMatchModel>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else if (resQuery.ResultData.Count > 0)
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
                else
                {
                    resQuery.ResultData = new List<GetMatchModel>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<contestModelallData> GetMatchbyContest(string RequestType, string Id, string param)
        {
            ResultObject<contestModelallData> resQuery = new ResultObject<contestModelallData>();
            try
            {
                resQuery.ResultData = new contestModelallData();
                resQuery.ResultData.contestList = new List<GetContestModel>();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@match_id", Id);
                parameters.Add("@user_id", param);
                //resQuery.ResultData = SqlMapper.Query<GetContestModel>(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                //resQuery.ResultData = SqlMapper.Query<GetContestModel>(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                using (var data = SqlMapper.QueryMultiple(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resQuery.ResultData.contestList = data.Read<GetContestModel>().ToList();
                    resQuery.ResultData.contestCount = data.Read<int>().FirstOrDefault();
                    resQuery.ResultData.packCount = data.Read<int>().FirstOrDefault();
                }
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new contestModelallData();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else if (resQuery.ResultData.contestList.Count > 0)
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
                else
                {
                    resQuery.ResultData = new contestModelallData();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<GetMyPackModel>> getMyPacksByUser(string RequestType, string Id, string param)
        {
            ResultObject<List<GetMyPackModel>> resQuery = new ResultObject<List<GetMyPackModel>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@match_id", Id);
                parameters.Add("@user_id", param);
                resQuery.ResultData = SqlMapper.Query<GetMyPackModel>(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<GetMyPackModel>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else if (resQuery.ResultData.Count > 0)
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
                else
                {
                    resQuery.ResultData = new List<GetMyPackModel>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<GetPackDataDetails>> getPacksStaticData(string RequestType, string Id, string param)
        {
            ResultObject<List<GetPackDataDetails>> resQuery = new ResultObject<List<GetPackDataDetails>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@match_id", Id);
                parameters.Add("@user_id", param);
                MarketAndCatDataList tmpcatlist = new MarketAndCatDataList();
                tmpcatlist.cat_market_data = new List<getMarketCat_>();
                tmpcatlist.market_data = new List<getMarketDatabyCat_>();
                tmpcatlist.selection_data = new List<getSelectionDatabyMarket>();
                // resQuery.ResultData = SqlMapper.Query<GetPackDataDetails>(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                using (var data = SqlMapper.QueryMultiple(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    tmpcatlist.cat_market_data = data.Read<getMarketCat_>().ToList();
                    tmpcatlist.market_data = data.Read<getMarketDatabyCat_>().ToList();
                    tmpcatlist.selection_data = data.Read<getSelectionDatabyMarket>().ToList();
                }
                GetPackDataDetails pData = new GetPackDataDetails();
                pData.total_credits = 100;
                pData.total_selection = 0;
                pData.cat_market_data = new List<getMarketCat>();
                foreach (getMarketCat_ marketCat in tmpcatlist.cat_market_data)
                {

                    getMarketCat cat = new getMarketCat();
                    cat.market_data = new List<getMarketDatabyCat>();
                    cat.category_id = marketCat.category_id;
                    cat.category_name = marketCat.category_name;
                    foreach (getMarketDatabyCat_ marketData in tmpcatlist.market_data.Where(x => x.category_id == cat.category_id).ToList())
                    {
                        getMarketDatabyCat mkt = new getMarketDatabyCat();
                        mkt.selection_data = new List<getSelectionDatabyMarket>();
                        mkt.category_id = marketData.category_id;
                        mkt.market_id = marketData.market_id;
                        mkt.market_name = marketData.market_name;
                        foreach (getSelectionDatabyMarket selData in tmpcatlist.selection_data.Where(x => x.market_id == mkt.market_id).ToList())
                        {
                            mkt.selection_data.Add(selData);
                        }
                        cat.market_data.Add(mkt);
                    }

                    pData.cat_market_data.Add(cat);
                }
                pData.cat_data = new List<getMarketCat_>();
                pData.cat_data.AddRange(tmpcatlist.cat_market_data);
                resQuery.ResultData = new List<GetPackDataDetails>();
                resQuery.ResultData.Add(pData);

                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<GetPackDataDetails>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else if (resQuery.ResultData.Count > 0)
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
                else
                {
                    resQuery.ResultData = new List<GetPackDataDetails>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Odds_History>> GetMatch_odds(string RequestType, string Id)
        {
            ResultObject<List<Tbl_Odds_History>> resQuery = new ResultObject<List<Tbl_Odds_History>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Odds_History>(con, "USP_Bet", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<Tbl_Odds_History>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> walletWithdraw(string RequestType, Tbl_Withdrawal_Request data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Wallet_Payout", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Failure")
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> walletTopUp(string RequestType, WalletTopUp data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Wallet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result != "0")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Wallet_Transaction> walletTransactionHistory(string RequestType, WalletTopUp data)
        {
            ResultObject<Wallet_Transaction> resQuery = new ResultObject<Wallet_Transaction>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);

                using (var data_ = SqlMapper.QueryMultiple(con, "USP_Wallet", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resQuery.ResultData = data_.Read<Wallet_Transaction>().FirstOrDefault();
                    resQuery.ResultData.lstwallet = data_.Read<Tbl_Wallet_Transaction_History>().ToList();
                    resQuery.ResultData.weeklylogins = data_.Read<weeklylogins>().ToList();
                }

                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Wallet_Transaction();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> updateemaillink(string RequestType, string email, string userid, string url)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@email", email);
                parameters.Add("@id", userid);
                parameters.Add("@param", url);
                string _Result = SqlMapper.Query<string>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<dynamic> matchDynamic(string RequestType, Int64 Id)
        {
            ResultObject<dynamic> resQuery = new ResultObject<dynamic>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<dynamic>(con, "USP_Match", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_User_Wallet();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Quiz>> getQuiz(string RequestType, string Id, List<Tbl_Quiz> data)
        {
            ResultObject<List<Tbl_Quiz>> resQuery = new ResultObject<List<Tbl_Quiz>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                if (RequestType == "submitQuiz")
                {
                    var XMLData = XmlConversion.SerializeToXElement(data);
                    parameters.Add("@XmlInput", XMLData);
                }
                resQuery.ResultData = SqlMapper.Query<Tbl_Quiz>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData.Count == 0)
                {
                    resQuery.ResultData = new List<Tbl_Quiz>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }


        public ResultObject<UserWalletApi> getUserWallet(string RequestType, string Id)
        {
            ResultObject<UserWalletApi> resQuery = new ResultObject<UserWalletApi>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<UserWalletApi>(con, "USP_Wallet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new UserWalletApi();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getUserWallet", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<WalletTransactiontApi>> getTransaction(string RequestType, string Id, WalletTopUp data)
        {
            ResultObject<List<WalletTransactiontApi>> resQuery = new ResultObject<List<WalletTransactiontApi>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<WalletTransactiontApi>(con, "USP_Wallet", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData.Count == 0)
                {
                    resQuery.ResultData = new List<WalletTransactiontApi>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getTransaction", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> submitPack(string RequestType, string Id, postPackData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                packsubmitalertdata packsubmitalertdata = new packsubmitalertdata();
                var XMLData = XmlConversion.SerializeToXElement(data);

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@user_id", Id);
                parameters.Add("@match_id", data.match_id);
                parameters.Add("@match_contest_id", data.match_contest_id);
                parameters.Add("@pack_name", data.pack_name);
                parameters.Add("@transaction_id", data.transaction_id);
                parameters.Add("@XmlInput", XMLData);
                packsubmitalertdata = SqlMapper.Query<packsubmitalertdata>(con, "USP_Bet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (packsubmitalertdata.resmsg == "Success")
                {
                    resPlan.ResultData = packsubmitalertdata.pack_id.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> packContestMapping(string RequestType, string Id, List<packContestMapData> data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                // packsubmitalertdata packsubmitalertdata = new packsubmitalertdata();
                var XMLData = XmlConversion.SerializeToXElement(data);

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@user_id", Id);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Bet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Tbl_CashfreeUser> addCashFree(string RequestType, string id, WalletTopUp data)
        {
            ResultObject<Tbl_CashfreeUser> resQuery = new ResultObject<Tbl_CashfreeUser>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                parameters.Add("@XmlInput", XMLData);

                resQuery.ResultData = SqlMapper.Query<Tbl_CashfreeUser>(con, "USP_Wallet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_CashfreeUser();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getUserWallet", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<contestDeailsandLeaderboard> contesDetails(string RequestType, string id, string data)
        {
            ResultObject<contestDeailsandLeaderboard> resQuery = new ResultObject<contestDeailsandLeaderboard>();
            try
            {
                resQuery.ResultData = new contestDeailsandLeaderboard();
                resQuery.ResultData.getContestModel = new GetContestModel();
                resQuery.ResultData.leaderboardModels = new List<StaticLeaderboardModel>();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);

                using (var data_ = SqlMapper.QueryMultiple(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resQuery.ResultData.getContestModel = data_.Read<GetContestModel>().FirstOrDefault();
                    resQuery.ResultData.leaderboardModels = data_.Read<StaticLeaderboardModel>().ToList();
                }
                //resQuery.ResultData = SqlMapper.Query<GetContestModel>(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new contestDeailsandLeaderboard();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getUserWallet", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<GetMyPackDetails>> getPackDetails(string RequestType, string Id, string param)
        {
            ResultObject<List<GetMyPackDetails>> resQuery = new ResultObject<List<GetMyPackDetails>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                parameters.Add("@param", param);
                resQuery.ResultData = SqlMapper.Query<GetMyPackDetails>(con, "USP_Match_Contest", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData.Count == 0)
                {
                    resQuery.ResultData = new List<GetMyPackDetails>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getPackDetails", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }


        public ResultObject<GetWinnerModel> showWinner(string RequestType, string id, string match_id, string param)
        {
            ResultObject<GetWinnerModel> resPlan = new ResultObject<GetWinnerModel>();
            try
            {
                resPlan.ResultData = new GetWinnerModel();
                resPlan.ResultData.GetWinnerModel1 = new List<GetWinnerModel1>();
                resPlan.ResultData.GetWinnerModel2 = new List<GetWinnerModel2>();

                //string _Result = "";
                //var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@user_id", id);
                parameters.Add("@match_id", match_id);
                parameters.Add("@match_contest_id", param);
                using (var data_ = SqlMapper.QueryMultiple(con, "USP_SHOW_WINNER", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resPlan.ResultData.GetWinnerModel1 = data_.Read<GetWinnerModel1>().ToList();
                    resPlan.ResultData.GetWinnerModel2 = data_.Read<GetWinnerModel2>().ToList();
                }
                resPlan.Result = ResultType.Success;
                resPlan.ResultMessage = "Success";
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editTagMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Tbl_Access_Token> getAccesTokenData(string RequestType, string param)
        {
            ResultObject<Tbl_Access_Token> resQuery = new ResultObject<Tbl_Access_Token>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                resQuery.ResultData = SqlMapper.Query<Tbl_Access_Token>(con, "USP_Wallet_Payout", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_Access_Token();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getAccesTokenData", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> updateToken(string RequestType, string param, string sp, string param2, string id, string reamrk)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param);
                parameters.Add("@transferId", param2);
                parameters.Add("@id", id);
                parameters.Add("@reamrk", reamrk);
                string _Result = SqlMapper.Query<string>(con, sp, param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Tbl_Withdrawal_Request> getWithdrawal_Request(string RequestType, string id, string param)
        {
            ResultObject<Tbl_Withdrawal_Request> resQuery = new ResultObject<Tbl_Withdrawal_Request>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                parameters.Add("@param", param);
                resQuery.ResultData = SqlMapper.Query<Tbl_Withdrawal_Request>(con, "USP_Wallet_Payout", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData.withdrawal_id == 0)
                {
                    resQuery.ResultData = new Tbl_Withdrawal_Request();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getWithdrawal_Request", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> mapdevicetoken(string RequestType, NotificationUserMapping data)
        {
            ResultObject<string> resQuery = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@user_id", data.user_id);
                parameters.Add("@device_token", data.device_token);
                resQuery.ResultData = SqlMapper.Query<string>(con, "USP_Notification_User_SP", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == "Success")
                {
                    //  resPlan.ResultData = _Result.ToString();
                    resQuery.Result = ResultType.Success;
                    resQuery.ResultMessage = "Success";
                }
                else
                {
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getWithdrawal_Request", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<Tbl_Withdrawal_Request> getWithdrawal_Request(string RequestType, Tbl_Withdrawal_Request data)
        {
            ResultObject<Tbl_Withdrawal_Request> resQuery = new ResultObject<Tbl_Withdrawal_Request>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", data.user_id);
                parameters.Add("@param", data.otp);
                parameters.Add("@withdrawal_id", data.withdrawal_id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Withdrawal_Request>(con, "USP_Wallet_Payout", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData.withdrawal_id == 0)
                {
                    resQuery.ResultData = new Tbl_Withdrawal_Request();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getWithdrawal_Request", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }
    }
}
