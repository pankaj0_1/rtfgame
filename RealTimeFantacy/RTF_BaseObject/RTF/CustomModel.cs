﻿using Amazon.SimpleNotificationService.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RTF_BaseObject.RTF
{
    public class Usersdetails
    {
        public long user_id { get; set; }

        public string token { get; set; }

        public string country_code { get; set; }
        public long? country_code_id { get; set; }

        public string mobile { get; set; }

        public string first_name { get; set; }

        public string last_name { get; set; }

        public string email { get; set; }
        public string otp { get; set; }

        public string profile_picture { get; set; }


        public short? email_is_verified { get; set; }

        public short? status { get; set; }

        public DateTime? last_login { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }
        public string email_link { get; set; }
        public string creditedCoin { get; set; }
        public string creditType { get; set; }

        public string referral_code { get; set; }

        public decimal addedBalance { get; set; }
        public decimal? winningBalance { get; set; }
        public decimal bonusBalance { get; set; }

        public int login_flag { get; set; }
        public string social_token { get; set; }

    }

    public class Configuration
    {
        public List<Tbl_Configurations> lstconfig { get; set; }
        public List<Tbl_Country_code> lstcountry { get; set; }
    }

    public class MatchData
    {
        public long match_id { get; set; }
        [Required(ErrorMessage = "Please Select Tournament")]
        public string tournament_id { get; set; }

        [Required(ErrorMessage = "Please Select Home Team")]
        public string home_team_id { get; set; }

        [Required(ErrorMessage = "Please Select Away Team")]
        public string away_team_id { get; set; }

        [Required(ErrorMessage = "Please Select Date")]
        public DateTime? match_date { get; set; }


        [Required(ErrorMessage = "Please Select Time")]
        //public TimeSpan match_time { get; set; }
        public DateTime match_time { get; set; }

        [Required(ErrorMessage = "Please Select Match Status")]
        public string match_status { get; set; }

        [Required(ErrorMessage = "Please Enter Match Venue")]
        public string venue { get; set; }
        public short? status { get; set; }

        [Range(double.Epsilon, double.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public string oddHomeTeam { get; set; }

        [Range(double.Epsilon, double.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public string oddAwayTeam { get; set; }
        public string tournament_name { get; set; }
        public string hometeam { get; set; }
        public string awayteam { get; set; }
        public string match_dateStr { get; set; }
        public TimeSpan match_timeStr { get; set; }
        public string hometeamcolorcode { get; set; }
        public string awayteamcolorcode { get; set; }
        public string MatchNumber { get; set; }
        public string winning_team { get; set; }

    }

    public class MatchModel
    {
        public long match_id { get; set; }

        public long? game_id { get; set; }

        public long? tournament_id { get; set; }

        public int? home_team_id { get; set; }

        public int? away_team_id { get; set; }

        public long? market_id { get; set; }

        public DateTime? match_date { get; set; }

        public string match_time { get; set; }

        public short? match_status { get; set; }

        public short? innings { get; set; }

        public short? batting { get; set; }

        public string venue { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

        public string hometeamname { get; set; }
        public string awayteamname { get; set; }

        public string tournament_name { get; set; }
        public string game_name { get; set; }

        public string market_name { get; set; }
        public int? home_team_runs { get; set; }
        public int? away_team_runs { get; set; }
        public int? home_team_wickets { get; set; }
        public int? away_team_wickets { get; set; }
        public decimal? home_team_overs { get; set; }
        public decimal? away_team_overs { get; set; }
        public string MatchNumber { get; set; }
        public decimal? HomeTeam_odds { get; set; }
        public decimal? AwayTeam_odds { get; set; }


        public decimal? Prev_HomeTeam_odds { get; set; }
        public decimal? Prev_AwayTeam_odds { get; set; }

        public decimal? HomeTeam_odds_change_status { get; set; }
        public decimal? AwayTeam_odds_change_status { get; set; }



        public short? Is_Live { get; set; }
        public string hometeamcolorcode { get; set; }
        public string awayteamcolorcode { get; set; }

        public int Hometeam_selectionid { get; set; }
        public int Awayteam_selectionid { get; set; }
        public decimal Hometeam_maxtstake { get; set; }
        public decimal Awayteam_maxtstake { get; set; }
        public string winning_team { get; set; }


        public long super_over_id { get; set; }

        public int? super_home_team_runs { get; set; }

        public int? super_away_team_runs { get; set; }

        public int? super_home_team_wickets { get; set; }

        public int? super_away_team_wickets { get; set; }

        public decimal? super_home_team_overs { get; set; }

        public decimal? super_away_team_overs { get; set; }

        public short? superover_batting { get; set; }
        public decimal Potential_earnings_hometeam { get; set; } = 0;
        public decimal Potential_earnings_Awayteam { get; set; } = 0;

        public string marketStatus { get; set; }
    }

    public class Match_para
    {
        public short? Is_Live { get; set; }
        public string Matchid { get; set; }
        public int match_id { get; set; }
        public int contest_id { get; set; }
        public int pack_id { get; set; }
    }
    public class ViewBetDtails
    {
        public long bet_id { get; set; }

        public long? user_id { get; set; }

        public long? match_id { get; set; }

        public long? market_id { get; set; }

        public long? selection_id { get; set; }

        public long? transaction_id { get; set; }

        public decimal? odds { get; set; }

        public decimal? amount { get; set; }

        public decimal? potential_payout { get; set; }

        public short? bet_status { get; set; }

        public short? won_status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? accepted_at { get; set; }

        public DateTime? rejected_at { get; set; }
        public string order_id { get; set; }

        public long score_id { get; set; }

        public int? home_team_runs { get; set; }

        public int? away_team_runs { get; set; }

        public int? home_team_wickets { get; set; }

        public int? away_team_wickets { get; set; }

        public decimal? home_team_overs { get; set; }

        public decimal? away_team_overs { get; set; }
        public string bet_Message { get; set; }
        public string hometeamname { get; set; }
        public string awayteamname { get; set; }
        public DateTime? match_date { get; set; }
        public string match_time { get; set; }
        public string MatchNumber { get; set; }
        public string winning_team { get; set; }
        public short? Is_Live { get; set; }
    }
    public class WalletTopUp
    {
        public long? user_id { get; set; }
        public decimal? amount { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string user_idstatus { get; set; }
        public string payment_trans_id { get; set; }
        public string paymentDetails { get; set; }
        public string orderId { get; set; }
        public string txStatus { get; set; }
        public string referenceId { get; set; }
    }

    public class updateScore
    {
        public long match_id { get; set; }
        public string tournament_name { get; set; }
        public string hometeamname { get; set; }
        public string awayteamname { get; set; }
        public string awayteamcolorcode { get; set; }
        public string hometeamcolorcode { get; set; }
        public short? status { get; set; }

        public DateTime? match_date { get; set; }
        public string match_dateStr { get; set; }
        public TimeSpan? match_timeStr { get; set; }
        public DateTime? match_time { get; set; }
        public string venue { get; set; }
        public short? innings { get; set; }
        public short? batting { get; set; }
        public short? battingCk { get; set; }

        public short? match_status { get; set; }
        public string match_status_Val { get; set; }

        public decimal? HomeTeam_odds { get; set; } = 0;
        public decimal? AwayTeam_odds { get; set; } = 0;
        public int? home_team_id { get; set; }
        public int? away_team_id { get; set; }
        public int score_id { get; set; }

        public int? home_team_runs { get; set; } = 0;
        public int? away_team_runs { get; set; } = 0;
        public int? home_team_wickets { get; set; } = 0;
        public int? away_team_wickets { get; set; } = 0;
        public decimal? home_team_overs { get; set; } = 0;
        public decimal? away_team_overs { get; set; } = 0;


        public string home_team_runsStatic { get; set; }
        public string away_team_runsStatic { get; set; }
        public string home_team_wicketsStatic { get; set; }
        public string away_team_wicketsStatic { get; set; }
        public string home_team_oversStatic { get; set; }
        public string away_team_oversStatic { get; set; }
        public long? selection_id { get; set; }


        [Required(ErrorMessage = "Please Select Winning Team")]
        public short? winning_team { get; set; }

        public long super_over_id { get; set; }

        public int? super_home_team_runs { get; set; } = 0;

        public int? super_away_team_runs { get; set; } = 0;

        public int? super_home_team_wickets { get; set; } = 0;

        public int? super_away_team_wickets { get; set; } = 0;

        public decimal? super_home_team_overs { get; set; } = 0;

        public decimal? super_away_team_overs { get; set; } = 0;

        public short? superover_innings { get; set; }

        public short? superover_batting { get; set; }

    }

    public class WalletTopUpAdd
    {
        public long? user_id { get; set; }

        [Required(ErrorMessage = "Please Enter Amount")]
        public string amount { get; set; }
        public string payment_trans_id { get; set; }
    }

    public class EncryptPostData
    {
        public string hdndata { get; set; }
    }
    public class Tbl_Wallet_Transaction_Admin
    {
        public long transaction_id { get; set; }

        public long? user_id { get; set; }

        public string trans_type { get; set; }

        public string typename { get; set; }
        public short? type { get; set; }

        public decimal? amount { get; set; }

        public string payment_trans_id { get; set; }

        public DateTime? created_at { get; set; }

        public decimal? subTotal { get; set; }
        public decimal? feeAmount { get; set; }
        public decimal? totalAmount { get; set; }

    }

    public class Wallet_Transaction
    {

        public List<Tbl_Wallet_Transaction_History> lstwallet { get; set; }
        public List<weeklylogins> weeklylogins { get; set; }
        public decimal? Deposits { get; set; }
        public decimal? Betsreturn { get; set; }
        public decimal? BetsPlaced { get; set; }
        public decimal? Withdrawals { get; set; }
        public decimal? totalamount { get; set; }
        public decimal? AvailableCoins { get; set; }
        public decimal? Bonus { get; set; }
        public decimal? Earning { get; set; }

    }
    public class weeklylogins
    {
        public string WeekName { get; set; }
        public decimal? amount { get; set; }
        public DateTime? created_at { get; set; }
    }

    public class ViewBetDtailsAdmin
    {
        public long bet_id { get; set; }

        public long? user_id { get; set; }

        public long? match_id { get; set; }

        public long? market_id { get; set; }

        public long? selection_id { get; set; }

        public long? transaction_id { get; set; }

        public decimal? odds { get; set; }

        public decimal? amount { get; set; }

        public decimal? potential_payout { get; set; }

        public short? bet_status { get; set; }

        public short? won_status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? accepted_at { get; set; }

        public DateTime? rejected_at { get; set; }
        public string order_id { get; set; }

        public long score_id { get; set; }

        public int? home_team_runs { get; set; }

        public int? away_team_runs { get; set; }

        public int? home_team_wickets { get; set; }

        public int? away_team_wickets { get; set; }

        public decimal? home_team_overs { get; set; }

        public decimal? away_team_overs { get; set; }
        public string bet_Message { get; set; }
        public string hometeamname { get; set; }
        public string awayteamname { get; set; }
        public DateTime? match_date { get; set; }
        public string match_time { get; set; }
        public string MatchNumber { get; set; }
        public string winning_team { get; set; }
        public string first_name { get; set; }
        public short? Is_Live { get; set; }
        public string hometeamcolorcode { get; set; }
        public string awayteamcolorcode { get; set; }
        public string mobile { get; set; }
        public string country_code { get; set; }
    }

    public class Tbl_Withdrawal_RequestAdmin
    {
        public long withdrawal_id { get; set; }

        public long? user_id { get; set; }

        public long? account_number { get; set; }

        public string ifsc_code { get; set; }

        public decimal? amount { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }
        public string bank_name { get; set; }
        public string bank_address { get; set; }
        public string bank_branch { get; set; }
        public string upi_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string mobile { get; set; }
        public string country_code { get; set; }
        public short? approve_status { get; set; }

        [Required(ErrorMessage = "Please Enter Remarks")]
        public string remarks { get; set; }

    }

    public class reconcile_amountAdmin
    {
        public decimal In_amt_bet { get; set; }
        public decimal In_amt_wallet { get; set; }
        public decimal out_amt_bet { get; set; }
        public decimal out_amt_wallet { get; set; }
    }
    public class Tbl_Quiz
    {
        public long question_id { get; set; }

        public string question { get; set; }

        public string option1 { get; set; }

        public string option2 { get; set; }

        public string option3 { get; set; }

        public string option4 { get; set; }

        public string answer { get; set; }

        public bool? status { get; set; }

        public DateTime? created_at { get; set; }

        public string quiz_data { get; set; }

    }

    public class PackAdmin
    {
        public string first_name { get; set; }
        public string mobile { get; set; }
        public DateTime? match_date { get; set; }
        public string MatchNumber { get; set; }
        public string bet_Message { get; set; }
        public string hometeamname { get; set; }
        public string awayteamname { get; set; }
        public string hometeamcolorcode { get; set; }
        public string awayteamcolorcode { get; set; }
        public string contest_name { get; set; }
        public string packs { get; set; }
        public string total_potential_payout { get; set; }
        public string rank { get; set; }
        public string match_time { get; set; }
        public string pack_name { get; set; }
    }

    public class UserWalletApi
    {
        public decimal? total_balance { get; set; }
        public decimal? amount { get; set; }
        public decimal? winning { get; set; }
        public decimal? cash_bonus { get; set; }

    }

    public class WalletTransactiontApi
    {
        public string transaction_id { get; set; }
        public string type_name { get; set; }
        public DateTime created_at { get; set; }
        public string pack_name { get; set; }

        public decimal? amount { get; set; }

        public string status { get; set; }
        public string type { get; set; }
    }


    public class Tbl_CashfreeUser
    {
        public string first_name { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string cashfree_order_id { get; set; }
        public int cashfree_amount { get; set; }
    }


    public class orderCreate
    {
        public string orderId { get; set; }
        public decimal? orderAmount { get; set; }
        public int referenceId { get; set; }
        public string txStatus { get; set; }
        public int paymentMode { get; set; }
        public string txTime { get; set; }
        public string signature { get; set; }
        public long user_id { get; set; }
    }

    public class postPackDataAdmin
    {
        public long match_id { get; set; }
        public long match_contest_id { get; set; }
        public long transaction_id { get; set; }
        public string pack_name { get; set; }

        public List<PackDetailsAdmin> list { get; set; }
    }
    public class PackDetailsAdmin
    {
        public int market_id { get; set; }
        public int category_id { get; set; }

        [Required(ErrorMessage = "Please Select Selection")]
        public string selection_id { get; set; }
        public int transaction_id { get; set; }
        public string market_name { get; set; }
        public string category_name { get; set; }
        public string selection_name { get; set; }
        public decimal multiplier { get; set; }
        public decimal credit { get; set; }
        public decimal total_potential_payout { get; set; }
        public short? isWon { get; set; }
        public short? isType { get; set; }
    }


    public class Tbl_Access_Token
    {
        public int Id { get; set; }
        public string payout_token { get; set; }
        public string payout_client_id { get; set; }
        public string payout_client_secret { get; set; }
    }

}
