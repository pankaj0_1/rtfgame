﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RTF_BaseObject.RTF
{
    public class Tbl_Admin_User
    {
        public long admin_id { get; set; }

        //[Required(ErrorMessage = "Please Enter First Name")]
        public string first_name { get; set; }

        public string Last_name { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Please enter valid email address")]

        public string email { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [MinLength(6, ErrorMessage = "Password should contain atleast 6 characters.")]
        public string password { get; set; }

        public DateTime? last_login { get; set; }

        public string profile_picture { get; set; }

    }

    public class Tbl_Bet_Details
    {
        public long bet_id { get; set; }

        public long? user_id { get; set; }

        public long? match_id { get; set; }

        public long? market_id { get; set; }

        public long? selection_id { get; set; }

        public long? transaction_id { get; set; }

        public decimal? odds { get; set; }

        public decimal? amount { get; set; }

        public decimal? potential_payout { get; set; }

        public short? bet_status { get; set; }

        public short? won_status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? accepted_at { get; set; }

        public DateTime? rejected_at { get; set; }

    }

    public class Tbl_Configurations
    {
        public long config_id { get; set; }

        public string type_name { get; set; }
        public string config_name { get; set; }
        public string config_value { get; set; }
        public int? type { get; set; }
        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }
    public class Tbl_Country_code
    {
        public long country_code_id { get; set; }

        public string country_name { get; set; }

        public string country_code { get; set; }

        public string flag { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

        public string iso_code { get; set; }

    }

    public class Tbl_Games
    {
        public long game_id { get; set; }

        public string game_name { get; set; }

        public short? sort_order { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }
    public class Tbl_Market
    {
        public long market_id { get; set; }

        public long? game_id { get; set; }

        public long? tournament_id { get; set; }

        public string market_name { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }

    public class Tbl_Match
    {   
        public long match_id { get; set; }

        public long? game_id { get; set; }


        public long? tournament_id { get; set; }

        public int? home_team_id { get; set; }

        public int? away_team_id { get; set; }

        public long? market_id { get; set; }

        public DateTime? match_date { get; set; }

        public TimeSpan? match_time { get; set; }

        public short? match_status { get; set; }

        public short? innings { get; set; }

        public short? batting { get; set; }

        public string venue { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }
    public class Tbl_Match_Score
    {
        public long score_id { get; set; }

        public long? match_id { get; set; }

        public int? home_team_runs { get; set; }

        public int? away_team_runs { get; set; }

        public int? home_team_wickets { get; set; }

        public int? away_team_wickets { get; set; }

        public int? home_team_overs { get; set; }

        public int? away_team_overs { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }

    public class Tbl_Notes
    {
        public int Id { get; set; }

        public short? Type_of_config { get; set; }

        public string Data { get; set; }

        public DateTime? Created_at { get; set; }

    }

    public class Tbl_Odds_History
    {
        public long odd_history_id { get; set; }

        public long? match_id { get; set; }

        public long? selection_id { get; set; }

        public decimal? odds { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

        public string selection_name { get; set; }
    }

    public class Tbl_Place_Bet_Orders
    {
        public long order_id { get; set; }

        public long? user_id { get; set; }

        public long? transaction_id { get; set; }

        public short? order_status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }

    public class Tbl_Selection
    {
        public long selection_id { get; set; }

        public long? market_id { get; set; }

        public string selection_name { get; set; }

        public int? max_stake { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }

    public class Tbl_Teams
    {
        public long team_id { get; set; }

        public long? tournament_id { get; set; }

        public string team_name { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }

    public class Tbl_Tournaments
    {
        public long tournament_id { get; set; }

        public long game_id { get; set; }

        public string tournament_name { get; set; }

        public short? sort_order { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }

    public class Tbl_User_Wallet
    {
        public long wallet_id { get; set; }

        public long? user_id { get; set; }

        public decimal? balance { get; set; }

    }

    public class Tbl_Users
    {
        public long user_id { get; set; }

        public long? country_code_id { get; set; }

        public string mobile { get; set; }

        public int? otp { get; set; }

        public string first_name { get; set; }

        public string last_name { get; set; }

        public string email { get; set; }

        public string profile_picture { get; set; }

        public short? email_is_verified { get; set; }

        public short? status { get; set; }

        public DateTime? last_login { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }
        public short? otp_is_verified { get; set; }

    }
    public class Tbl_Wallet_Transaction_History
    {
        public long transaction_id { get; set; }

        public long? user_id { get; set; }

        public string trans_type { get; set; }

        public string typename { get; set; }
        public short? type { get; set; }

        public decimal? amount { get; set; }

        public string payment_trans_id { get; set; }

        public DateTime? created_at { get; set; }

        //public string created_at { get; set; }

        public decimal? subTotal { get; set; }
        public decimal? feeAmount { get; set; }
        public decimal? totalAmount { get; set; }

        public string narration { get; set; }
        public string orderid { get; set; }

        public string approve_status { get; set; }
        public string remarks { get; set; }
        public string Datedayname { get; set; }

    }


    public class Tbl_Withdrawal_Request
    {
        public long withdrawal_id { get; set; }

        public long? user_id { get; set; }

        public string batchFormat { get; set; }
        public string batchTransferId { get; set; }

        public decimal? amount { get; set; }

        public string transferId { get; set; }

        public string remarks { get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public string phone { get; set; }

        public long? bank_account { get; set; }

        public string ifsc { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

        public string upi_id { get; set; }

        public string approve_status { get; set; }

        public long? Trans_Id { get; set; }

        public long? referenceId { get; set; }

        public string getBatchTransferStatusJson { get; set; }
        public string pan_number { get; set; }
        public int otp { get; set; }
        public short? otp_is_verified { get; set; }
    }
    public class Tbl_Withdrawal_RequestJson
    {
        public string batchTransferId { get; set; }
        public string batchFormat { get; set; }

        public List<batch> batch { get; set; }
    }

    public class batch
    {
        public decimal? amount { get; set; }

        public string transferId { get; set; }

        public string remarks { get; set; }
        public string name { get; set; }

        public string email { get; set; }

        public string phone { get; set; }

        public long? bankAccount { get; set; }

        public string ifsc { get; set; }

    }

    public class FaqData
    {
        public int faq_id { get; set; }

        [Required(ErrorMessage = "Please Enter Question")]
        [MaxLength(200, ErrorMessage = "Question should be upto 200 characters.")]
        public string faq_question { get; set; }

        public string faq_answer { get; set; }

        public bool active { get; set; }
        public int RowNo { get; set; }
    }

    public class CmsPageData
    {
        public int cms_id { get; set; }

        [Required(ErrorMessage = "Please Enter Page Title")]
        [MaxLength(100, ErrorMessage = "Title should be upto 100 characters.")]
        public string page_title { get; set; }

        //[Required(ErrorMessage = "Please Enter Page Content")]
        public string page_content { get; set; }

        [Required(ErrorMessage = "Please Enter Page Meta Title")]
        [MaxLength(255, ErrorMessage = "Meta Title should be upto 255 characters.")]
        public string page_metatitle { get; set; }

        [MaxLength(500, ErrorMessage = "Meta Description should be upto 500 characters.")]
        public string page_metadescription { get; set; }


        [MaxLength(255, ErrorMessage = "Meta Keyword should be upto 255 characters.")]
        public string page_metakeywords { get; set; }
        public DateTime? created_at { get; set; }
    }


    public class Tbl_QuizAdmin
    {
        public long question_id { get; set; }

        [Required(ErrorMessage = "Please Enter Quetion")]
        [MaxLength(500, ErrorMessage = "Question should be upto 500 characters.")]
        public string question { get; set; }

        [Required(ErrorMessage = "Please Enter Option 1")]
        [MaxLength(100, ErrorMessage = "Option 1 should be upto 100 characters.")]
        public string option1 { get; set; }

        [Required(ErrorMessage = "Please Enter Option 2")]
        [MaxLength(100, ErrorMessage = "Option 2 should be upto 100 characters.")]
        public string option2 { get; set; }

        [Required(ErrorMessage = "Please Enter Option 3")]
        [MaxLength(100, ErrorMessage = "Option 3 should be upto 100 characters.")]
        public string option3 { get; set; }

        [Required(ErrorMessage = "Please Enter Option 4")]
        [MaxLength(100, ErrorMessage = "Option 4 should be upto 100 characters.")]
        public string option4 { get; set; }

        [Required(ErrorMessage = "Please Select Option")]
        public string answer { get; set; }

        public bool? status { get; set; }

        public DateTime? created_at { get; set; }

    }
    public class Tbl_ContestAdmin
    {
        public long contest_id { get; set; }

        [Required(ErrorMessage = "Please Select Tournament")]
        public string tournament_id { get; set; }

        [Required(ErrorMessage = "Please Select Contest Type")]
        public string contest_type { get; set; }

        [Required(ErrorMessage = "Please Enter Contest Name")]
        public string contest_name { get; set; }

        [Required(ErrorMessage = "Please Enter Entry Amount")]
        public string entry_amount { get; set; }

        [Required(ErrorMessage = "Please Enter Packs")]
        public string packs { get; set; }

        public string max_prize { get; set; }


        [Required(ErrorMessage = "Please Enter Pack Win Percentage")]
        public string pack_win_percentage { get; set; }

        [Required(ErrorMessage = "Please Enter Amount Distribute Percentage")]
        public string amount_distribute_percentage { get; set; }

        [Required(ErrorMessage = "Please Select Status")]
        public int status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }

    public class Tbl_Match_ContestAdmin
    {
        public long match_contest_id { get; set; }

        public long? match_id { get; set; }

        public long? contest_id { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

    }

}
