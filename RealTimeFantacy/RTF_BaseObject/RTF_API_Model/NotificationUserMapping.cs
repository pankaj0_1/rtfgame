﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{
    public class NotificationUserMapping
    {
        // public long notification_map_id { get; set; }

        public long user_id { get; set; }

        public string device_token { get; set; }

        //public DateTime? created_at { get; set; }
    }

    public class submitNotificationModal
    {
        public string title { get; set; }
        public string message { get; set; }
        [Required(ErrorMessage = "Please select Target Audience.")]
        public string target_audience { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public bool notification_success { get; set; }
    }
}
