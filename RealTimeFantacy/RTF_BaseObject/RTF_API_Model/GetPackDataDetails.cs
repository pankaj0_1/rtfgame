﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{
    public class GetPackDataDetails
    {
        public decimal total_selection { get; set; }
        public decimal total_credits { get; set; }
        public List<getMarketCat> cat_market_data { get; set; }
        public List<getMarketCat_> cat_data { get; set; }
    }

    public class MarketAndCatDataList
    {
        public List<getMarketCat_> cat_market_data { get; set; }
        public List<getMarketDatabyCat_> market_data { get; set; }
        public List<getSelectionDatabyMarket> selection_data { get; set; }
    }

    public class getMarketCat
    {
        public int category_id { get; set; }
        public string category_name { get; set; }
        public List<getMarketDatabyCat> market_data { get; set; }
       
    }
    public class getMarketCat_
    {
        public int category_id { get; set; }
        public string category_name { get; set; }       

    }

    public class getMarketDatabyCat
    {
        public int category_id { get; set; }
        public int market_id { get; set; }
        public string market_name { get; set; }
        public List<getSelectionDatabyMarket> selection_data { get; set; }
    }
    public class getMarketDatabyCat_
    {
        public int category_id { get; set; }
        public int market_id { get; set; }
        public string market_name { get; set; }
       
    }

    public class getSelectionDatabyMarket
    {
        public int match_id { get; set; }
        public int market_id { get; set; }
        public int selection_id { get; set; }
        public string selection_name { get; set; }
        public decimal? multiplier { get; set; }
        public decimal? credits { get; set; }
        public decimal? potential_points { get; set; }

    }

    public class postPackData
    {
        public long match_id { get; set; }
        public long match_contest_id { get; set; }
        public long transaction_id { get; set; }
        public string  pack_name { get; set; }

        public List<PackDetails> list { get; set; }
    }

    public class packContestMapData
    {
        public long match_contest_id { get; set; }
        public long match_id { get; set; }
        public long pack_id { get; set; }
       // public long user_id { get; set; }
    }

    public class PackDetails
    {
        public int market_id { get; set; }
        public int category_id { get; set; }
        public int selection_id { get; set; }
        public int transaction_id { get; set; }
        public string market_name { get; set; }
        public string category_name { get; set; }
        public string selection_name { get; set; }
        public decimal multiplier { get; set; }
        public decimal credit { get; set; }
        public decimal total_potential_payout { get; set; }
    }
}
