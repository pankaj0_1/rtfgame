﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{
    public class StaticLeaderboardModelData
    {
        public List<GetContestModel> contestModels { get; set; }
        public List<StaticLeaderboardModel> leaderboardModels { get; set; }
        public string resultD { get; set; }
    }
    public class StaticLeaderboardModel
    {
        public long static_leaderboard_id { get; set; }
        public long leaderboard_id { get; set; }
        public long contest_id { get; set; }
        public long match_contest_id { get; set; }

        public long? Ranks_From { get; set; }

        public long? Ranks_To { get; set; }

        public decimal? Prize_Money_per_person { get; set; }

        public decimal? per_of_the_pool { get; set; }

        public long? no_of_ppl_n_rank { get; set; }
        public long leaderboard_for_packs { get; set; }
        public string contest_ids { get; set; }
    }
}
