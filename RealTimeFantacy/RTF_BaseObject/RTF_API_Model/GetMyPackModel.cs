﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{
    public class GetMyPackModel
    {
        public string pack_name { get; set; }

        public long pack_order_id { get; set; }

        public long? match_id { get; set; }

        public long? match_contest_id { get; set; }

        public string home_team { get; set; }

        public string home_team_color { get; set; }

        public string away_team { get; set; }

        public string away_team_color { get; set; }

        public int? selections { get; set; }

        public decimal? potential_payout { get; set; }

        public decimal? credits { get; set; }
        public bool is_purchased { get; set; }

        public decimal? final_potential_payout { get; set; }
    }


    public class GetMyPackDetails
    {
        public string pack_name { get; set; }

        public long pack_order_id { get; set; }

        public decimal? total_potential_payout { get; set; }

        public int? selection_id { get; set; }
        public string selection_name { get; set; }

        public decimal? potential_payout { get; set; }

        public decimal? credits { get; set; }
        public decimal? multipliers { get; set; }

        public string market_name { get; set; }

        public int won_status { get; set; }

        public decimal? final_potential_payout { get; set; }
    }
}
