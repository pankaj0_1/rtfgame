﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{

    public class contestModelallData
    {
        public int contestCount { get; set; }
        public int packCount { get; set; }
        public List<GetContestModel> contestList { get; set; }
    }

    public class contestDeailsandLeaderboard
    {
        public GetContestModel getContestModel { get; set; }
        public List<StaticLeaderboardModel> leaderboardModels { get; set; }
    }
    public class GetContestModel
    {
        public long match_contest_id { get; set; }

        public long contest_id { get; set; }

        public string contest_name { get; set; }

        public int? contest_type { get; set; }

        public decimal? max_prize { get; set; }

        public int? entry_amount { get; set; }

        public int? packs { get; set; }

        public decimal? pack_win_percentage { get; set; }

        public int? remaining_packs { get; set; }

        public long match_id { get; set; }

        public int? current_contest_value { get; set; }

        public int top_price { get; set; }
        public decimal? won_amount { get; set; }
        public long rank { get; set; }
        public decimal? total_potential_points { get; set; }
        public int purchased_pack_count { get; set; }
        public int match_status { get; set; }
    }

    public class packsubmitalertdata
    {
        public string resmsg { get; set; }
        public string pack_id { get; set; }
    }
}
