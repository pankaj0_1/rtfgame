﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{
    public class GetMatchResultsStaticDetails
    {
        public List<staticPackOrdersData> staticPackOrders { get; set; }
        public List<staticPackOrderDetailsData> staticPackOrdersDetails { get; set; }
        public List<staticMarketWinnersData> staticMarketWinners { get; set; }
        public List<staticLeaderboardNmappedContestData> leaderboardNmappedContestDatas { get; set; }
    }
    public class staticPackOrdersData
    {
        public long pack_order_id { get; set; }

        public long? user_id { get; set; }

        public long? match_id { get; set; }

        public long? transaction_id { get; set; }

        public long match_contest_id { get; set; }

        public string pack_name { get; set; }

        public decimal? total_potential_payout { get; set; }

        public decimal? final_potential_payout { get; set; }

        public long rank { get; set; }

        public short? bet_status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? accepted_at { get; set; }

        public DateTime? rejected_at { get; set; }

        public string order_id { get; set; }
        public decimal won_amount { get; set; }
    }
    public class staticPackOrderDetailsData
    {
        public long pack_detail_id { get; set; }

        public long? pack_order_id { get; set; }

        public long? market_id { get; set; }

        public long? selection_id { get; set; }

        public long? match_contest_id { get; set; }

        public decimal? odds { get; set; }

        public decimal? amount { get; set; }

        public decimal? potential_payout { get; set; }

        public short? won_status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }
    }
    public class staticMarketWinnersData
    {
        public long admin_pack_order_id { get; set; }

        public long? match_id { get; set; }

        public DateTime? created_at { get; set; }

        public long? category_id { get; set; }

        public long? market_id { get; set; }

        public long? selection_id { get; set; }

        public short? is_won { get; set; }

        public short? is_type { get; set; }
    }
    public class staticLeaderboardNmappedContestData
    {
        public long leaderboard_rank_id { get; set; }

        public long? leaderboard_id { get; set; }

        public long? Ranks_From { get; set; }

        public long? Ranks_To { get; set; }

        public decimal Prize_Money_per_person { get; set; }

        public float? per_of_the_pool { get; set; }

        public long? no_of_ppl_n_rank { get; set; }

        public DateTime? created_at { get; set; }

        public long? match_contest_id { get; set; }
    }
}
