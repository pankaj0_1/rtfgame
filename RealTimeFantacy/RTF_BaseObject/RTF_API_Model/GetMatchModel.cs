﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{
    public class GetMatchModel
    {
        public long match_id { get; set; }

        public long? game_id { get; set; }

        public long? tournament_id { get; set; }

        public DateTime? match_date { get; set; }
        public string match_date_time { get; set; }

        public string match_time { get; set; }

        public short? match_status { get; set; }

        public short? innings { get; set; }

        public short? batting { get; set; }

        public string venue { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

        public string hometeamcolorcode { get; set; }

        public string awayteamcolorcode { get; set; }

        public int Is_Live { get; set; }

        public string hometeamname { get; set; }

        public string awayteamname { get; set; }

        public string tournament_name { get; set; }

        public int? home_team_runs { get; set; }

        public int? away_team_runs { get; set; }

        public int? home_team_wickets { get; set; }

        public int? away_team_wickets { get; set; }

        public decimal? home_team_overs { get; set; }

        public decimal? away_team_overs { get; set; }

        public string MatchNumber { get; set; }

        public decimal? super_away_team_overs { get; set; }

        public int? super_away_team_runs { get; set; }

        public int? super_away_team_wickets { get; set; }

        public decimal? super_home_team_overs { get; set; }

        public int? super_home_team_runs { get; set; }

        public int? super_home_team_wickets { get; set; }

        public short? superover_batting { get; set; }

        public short? superover_innings { get; set; }

        public decimal? max_prize { get; set; }
        public decimal? no_of_pack { get; set; }
        public decimal? no_of_contest { get; set; }
        public decimal? won_amount { get; set; }
        public bool is_mymatch { get; set; }
        public string match_result { get; set; }
    }


    public class GetMatchModelforWinner
    {        
        public long match_id { get; set; }

        public long? game_id { get; set; }

        public long? tournament_id { get; set; }

        public DateTime? match_date { get; set; }
        public string match_date_time { get; set; }

        public string match_time { get; set; }

        public short? match_status { get; set; }

        public short? innings { get; set; }

        public short? batting { get; set; }

        public string venue { get; set; }

        public short? status { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

        public string hometeamcolorcode { get; set; }

        public string awayteamcolorcode { get; set; }

        public int Is_Live { get; set; }

        public string hometeamname { get; set; }

        public string awayteamname { get; set; }

        public string tournament_name { get; set; }

        public int? home_team_runs { get; set; }

        public int? away_team_runs { get; set; }

        public int? home_team_wickets { get; set; }

        public int? away_team_wickets { get; set; }

        public decimal? home_team_overs { get; set; }

        public decimal? away_team_overs { get; set; }

        public string MatchNumber { get; set; }

        public decimal? super_away_team_overs { get; set; }

        public int? super_away_team_runs { get; set; }

        public int? super_away_team_wickets { get; set; }

        public decimal? super_home_team_overs { get; set; }

        public int? super_home_team_runs { get; set; }

        public int? super_home_team_wickets { get; set; }

        public short? superover_batting { get; set; }

        public short? superover_innings { get; set; }

        public decimal? max_prize { get; set; }
        public decimal? no_of_pack { get; set; }
        public decimal? no_of_contest { get; set; }
        public decimal? won_amount { get; set; }
        public bool is_mymatch { get; set; }
    }
}
