﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{

    public class GetWinnerModelNew
    {
        public GetWinnerModel1 contestData { get; set; }
        public List<GetWinnerModel2> WinnerModel { get; set; }
    }

    public class GetWinnerModel
    {
        public List<GetWinnerModel1> GetWinnerModel1 { get; set; }
        public List<GetWinnerModel2> GetWinnerModel2 { get; set; }
    }

    public  class GetWinnerModel1
    {
        public int match_contest_id { get; set; }
        public string contest_name { get; set; }
        public int contest_type { get; set; }
        public int rank { get; set; }
        public decimal? max_prize { get; set; }
        public int match_id { get; set; }
        public decimal? top_price { get; set; }
        public decimal? won_amount { get; set; }
        public string matchNumber { get; set; }
        public string home_team_color { get; set; }
        public string home_team_name { get; set; }
        public string away_team_color { get; set; }
        public string away_team_name { get; set; }
        public DateTime match_date { get; set; }
    }

    public class GetWinnerModel2
    {
        public int match_contest_id { get; set; }
        public string pack_name { get; set; }
        public decimal? total_potential_payout { get; set; }
        public int rank { get; set; }
        public decimal? won_amount { get; set; }
        public string profile_img { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }

        public int user_id { get; set; }


        public decimal? final_potential_payout { get; set; }
        public int my_record { get; set; }
    }
}
