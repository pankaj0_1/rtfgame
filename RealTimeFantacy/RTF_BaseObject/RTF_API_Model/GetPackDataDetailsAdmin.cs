﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.RTF_API_Model
{

    public class MappedResultData
    {
         public long selection_id { get; set; }
        public string selection_name { get; set; }
        public int isWon { get; set; }
        public long market_id { get; set; }
        public string market_name { get; set; }
        public long cat_id{ get; set; }
        public string cat_name{ get; set; }
        public long match_id{ get; set; }
        public int isType{ get; set; }
        public long admin_pack_order_id { get; set; }
    }
    public class GetPackDataDetailsAdmin
    {
        public decimal total_selection { get; set; }
        public decimal total_credits { get; set; }
        public List<getMarketCatAdmin> cat_market_data { get; set; }
        public List<getMarketCatAdmin_> cat_data { get; set; }
    }

    public class MarketAndCatDataListAdmin
    {
        public List<getMarketCatAdmin_> cat_market_data { get; set; }
        public List<getMarketDatabyCatAdmin_> market_data { get; set; }
        public List<getSelectionDatabyMarketAdmin> selection_data { get; set; }
    }

    public class getMarketCatAdmin
    {
        public int category_id { get; set; }
        public string category_name { get; set; }
        public List<getMarketDatabyCatAdmin> market_data { get; set; }

    }
    public class getMarketCatAdmin_
    {
        public int category_id { get; set; }
        public string category_name { get; set; }

    }

    public class getMarketDatabyCatAdmin
    {
        public int category_id { get; set; }
        public int market_id { get; set; }
        public string market_name { get; set; }

        public int isWon { get; set; } = 1;
        public int isCheckCount { get; set; }
        public List<getSelectionDatabyMarketAdmin> selection_data { get; set; }
    }
    public class getMarketDatabyCatAdmin_
    {
        public int category_id { get; set; }
        public int market_id { get; set; }
        public string market_name { get; set; }

    }

    public class getSelectionDatabyMarketAdmin
    {
        public int match_id { get; set; }
        public int market_id { get; set; }
        public int selection_id { get; set; }
        public string selection_name { get; set; }
        public decimal? multiplier { get; set; }
        public decimal? credits { get; set; }
        public decimal? potential_points { get; set; }

    }

    public class postPackDataAdminWin
    {
        public long match_id { get; set; }
        public long match_contest_id { get; set; }
        public long transaction_id { get; set; }
        public string pack_name { get; set; }

        public List<PackDetailsAdminWin> list { get; set; }
    }

    public class packContestMapDataAdminWin
    {
        public long match_contest_id { get; set; }
        public long match_id { get; set; }
        public long pack_id { get; set; }
        // public long user_id { get; set; }
    }

    public class PackDetailsAdminWin
    {
        public int market_id { get; set; }
        public int category_id { get; set; }
        public int selection_id { get; set; }
        public int transaction_id { get; set; }
        public string market_name { get; set; }
        public string category_name { get; set; }
        public string selection_name { get; set; }
        public decimal multiplier { get; set; }
        public decimal credit { get; set; }
        public decimal total_potential_payout { get; set; }
    }
}
