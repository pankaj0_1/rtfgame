﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTF_BaseObject.PostModel
{
    public class PostDataModel
    {
        public class signin
        {
            public string country_code { get; set; }
            public string mobile { get; set; }
            public string otp { get; set; }
            public string referral_code { get; set; }

            public string email { get; set; }
            public string url { get; set; }
        }
        public class matchPost
        {
            public int match_status { get; set; }
        }

        public class  emailVerifyPost
        {
            public string accessKey { get; set; }
        }
    }
}
