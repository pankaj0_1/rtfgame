﻿using RTF_Utilities.Enum;
using System;

namespace RTF_BaseObject
{
    public class ResultObject<T>
    {
        public ResultType Result;
        public string ResultMessage;
        public string Remark;
        public T ResultData;
    }
}
